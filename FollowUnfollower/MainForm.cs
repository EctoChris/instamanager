﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using OpenQA.Selenium.Chrome;

namespace FollowUnfollower
{
    public partial class MainForm : Form
    {
        public delegate void JustFuckingTest();
        public bool UnfollowerReady;
        public bool FollowerReady = false;
        public bool running = true;
        public bool UnfollowerRunning = false;
        public bool unfollowedEveryone = false;
        public Point SleepIntervals;
        public Point FollowSleepIntervals;
        public int MaxUnfollows;
        public int MaxFollows;
        public int UnfollowCounter;
        public DateTime RunTime1;
        public DateTime RunTime2;
        public DateTime RunTime3;
        public DateTime RunTime4;
        public TimeSpan TimeUnfollowing;
        public int Seconds = 00;
        public int Minutes = 00;
        public int Hours = 00;
        public int Milliseconds = 00;
        public string LoginUsername;

        public MainForm(string oldLoginUsername)
        {
            InitializeComponent();
            unfollowTime1.Text = "0";
            LoginUsername = oldLoginUsername;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            initializeTrackbar(unfollowTrackBar1, 300);
            initializeTrackbar(unfollowTrackBar2, 300);
            initializeTrackbar(unfollowMaxTrackBar, 1000);
            initializeTimeSection();
            StartUnfollowButton.Controls.Add(startButton);
            startButton.Location = new Point(143, 20);
            startButton.BackColor = Color.Transparent;
            UnfollowerReady = false;
            TimeSelectorFrom1.Value = DateTime.Now;
            TimeSelectorUntil1.Value = DateTime.Now;
            TimeSelectorFrom2.Value = DateTime.Now;
            TimeSelectorUntil2.Value = DateTime.Now;
            // UnfollowPanel.Show();
            // UnfollowPanel.Visible = true;
        }
        private void initializeTimeSection()
        {
            FromLabel2.Hide();
            UntilLabel2.Hide();
            TimeSelectorFrom2.Hide();
            TimeSelectorUntil2.Hide();
            FromLabel2F.Hide();
            UntilLabelF.Hide();
            TimeSelectorFrom2F.Hide();
            TimeSelectorUntil2F.Hide();
        }
        public void initializeTrackbar(TrackBar trackbar, int max)
        {
            trackbar.Minimum = 0;
            trackbar.Maximum = max;
            trackbar.TickStyle = TickStyle.BottomRight;
            trackbar.TickFrequency = 5;
        }

        private void button1_MouseHover(object sender, EventArgs e)
        {
            SleepButton.Hide();
        }

        private void unfollowTrackBar1_Scroll(object sender, EventArgs e)
        {
            unfollowTime1.Text = unfollowTrackBar1.Value.ToString();
            SleepIntervals.X = unfollowTrackBar1.Value;
            if (int.Parse(unfollowTime2.Text) < int.Parse(unfollowTime1.Text))
            {
                unfollowTime2.Text = unfollowTime1.Text;
                unfollowTrackBar2.Value = unfollowTrackBar1.Value;
            }
            else if (int.Parse(unfollowTime2.Text) == 0)
            {
                unfollowTime2.Text = unfollowTime1.Text;
            }
        }
        private void followTrackBar1_Scroll(object sender, EventArgs e)
        {
            followTime1.Text = followTrackBar1.Value.ToString();
            FollowSleepIntervals.X = followTrackBar1.Value;
            if (int.Parse(followTime2.Text) < int.Parse(followTime1.Text))
            {
                followTime2.Text = followTime1.Text;
                followTrackBar2.Value = followTrackBar1.Value;
            }
            else if (int.Parse(followTime2.Text) == 0)
            {
                followTime2.Text = followTime1.Text;
            }
        }

        private void unfollowTrackBar2_Scroll(object sender, EventArgs e)
        {
            unfollowTime2.Text = unfollowTrackBar2.Value.ToString();
            SleepIntervals.Y = unfollowTrackBar2.Value;
            if (int.Parse(unfollowTime2.Text) < int.Parse(unfollowTime1.Text))
            {
                unfollowTime2.Text = unfollowTime1.Text;
                unfollowTrackBar2.Value = unfollowTrackBar1.Value;
            }
        }
        private void followTrackBar2_Scroll(object sender, EventArgs e)
        {
            followTime2.Text = followTrackBar2.Value.ToString();
            FollowSleepIntervals.Y = followTrackBar2.Value;
            if (int.Parse(followTime2.Text) < int.Parse(followTime1.Text))
            {
                followTime2.Text = followTime1.Text;
                followTrackBar2.Value = followTrackBar1.Value;
            }
        }
        private void SetUnfollowTime_Click(object sender, EventArgs e)
        {
            SetUnfollowTime.BackColor = Color.Black;
            SetUnfollowTime.ForeColor = Color.White;
            SetUnfollowTime.Refresh();
            Thread.Sleep(450);
            changeButtonStatus(SleepButton, setSleepPic, Step1);
            UnfollowStep1Check.CheckState = CheckState.Checked;
        }

        private void submitFollowTime_Click(object sender, EventArgs e)
        {
            submitFollowTime.BackColor = Color.Black;
            submitFollowTime.ForeColor = Color.White;
            submitFollowTime.Refresh();
            Thread.Sleep(450);
            changeButtonStatus(SleepButtonFollow, SleepPicFollow, followStep1);
            FollowCheck1.CheckState = CheckState.Checked;
        }

        private void changeButtonStatus(Button button, PictureBox pic, Label StepLabel)
        {
            button.BackColor = Color.LightGreen;
            button.FlatAppearance.BorderColor = Color.ForestGreen;
            button.BackgroundImage = null;
            button.ForeColor = Color.Black;
            StepLabel.BackColor = Color.LightGreen;
            StepLabel.ForeColor = Color.Black;
            pic.BackColor = Color.LightGreen;
            StepLabel.Show();
            button.Show();
            pic.Show();
        }


        private void SleepButton_Click(object sender, EventArgs e)   //Unfollow SLEEP
        {
            resetSubmitButton(SetUnfollowTime);
            UnfollowStep1Check.CheckState = CheckState.Unchecked;
            SleepButton.Hide();
            setSleepPic.Hide();
            Step1.Hide();
        }
        private void SleepButtonFollow_Click(object sender, EventArgs e)
        {
            resetSubmitButton(submitFollowTime);
            FollowCheck1.CheckState = CheckState.Unchecked;
            SleepButtonFollow.Hide();
            SleepPicFollow.Hide();
            followStep1.Hide();
        }

        private void resetSubmitButton(Button submitButton)
        {
            submitButton.FlatAppearance.BorderColor = Color.DarkOrange;
            submitButton.BackColor = Color.White;
            submitButton.ForeColor = Color.Black;
            submitButton.Refresh();
        }
        private void submitUnfollows_Click(object sender, EventArgs e)
        {
            submitUnfollows.BackColor = Color.Black;
            submitUnfollows.ForeColor = Color.White;
            submitUnfollows.Refresh();
            Thread.Sleep(450);
            changeButtonStatus(UnfollowLimitButton, setUnfollowLimitPic, Step2);
            UnfollowStep2Check.CheckState = CheckState.Checked;
        }
        private void submitFollows_Click(object sender, EventArgs e)
        {
            submitFollows.BackColor = Color.Black;
            submitFollows.ForeColor = Color.White;
            submitFollows.Refresh();
            Thread.Sleep(450);
            changeButtonStatus(FollowLimitButton, FollowLimitPic, followStep2);
            FollowCheck2.CheckState = CheckState.Checked;
        }

        private void UnfollowLimitButton_Click(object sender, EventArgs e) //Unfollow MAXUNFOLLOW
        {
            resetSubmitButton(submitUnfollows);
            UnfollowStep2Check.CheckState = CheckState.Unchecked;
            UnfollowLimitButton.Hide();
            setUnfollowLimitPic.Hide();
            Step2.Hide();
        }
        private void FollowLimitButton_Click(object sender, EventArgs e)
        {
            resetSubmitButton(submitFollows);
            FollowCheck2.CheckState = CheckState.Unchecked;
            FollowLimitButton.Hide();
            FollowLimitPic.Hide();
            followStep2.Hide();
        }

        private void unfollowMaxTrackBar_Scroll(object sender, EventArgs e)
        {
            maxUnfollows.Text = unfollowMaxTrackBar.Value.ToString();
            MaxUnfollows = unfollowMaxTrackBar.Value;
            progressBar1.Maximum = MaxUnfollows;
        }
        private void followMaxTrackBar_Scroll(object sender, EventArgs e)
        {
            maxFollowsLabel.Text = followMaxTrackBar.Value.ToString();
            MaxFollows = unfollowMaxTrackBar.Value;
            progressBar1.Maximum = MaxUnfollows; //CHANGE THIS TO PROGRESSBAR2
        }

        private void AddTimeSelector_Click(object sender, EventArgs e)
        {
            FromLabel2.Show();
            UntilLabel2.Show();
            TimeSelectorFrom2.Show();
            TimeSelectorUntil2.Show();
            AddTimeSelector.Hide();
            TimeFrom2.Show();
            TimeUntil2.Show();
        }
        private void addTimeSelectorF_Click(object sender, EventArgs e)
        {
            FromLabel2F.Show();
            UntilLabelF.Show();
            TimeSelectorFrom2F.Show();
            TimeSelectorUntil2F.Show();
            addTimeSelectorF.Hide();
        }

        private void TimeOfDayButton_Click(object sender, EventArgs e)
        {
            resetSubmitButton(SubmitTimeOfDay);
            UnfollowStep3Check.CheckState = CheckState.Unchecked;
            Step3.Hide();
            TimeOfDayButton.Hide();
            setTimeOfDayPic.Hide();
        }


        private void TimeOfDayFollowButton_Click(object sender, EventArgs e)
        {
            resetSubmitButton(submitTimeOfDayF);
            FollowCheck3.CheckState = CheckState.Unchecked;
            followStep3.Hide();
            TimeOfDayFollowButton.Hide();
            TimeOfDayFollowPic.Hide();
        }
        private void SubmitTimeOfDay_Click(object sender, EventArgs e)
        {
            SubmitTimeOfDay.BackColor = Color.Black;
            SubmitTimeOfDay.ForeColor = Color.White;
            SubmitTimeOfDay.Refresh();
            Thread.Sleep(450);
            changeButtonStatus(TimeOfDayButton, setTimeOfDayPic, Step3);
            UnfollowStep3Check.CheckState = CheckState.Checked;
            RunTime1 = TimeSelectorFrom1.Value;
            RunTime2 = TimeSelectorUntil1.Value;
            RunTime3 = TimeSelectorFrom2.Value;
            RunTime4 = TimeSelectorUntil2.Value;
        }

        private void submitTimeOfDayF_Click(object sender, EventArgs e)
        {
            submitTimeOfDayF.BackColor = Color.Black;
            submitTimeOfDayF.ForeColor = Color.White;
            submitTimeOfDayF.Refresh();
            Thread.Sleep(450);
            changeButtonStatus(TimeOfDayFollowButton, TimeOfDayFollowPic, followStep3);
            FollowCheck3.CheckState = CheckState.Checked;
            RunTime1 = TimeSelectorFrom1F.Value;
            RunTime2 = TimeSelectorUntil1F.Value;
            RunTime3 = TimeSelectorFrom2F.Value;
            RunTime4 = TimeSelectorUntil2F.Value;
        }

        private void UnfollowStep1Check_CheckStateChanged(object sender, EventArgs e)
        {
            if (UnfollowStep1Check.CheckState == CheckState.Checked && UnfollowStep2Check.CheckState == CheckState.Checked && UnfollowStep3Check.CheckState == CheckState.Checked)
            {
                UnfollowerReady = true;
            }
        }


        private void FollowCheck1_CheckStateChanged(object sender, EventArgs e)
        {
            if (FollowCheck1.CheckState == CheckState.Checked && FollowCheck2.CheckState == CheckState.Checked && FollowCheck3.CheckState == CheckState.Checked)
            {
                FollowerReady = true;
            }

        }
        private async void startButton_Click(object sender, EventArgs e)
        {
            if (UnfollowerReady == true)
            {
                InitializeUnfollowPanel();
                setFollowTime.Hide();
                //JustFuckingTest callback = loadAllTheFuckingThingAfterTheFunctionIsLoaded;
                //  InitializeUnfollowPanel(callback);
                UnfollowerRunning = true;
                UnfollowPanel.Show();
                UnfollowTimer.Start();
                // UnfollowPanel.Visible = true;
                //Utils.StartTimer(UnfollowPauseButton, UnfollowPlayButton, RunningStatus, UnfollowTimer);
                UnfollowPauseButton.Show();
                UnfollowPlayButton.Hide();
                RunningStatus.Text = "Running.";
                RunningStatus.BackColor = Color.Transparent;
                RunningStatus.ForeColor = Color.Lime;
                await Task.Run(async () => await UnfollowAction()); // with <3 by steven and Google
                //UnfollowPauseButton_Click(sender, e);
            }
        }

        private void loadAllTheFuckingThingAfterTheFunctionIsLoaded()
        {
            setFollowTime.Hide();
        }

        private void InitializeUnfollowPanel(JustFuckingTest callback)
        {
            SleepInterval1.Text = SleepIntervals.X.ToString();
            SleepInterval2.Text = SleepIntervals.Y.ToString();
            MaxUnfollowDisplay.Text = MaxUnfollows.ToString();
            TimeFrom1.Text = RunTime1.ToShortTimeString();
            TimeUntil1.Text = RunTime2.ToShortTimeString();
            TimeFrom2.Text = RunTime3.ToShortTimeString();
            TimeUntil2.Text = RunTime4.ToShortTimeString();
            callback();
        }
        private void InitializeUnfollowPanel()
        {
            SleepInterval1.Text = SleepIntervals.X.ToString();
            SleepInterval2.Text = SleepIntervals.Y.ToString();
            MaxUnfollowDisplay.Text = MaxUnfollows.ToString();
            TimeFrom1.Text = RunTime1.ToShortTimeString();
            TimeUntil1.Text = RunTime2.ToShortTimeString();
            TimeFrom2.Text = RunTime3.ToShortTimeString();
            TimeUntil2.Text = RunTime4.ToShortTimeString();
        }

        private async void UnfollowTimer_Tick(object sender, EventArgs e)
        {
            running = !running;
            string running1 = "Running.";
            string running2 = "Running..";
            if (running == true) { RunningStatus.Text = running1; }
            else { RunningStatus.Text = running2; }

            Secs.Text = Seconds.ToString();
            Mins.Text = Minutes.ToString();
            Hrs.Text = Hours.ToString();
            Seconds++;
            if (Seconds > 59)
            {
                Minutes++;
                Seconds = 0;
            }
            if (Minutes > 59)
            {
                Hours++;
                Minutes = 0;
            }

            if (UnfollowerRunning)
            {
                UnfollowPauseButton.Show();
                UnfollowPlayButton.Hide();
            }
            else
            {
                UnfollowPauseButton.Hide();
                UnfollowPlayButton.Show();
            }

            //bool ready = Utils.CheckConditions(RunTime1, RunTime2, RunTime3, RunTime4, UnfollowCounter, MaxUnfollows, UnfollowerRunning, UnfollowerReady);
            //if (ready && unfollowedEveryone == false)
            //{
            //    UnfollowerRunning = true;
            //    await Task.Run(async () => await UnfollowAction());
            //}
               
              
        }

        private void UnfollowBackButton_Click(object sender, EventArgs e)
        {
            UnfollowPanel.Hide();
            // UnfollowPanel.Visible = false;
            UnfollowTimer.Stop();
            setFollowTime.Show();
            UnfollowerRunning = false;
        }

        private void UnfollowPauseButton_Click(object sender, EventArgs e)
        {
            // UnfollowerRunning = false;
            //Utils.StopTimer(UnfollowPauseButton, UnfollowPlayButton, RunningStatus, UnfollowTimer);
            UnfollowTimer.Stop();
            UnfollowerRunning = false;
            UnfollowPauseButton.Hide();
            UnfollowPlayButton.Show();
            RunningStatus.Text = "Stopped";
            RunningStatus.BackColor = Color.Black;
            RunningStatus.ForeColor = Color.Red;
        }

        private async void UnfollowPlayButton_Click(object sender, EventArgs e)
        {
           // bool ready = Utils.CheckConditions(RunTime1, RunTime2, RunTime3, RunTime4, UnfollowCounter, MaxUnfollows, UnfollowerRunning, UnfollowerReady);
            //if (ready)
            //{
            //    UnfollowTimer.Start();
            //    UnfollowPauseButton.Show();
            //    UnfollowPlayButton.Hide();
            //    RunningStatus.Text = "Running.";
            //    RunningStatus.BackColor = Color.Transparent;
            //    RunningStatus.ForeColor = Color.Lime;
            //    //  await Task.Run(async () => await UnfollowAction());
            //}
        }

        public async Task UnfollowAction()
        {
            Utils.GoToUrl("https://www.instagram.com/" + LoginUsername + '/');
            Utils.FindAndClick(Const.PeopleFollowing);
            int counter = 0;
            UnfollowerRunning = true;

            while (UnfollowerRunning)
            {
                if (!(DateTime.Now >= RunTime1 && DateTime.Now <= RunTime2 || DateTime.Now >= RunTime3 && DateTime.Now <= RunTime4))
                {
                    Invoke(new Action(() =>
                    {
                        StatusLabel.Visible = true;
                        StatusLabel.Text = "Cannot Run at this time";
                        UnfollowTimer.Stop();
                        RunningStatus.Text = "Stopped";
                        RunningStatus.BackColor = Color.Black;
                        RunningStatus.ForeColor = Color.Red;
                    }));
                    UnfollowerRunning = false;
                    return;
                }
              
                IList<IWebElement> unfollowButtonsVisible = new List<IWebElement>();
                unfollowButtonsVisible = Globals.pDriver.FindElements(By.XPath(Const.visibleUnfollowButtons));

                Thread.Sleep(1500);

                if (unfollowButtonsVisible.Count == 0)
                {
                    Invoke(new Action(() =>
                    {
                        StatusLabel.Visible = true;
                        StatusLabel.Text = "Unfollowed All Possible Users";
                        UnfollowerRunning = false;
                        unfollowedEveryone = true;
                        return;
                    }));

                }
               
                foreach (IWebElement e in unfollowButtonsVisible)
                {
                    if (UnfollowerRunning == false)
                        return;

                    e.Click();
                    counter++;
                    IWebElement tempProfileNameLi = Utils.FindElement(e, Const.UnfollowProfileNameLi);
                    IWebElement tempProfileName = Utils.FindElement(tempProfileNameLi, Const.UnfollowProfileName);

                    string tempProfileNameString = tempProfileName.Text;
                    ListViewItem tempListViewItem = new ListViewItem(tempProfileNameString);
                    Invoke(new Action(() =>
                    {
                        SuccessfulUnfollowDisplay.Items.Insert(0, tempListViewItem);
                        progressBar1.Value++; //threw exception when higher than maximum
                        UnfollowCounter++;
                        CurrentUnfollowAmount.Text = UnfollowCounter.ToString();
                        
                        if (UnfollowCounter == MaxUnfollows)
                        {
                            StatusLabel.Visible = true;
                            StatusLabel.Text = "Unfollowed All Possible Users";
                            UnfollowerRunning = false;
                            return;
                        }
                    }));

                    Utils.RandomSleepInterval(SleepIntervals.X, SleepIntervals.Y);
                }
                unfollowScrollDown();
            }
        }


        public void unfollowScrollDown()
        {
            IWebElement scrollerHeader = Utils.FindElement(Const.UnfollowFormScrollingHeader);
            IWebElement scrollerHeaderParent = Utils.FindElement(Const.UnfollowFormScrollingParent);
            IWebElement scroller = Utils.FindElement(Const.UnfollowFormScrolling);
            ((IJavaScriptExecutor)Globals.pDriver).ExecuteScript("arguments[0].scrollBy(0, 400)", scroller);
        }
    }
}



//TODO:
//"Cannot run at this time" timer keeps running ...