﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Net;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;
using System.Threading;
using OpenQA.Selenium.Chrome;
using System.Resources;

namespace FollowUnfollower
{
    public partial class HomePage : Form
    {
        public string username;
        public string resourceFolderPath;

        public HomePage(string username)
        {
            this.username = username;
            InitializeComponent();

            //Profile Pic Logic                
            string RunningPath = AppDomain.CurrentDomain.BaseDirectory;
            resourceFolderPath = Path.GetFullPath(Path.Combine(RunningPath, @"..\..\"));
            resourceFolderPath += @"Resources\";
            setProfilePic();
            if (Globals.pDriver.Url != "https://www.instagram.com/" + username + @"/")
            {
                Utils.GoToUrl("https://www.instagram.com/" + username + @"/");
            }
            refreshFollowAndFollowersAmount();
            usernameLabel.Text = username;
            totalFollowCountLabel.Text = Properties.Settings.Default.TotalFollows.ToString();
            totalUnfollowCountLabel.Text = Properties.Settings.Default.TotalUnfollows.ToString();
        }

        private void setProfilePic()
        {
            string resourceName = username + "ProfilePic.jpg";
            // Bitmap bmp = (Bitmap)Properties.Resources.ResourceManager.GetObject(resourceName);
            try
            {
                Image image = Image.FromFile(resourceFolderPath + resourceName);
                profilePicSource.BackgroundImage = image;

            } catch(FileNotFoundException e)
            {
                downloadProfilePic();
            }
        }

        private void downloadProfilePic()
        {
            usernameLabel.Text = Properties.Resources.ResourceManager.BaseName.ToString();
            if (Globals.pDriver.Url != "https://www.instagram.com/" + username + @"/")
            {
                Utils.GoToUrl("https://www.instagram.com/" + username + @"/");
            }

            string srcUrl = "";
            //Find image src url here
            IWebElement profilePic = Globals.pDriver.FindElement(By.XPath(Const.profilePic));
            srcUrl = profilePic.GetAttribute("src");
            using (WebClient client = new WebClient())
            {
                 //client.DownloadFileAsync(new Uri(srcUrl), @"C:\tempImages\" + username + "ProfilePic.jpg");
                 client.DownloadFileAsync(new Uri(srcUrl), resourceFolderPath + username + "ProfilePic.jpg");
            }
            Globals.pDriver.Navigate().GoToUrl(srcUrl); //download image here
            Thread.Sleep(500);
            setProfilePic();
        }

        private void refreshFollowAndFollowersAmount()
        {
            IWebElement followersCount, followingCount, followersParentElement, followingParentElement;
            followersParentElement = Utils.FindElement(@"//a[@href = '/" + username.ToLower() + @"/followers/']");
            followingParentElement = Utils.FindElement(@"//a[@href = '/" + username.ToLower() + @"/following/']");
            followersCount = followersParentElement.FindElement(By.XPath(".//span[1]"));
            followingCount = followingParentElement.FindElement(By.XPath(".//span[1]"));

            followersLabel.Text = followersCount.Text;
            followingLabel.Text = followingCount.Text;
        }

        private void HomePage_Load(object sender, EventArgs e)
        {
            
        }

        private void startFollowingButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            if (Globals.followPage1 == null)
                Globals.followPage1 = new FollowPage1();

            Globals.followPage1.Show();
            
        }

        private void startUnfollowingButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            if (Globals.unfollowPage1 == null)
                Globals.unfollowPage1 = new UnfollowPage1();

            Globals.unfollowPage1.Show();
        }
    }
}
