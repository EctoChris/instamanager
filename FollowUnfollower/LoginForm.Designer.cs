﻿namespace FollowUnfollower
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.usernameInput = new System.Windows.Forms.TextBox();
            this.passwordInput = new System.Windows.Forms.TextBox();
            this.AutomationLabel = new System.Windows.Forms.Label();
            this.LoginSecurelyLabel = new System.Windows.Forms.Label();
            this.LoginMessageLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.loginStatusLabel = new System.Windows.Forms.Label();
            this.usernameIcon = new System.Windows.Forms.PictureBox();
            this.loginButton = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.FollowGramName = new System.Windows.Forms.PictureBox();
            this.WhiteFollowGramLogo = new System.Windows.Forms.PictureBox();
            this.backgroundGradient = new System.Windows.Forms.PictureBox();
            this.backgroundBorder = new System.Windows.Forms.PictureBox();
            this.passwordIcon = new System.Windows.Forms.PictureBox();
            this.LoginTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.usernameIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loginButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FollowGramName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WhiteFollowGramLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backgroundGradient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backgroundBorder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.passwordIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Lavender;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.button1.Location = new System.Drawing.Point(548, 362);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(114, 34);
            this.button1.TabIndex = 0;
            this.button1.Text = "Log In";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // usernameInput
            // 
            this.usernameInput.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.usernameInput.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.usernameInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernameInput.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.usernameInput.Location = new System.Drawing.Point(530, 196);
            this.usernameInput.Name = "usernameInput";
            this.usernameInput.Size = new System.Drawing.Size(201, 19);
            this.usernameInput.TabIndex = 3;
            this.usernameInput.Click += new System.EventHandler(this.usernameInput_Click);
            // 
            // passwordInput
            // 
            this.passwordInput.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.passwordInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passwordInput.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.passwordInput.Location = new System.Drawing.Point(530, 262);
            this.passwordInput.Name = "passwordInput";
            this.passwordInput.Size = new System.Drawing.Size(199, 19);
            this.passwordInput.TabIndex = 4;
            this.passwordInput.Click += new System.EventHandler(this.passwordInput_Click);
            // 
            // AutomationLabel
            // 
            this.AutomationLabel.AutoSize = true;
            this.AutomationLabel.BackColor = System.Drawing.Color.Transparent;
            this.AutomationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.AutomationLabel.ForeColor = System.Drawing.Color.White;
            this.AutomationLabel.Location = new System.Drawing.Point(35, 257);
            this.AutomationLabel.Name = "AutomationLabel";
            this.AutomationLabel.Size = new System.Drawing.Size(286, 62);
            this.AutomationLabel.TabIndex = 11;
            this.AutomationLabel.Text = "Instagram Automation \r\nSoftware";
            this.AutomationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LoginSecurelyLabel
            // 
            this.LoginSecurelyLabel.AutoSize = true;
            this.LoginSecurelyLabel.BackColor = System.Drawing.Color.Transparent;
            this.LoginSecurelyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.LoginSecurelyLabel.ForeColor = System.Drawing.Color.White;
            this.LoginSecurelyLabel.Location = new System.Drawing.Point(27, 397);
            this.LoginSecurelyLabel.Name = "LoginSecurelyLabel";
            this.LoginSecurelyLabel.Size = new System.Drawing.Size(297, 60);
            this.LoginSecurelyLabel.TabIndex = 12;
            this.LoginSecurelyLabel.Text = "Login securely with FollowGram. We\r\ncannot access your login information, it is\r\n" +
    "stored and encrypted locally\r\n";
            this.LoginSecurelyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LoginMessageLabel
            // 
            this.LoginMessageLabel.AutoSize = true;
            this.LoginMessageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoginMessageLabel.Location = new System.Drawing.Point(519, 133);
            this.LoginMessageLabel.Name = "LoginMessageLabel";
            this.LoginMessageLabel.Size = new System.Drawing.Size(163, 24);
            this.LoginMessageLabel.TabIndex = 15;
            this.LoginMessageLabel.Text = "Login to Instagram";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(491, 435);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(230, 16);
            this.label1.TabIndex = 20;
            this.label1.Text = "We are not associated with Instagram";
            // 
            // loginStatusLabel
            // 
            this.loginStatusLabel.AutoSize = true;
            this.loginStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.loginStatusLabel.Location = new System.Drawing.Point(544, 503);
            this.loginStatusLabel.Name = "loginStatusLabel";
            this.loginStatusLabel.Size = new System.Drawing.Size(60, 24);
            this.loginStatusLabel.TabIndex = 21;
            this.loginStatusLabel.Text = "label2";
            this.loginStatusLabel.Visible = false;
            // 
            // usernameIcon
            // 
            this.usernameIcon.BackgroundImage = global::FollowUnfollower.Properties.Resources.usernameIcon;
            this.usernameIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.usernameIcon.Location = new System.Drawing.Point(489, 182);
            this.usernameIcon.Name = "usernameIcon";
            this.usernameIcon.Size = new System.Drawing.Size(35, 36);
            this.usernameIcon.TabIndex = 22;
            this.usernameIcon.TabStop = false;
            // 
            // loginButton
            // 
            this.loginButton.BackgroundImage = global::FollowUnfollower.Properties.Resources.loginButton;
            this.loginButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.loginButton.Location = new System.Drawing.Point(462, 348);
            this.loginButton.Name = "loginButton";
            this.loginButton.Size = new System.Drawing.Size(290, 71);
            this.loginButton.TabIndex = 19;
            this.loginButton.TabStop = false;
            this.loginButton.Click += new System.EventHandler(this.loginButton_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Black;
            this.pictureBox2.Location = new System.Drawing.Point(489, 290);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(240, 2);
            this.pictureBox2.TabIndex = 17;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Black;
            this.pictureBox1.Location = new System.Drawing.Point(489, 224);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(240, 2);
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // FollowGramName
            // 
            this.FollowGramName.BackgroundImage = global::FollowUnfollower.Properties.Resources.FollowGram;
            this.FollowGramName.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FollowGramName.Location = new System.Drawing.Point(489, 59);
            this.FollowGramName.Name = "FollowGramName";
            this.FollowGramName.Size = new System.Drawing.Size(246, 50);
            this.FollowGramName.TabIndex = 14;
            this.FollowGramName.TabStop = false;
            // 
            // WhiteFollowGramLogo
            // 
            this.WhiteFollowGramLogo.BackColor = System.Drawing.Color.Transparent;
            this.WhiteFollowGramLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.WhiteFollowGramLogo.Image = global::FollowUnfollower.Properties.Resources.whiteInstaLogo;
            this.WhiteFollowGramLogo.Location = new System.Drawing.Point(86, 42);
            this.WhiteFollowGramLogo.Name = "WhiteFollowGramLogo";
            this.WhiteFollowGramLogo.Size = new System.Drawing.Size(202, 197);
            this.WhiteFollowGramLogo.TabIndex = 10;
            this.WhiteFollowGramLogo.TabStop = false;
            // 
            // backgroundGradient
            // 
            this.backgroundGradient.BackgroundImage = global::FollowUnfollower.Properties.Resources.instaGradient;
            this.backgroundGradient.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.backgroundGradient.Location = new System.Drawing.Point(0, 0);
            this.backgroundGradient.Name = "backgroundGradient";
            this.backgroundGradient.Size = new System.Drawing.Size(383, 561);
            this.backgroundGradient.TabIndex = 9;
            this.backgroundGradient.TabStop = false;
            // 
            // backgroundBorder
            // 
            this.backgroundBorder.BackgroundImage = global::FollowUnfollower.Properties.Resources.backgroundBorder1;
            this.backgroundBorder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.backgroundBorder.Location = new System.Drawing.Point(431, 42);
            this.backgroundBorder.Name = "backgroundBorder";
            this.backgroundBorder.Size = new System.Drawing.Size(355, 436);
            this.backgroundBorder.TabIndex = 13;
            this.backgroundBorder.TabStop = false;
            // 
            // passwordIcon
            // 
            this.passwordIcon.BackgroundImage = global::FollowUnfollower.Properties.Resources.passwordIcon;
            this.passwordIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.passwordIcon.Location = new System.Drawing.Point(487, 248);
            this.passwordIcon.Name = "passwordIcon";
            this.passwordIcon.Size = new System.Drawing.Size(35, 36);
            this.passwordIcon.TabIndex = 23;
            this.passwordIcon.TabStop = false;
            // 
            // LoginTimer
            // 
            this.LoginTimer.Interval = 1000;
            this.LoginTimer.Tick += new System.EventHandler(this.LoginTimer_Tick);
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(827, 560);
            this.Controls.Add(this.passwordIcon);
            this.Controls.Add(this.usernameIcon);
            this.Controls.Add(this.loginStatusLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.loginButton);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.LoginMessageLabel);
            this.Controls.Add(this.FollowGramName);
            this.Controls.Add(this.AutomationLabel);
            this.Controls.Add(this.LoginSecurelyLabel);
            this.Controls.Add(this.WhiteFollowGramLogo);
            this.Controls.Add(this.backgroundGradient);
            this.Controls.Add(this.passwordInput);
            this.Controls.Add(this.usernameInput);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.backgroundBorder);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Follower";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.usernameIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loginButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FollowGramName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WhiteFollowGramLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backgroundGradient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backgroundBorder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.passwordIcon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox usernameInput;
        private System.Windows.Forms.TextBox passwordInput;
        private System.Windows.Forms.PictureBox backgroundGradient;
        private System.Windows.Forms.PictureBox WhiteFollowGramLogo;
        private System.Windows.Forms.Label AutomationLabel;
        private System.Windows.Forms.Label LoginSecurelyLabel;
        private System.Windows.Forms.PictureBox backgroundBorder;
        private System.Windows.Forms.PictureBox FollowGramName;
        private System.Windows.Forms.Label LoginMessageLabel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox loginButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label loginStatusLabel;
        private System.Windows.Forms.PictureBox usernameIcon;
        private System.Windows.Forms.PictureBox passwordIcon;
        private System.Windows.Forms.Timer LoginTimer;
    }
}

