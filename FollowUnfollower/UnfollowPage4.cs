﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FollowUnfollower
{
    public partial class UnfollowPage4 : Form
    {
        public UnfollowPage4()
        {
            InitializeComponent();
        }

        private void SettingsGuideScreen_Load(object sender, EventArgs e)
        {
            //comment test
        }

        private void ContinueButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            if (Globals.unfollowingPage == null)
                Globals.unfollowingPage = new UnfollowingPage();

            Globals.unfollowingPage.Show();
        }

        private void HomeButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            Globals.homePage.Show();
        }
    }
}
