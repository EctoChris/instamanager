﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FollowUnfollower.Properties;


namespace FollowUnfollower
{
    public partial class UnfollowPage2 : Form
    {
        public UnfollowPage2()
        {
            InitializeComponent();
            TimeBetweenSessionsFrom.Value = (int)Settings.Default["TimeBetweenSessionsFrom"];
            TimeBetweenSessionsTo.Value = (int)Settings.Default["TimeBetweenSessionsTo"];
            UnfollowsPerSessionFrom.Value = (int)Settings.Default["FollowsPerSessionFrom"];
            UnfollowsPerSessionTo.Value = (int)Settings.Default["FollowsPerSessionTo"];
            TimeBetweenUnfollowsFrom.Value = (int)Settings.Default["TimeBetweenFollowsFrom"];
            TimeBetweenUnfollowsTo.Value = (int)Settings.Default["TimeBetweenFollowsTo"];
        }

        private void SettingsGuideScreen_Load(object sender, EventArgs e)
        {
            //comment test
        }

        private void ContinueButton_Click(object sender, EventArgs e)
        {
            Settings.Default["TimeBetweenSessionsFrom"] = (int)TimeBetweenSessionsFrom.Value;
            Settings.Default["TimeBetweenSessionsTo"] = (int)TimeBetweenSessionsTo.Value;
            Settings.Default["FollowsPerSessionFrom"] = (int)UnfollowsPerSessionFrom.Value;
            Settings.Default["FollowsPerSessionTo"] = (int)UnfollowsPerSessionTo.Value;
            Settings.Default["TimeBetweenFollowsFrom"] = (int)TimeBetweenUnfollowsFrom.Value;
            Settings.Default["TimeBetweenFollowsTo"] = (int)TimeBetweenUnfollowsTo.Value;
            Settings.Default.Save();

            this.Hide();
            if (Globals.unfollowPage3 == null)
                Globals.unfollowPage3 = new UnfollowPage3();

            Globals.unfollowPage3.Show();

        }

        private void HomeButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            Globals.homePage.Show();
        }

        private void TimeBetweenSessionsFrom_ValueChanged(object sender, EventArgs e)
        {
            if (TimeBetweenSessionsTo.Value <= TimeBetweenSessionsFrom.Value)
                TimeBetweenSessionsTo.Value = TimeBetweenSessionsFrom.Value;
        }

        private void TimeBetweenSessionsTo_ValueChanged(object sender, EventArgs e)
        {
            if (TimeBetweenSessionsFrom.Value > TimeBetweenSessionsTo.Value)
                TimeBetweenSessionsFrom.Value = TimeBetweenSessionsTo.Value;
        }

        private void UnfollowsPerSessionFrom_ValueChanged(object sender, EventArgs e)
        {
            if (UnfollowsPerSessionTo.Value < UnfollowsPerSessionFrom.Value)
                UnfollowsPerSessionTo.Value = UnfollowsPerSessionFrom.Value;
        }

        private void UnfollowsPerSessionTo_ValueChanged(object sender, EventArgs e)
        {
            if (UnfollowsPerSessionFrom.Value > UnfollowsPerSessionTo.Value)
                UnfollowsPerSessionFrom.Value = UnfollowsPerSessionTo.Value;
        }

        private void TimeBetweenUnfollowsFrom_ValueChanged(object sender, EventArgs e)
        {
            if (TimeBetweenUnfollowsTo.Value < TimeBetweenUnfollowsFrom.Value)
                TimeBetweenUnfollowsTo.Value = TimeBetweenUnfollowsFrom.Value;
        }

        private void TimeBetweenUnfollowsTo_ValueChanged(object sender, EventArgs e)
        {
            if (TimeBetweenUnfollowsFrom.Value > TimeBetweenUnfollowsTo.Value)
                TimeBetweenUnfollowsFrom.Value = TimeBetweenUnfollowsTo.Value;
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            Utils.ExitProgram();
        }
    }
}
