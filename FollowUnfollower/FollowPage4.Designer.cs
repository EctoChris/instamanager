﻿namespace FollowUnfollower
{
    partial class FollowPage4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FollowPage4));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.logoutButton = new System.Windows.Forms.PictureBox();
            this.HomeButton = new System.Windows.Forms.PictureBox();
            this.FollowSettingsLabel = new System.Windows.Forms.Label();
            this.Step4Label = new System.Windows.Forms.Label();
            this.TipLabel = new System.Windows.Forms.Label();
            this.CurrentTargetAccountsLabel = new System.Windows.Forms.Label();
            this.StartFollowingButton = new System.Windows.Forms.PictureBox();
            this.LinkToFollowPage1 = new System.Windows.Forms.PictureBox();
            this.LinkToFollowPage2 = new System.Windows.Forms.PictureBox();
            this.LinkToFollowPage3 = new System.Windows.Forms.PictureBox();
            this.LinkToFollowPage4 = new System.Windows.Forms.PictureBox();
            this.Bar1 = new System.Windows.Forms.PictureBox();
            this.Bar2 = new System.Windows.Forms.PictureBox();
            this.Bar3 = new System.Windows.Forms.PictureBox();
            this.DepletedTargetAccountsLabel = new System.Windows.Forms.Label();
            this.CurrentTargetAccountsListBox = new System.Windows.Forms.ListBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.AddMoreTargetAccountsButton = new System.Windows.Forms.Button();
            this.ClearDepletedAccountsButton = new System.Windows.Forms.Button();
            this.ClearTargets = new System.Windows.Forms.Button();
            this.InvalidAccountsMessage = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoutButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HomeButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartFollowingButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar3)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::FollowUnfollower.Properties.Resources.instaGradientRect;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(100, 100);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(636, 377);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // logoutButton
            // 
            this.logoutButton.BackgroundImage = global::FollowUnfollower.Properties.Resources.logoutButton;
            this.logoutButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.logoutButton.Location = new System.Drawing.Point(676, 22);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(126, 49);
            this.logoutButton.TabIndex = 2;
            this.logoutButton.TabStop = false;
            // 
            // HomeButton
            // 
            this.HomeButton.BackgroundImage = global::FollowUnfollower.Properties.Resources.FollowGramLogo;
            this.HomeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.HomeButton.Location = new System.Drawing.Point(12, -3);
            this.HomeButton.Name = "HomeButton";
            this.HomeButton.Size = new System.Drawing.Size(103, 97);
            this.HomeButton.TabIndex = 3;
            this.HomeButton.TabStop = false;
            this.HomeButton.Click += new System.EventHandler(this.HomeButton_Click);
            // 
            // FollowSettingsLabel
            // 
            this.FollowSettingsLabel.AutoSize = true;
            this.FollowSettingsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.25F);
            this.FollowSettingsLabel.Location = new System.Drawing.Point(320, 74);
            this.FollowSettingsLabel.Name = "FollowSettingsLabel";
            this.FollowSettingsLabel.Size = new System.Drawing.Size(187, 30);
            this.FollowSettingsLabel.TabIndex = 4;
            this.FollowSettingsLabel.Text = "Follow Settings";
            // 
            // Step4Label
            // 
            this.Step4Label.AutoSize = true;
            this.Step4Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.Step4Label.Location = new System.Drawing.Point(374, 126);
            this.Step4Label.Name = "Step4Label";
            this.Step4Label.Size = new System.Drawing.Size(81, 26);
            this.Step4Label.TabIndex = 5;
            this.Step4Label.Text = "Step 4:";
            // 
            // TipLabel
            // 
            this.TipLabel.AutoSize = true;
            this.TipLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.TipLabel.Location = new System.Drawing.Point(200, 164);
            this.TipLabel.Name = "TipLabel";
            this.TipLabel.Size = new System.Drawing.Size(465, 18);
            this.TipLabel.TabIndex = 6;
            this.TipLabel.Text = "Tip: Add the names of accounts similar to yours with a large following.";
            // 
            // CurrentTargetAccountsLabel
            // 
            this.CurrentTargetAccountsLabel.AutoSize = true;
            this.CurrentTargetAccountsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.CurrentTargetAccountsLabel.Location = new System.Drawing.Point(147, 195);
            this.CurrentTargetAccountsLabel.Name = "CurrentTargetAccountsLabel";
            this.CurrentTargetAccountsLabel.Size = new System.Drawing.Size(220, 24);
            this.CurrentTargetAccountsLabel.TabIndex = 7;
            this.CurrentTargetAccountsLabel.Text = "Current Target Accounts:";
            // 
            // StartFollowingButton
            // 
            this.StartFollowingButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("StartFollowingButton.BackgroundImage")));
            this.StartFollowingButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.StartFollowingButton.Location = new System.Drawing.Point(256, 384);
            this.StartFollowingButton.Name = "StartFollowingButton";
            this.StartFollowingButton.Size = new System.Drawing.Size(314, 45);
            this.StartFollowingButton.TabIndex = 15;
            this.StartFollowingButton.TabStop = false;
            this.StartFollowingButton.Click += new System.EventHandler(this.ContinueButton_Click);
            // 
            // LinkToFollowPage1
            // 
            this.LinkToFollowPage1.BackgroundImage = global::FollowUnfollower.Properties.Resources.Export_Page;
            this.LinkToFollowPage1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LinkToFollowPage1.Location = new System.Drawing.Point(218, 469);
            this.LinkToFollowPage1.Name = "LinkToFollowPage1";
            this.LinkToFollowPage1.Size = new System.Drawing.Size(55, 55);
            this.LinkToFollowPage1.TabIndex = 16;
            this.LinkToFollowPage1.TabStop = false;
            this.LinkToFollowPage1.Click += new System.EventHandler(this.LinkToFollowPage1_Click);
            // 
            // LinkToFollowPage2
            // 
            this.LinkToFollowPage2.BackgroundImage = global::FollowUnfollower.Properties.Resources.Coloured2;
            this.LinkToFollowPage2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LinkToFollowPage2.Location = new System.Drawing.Point(338, 469);
            this.LinkToFollowPage2.Name = "LinkToFollowPage2";
            this.LinkToFollowPage2.Size = new System.Drawing.Size(55, 55);
            this.LinkToFollowPage2.TabIndex = 17;
            this.LinkToFollowPage2.TabStop = false;
            this.LinkToFollowPage2.Click += new System.EventHandler(this.LinkToFollowPage2_Click);
            // 
            // LinkToFollowPage3
            // 
            this.LinkToFollowPage3.BackgroundImage = global::FollowUnfollower.Properties.Resources.Coloured3;
            this.LinkToFollowPage3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LinkToFollowPage3.Location = new System.Drawing.Point(440, 469);
            this.LinkToFollowPage3.Name = "LinkToFollowPage3";
            this.LinkToFollowPage3.Size = new System.Drawing.Size(55, 55);
            this.LinkToFollowPage3.TabIndex = 18;
            this.LinkToFollowPage3.TabStop = false;
            this.LinkToFollowPage3.Click += new System.EventHandler(this.LinkToFollowPage3_Click);
            // 
            // LinkToFollowPage4
            // 
            this.LinkToFollowPage4.BackgroundImage = global::FollowUnfollower.Properties.Resources.Coloured4;
            this.LinkToFollowPage4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LinkToFollowPage4.Location = new System.Drawing.Point(553, 469);
            this.LinkToFollowPage4.Name = "LinkToFollowPage4";
            this.LinkToFollowPage4.Size = new System.Drawing.Size(55, 55);
            this.LinkToFollowPage4.TabIndex = 19;
            this.LinkToFollowPage4.TabStop = false;
            // 
            // Bar1
            // 
            this.Bar1.BackgroundImage = global::FollowUnfollower.Properties.Resources.ColouredBar1To2;
            this.Bar1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Bar1.Location = new System.Drawing.Point(256, 493);
            this.Bar1.Name = "Bar1";
            this.Bar1.Size = new System.Drawing.Size(109, 14);
            this.Bar1.TabIndex = 20;
            this.Bar1.TabStop = false;
            // 
            // Bar2
            // 
            this.Bar2.BackgroundImage = global::FollowUnfollower.Properties.Resources.ColouredBar2To3;
            this.Bar2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Bar2.Location = new System.Drawing.Point(371, 493);
            this.Bar2.Name = "Bar2";
            this.Bar2.Size = new System.Drawing.Size(109, 14);
            this.Bar2.TabIndex = 21;
            this.Bar2.TabStop = false;
            // 
            // Bar3
            // 
            this.Bar3.BackgroundImage = global::FollowUnfollower.Properties.Resources.ColouredBar3To4;
            this.Bar3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Bar3.Location = new System.Drawing.Point(474, 493);
            this.Bar3.Name = "Bar3";
            this.Bar3.Size = new System.Drawing.Size(109, 14);
            this.Bar3.TabIndex = 22;
            this.Bar3.TabStop = false;
            // 
            // DepletedTargetAccountsLabel
            // 
            this.DepletedTargetAccountsLabel.AutoSize = true;
            this.DepletedTargetAccountsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.DepletedTargetAccountsLabel.Location = new System.Drawing.Point(439, 195);
            this.DepletedTargetAccountsLabel.Name = "DepletedTargetAccountsLabel";
            this.DepletedTargetAccountsLabel.Size = new System.Drawing.Size(229, 24);
            this.DepletedTargetAccountsLabel.TabIndex = 23;
            this.DepletedTargetAccountsLabel.Text = "Depleted Target Accounts";
            // 
            // CurrentTargetAccountsListBox
            // 
            this.CurrentTargetAccountsListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.CurrentTargetAccountsListBox.FormattingEnabled = true;
            this.CurrentTargetAccountsListBox.ItemHeight = 18;
            this.CurrentTargetAccountsListBox.Location = new System.Drawing.Point(149, 222);
            this.CurrentTargetAccountsListBox.Name = "CurrentTargetAccountsListBox";
            this.CurrentTargetAccountsListBox.Size = new System.Drawing.Size(242, 112);
            this.CurrentTargetAccountsListBox.TabIndex = 24;
            // 
            // listBox2
            // 
            this.listBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.listBox2.FormattingEnabled = true;
            this.listBox2.ItemHeight = 18;
            this.listBox2.Location = new System.Drawing.Point(440, 222);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(242, 112);
            this.listBox2.TabIndex = 25;
            // 
            // AddMoreTargetAccountsButton
            // 
            this.AddMoreTargetAccountsButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.AddMoreTargetAccountsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddMoreTargetAccountsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.AddMoreTargetAccountsButton.Location = new System.Drawing.Point(149, 336);
            this.AddMoreTargetAccountsButton.Name = "AddMoreTargetAccountsButton";
            this.AddMoreTargetAccountsButton.Size = new System.Drawing.Size(114, 42);
            this.AddMoreTargetAccountsButton.TabIndex = 26;
            this.AddMoreTargetAccountsButton.Text = "Add Targets";
            this.AddMoreTargetAccountsButton.UseVisualStyleBackColor = true;
            this.AddMoreTargetAccountsButton.Click += new System.EventHandler(this.AddMoreTargetAccountsButton_Click);
            // 
            // ClearDepletedAccountsButton
            // 
            this.ClearDepletedAccountsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ClearDepletedAccountsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.ClearDepletedAccountsButton.Location = new System.Drawing.Point(440, 336);
            this.ClearDepletedAccountsButton.Name = "ClearDepletedAccountsButton";
            this.ClearDepletedAccountsButton.Size = new System.Drawing.Size(242, 42);
            this.ClearDepletedAccountsButton.TabIndex = 27;
            this.ClearDepletedAccountsButton.Text = "Clear All Depleted Accounts";
            this.ClearDepletedAccountsButton.UseVisualStyleBackColor = true;
            // 
            // ClearTargets
            // 
            this.ClearTargets.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ClearTargets.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.ClearTargets.Location = new System.Drawing.Point(269, 336);
            this.ClearTargets.Name = "ClearTargets";
            this.ClearTargets.Size = new System.Drawing.Size(122, 42);
            this.ClearTargets.TabIndex = 28;
            this.ClearTargets.Text = "Clear Targets";
            this.ClearTargets.UseVisualStyleBackColor = true;
            this.ClearTargets.Click += new System.EventHandler(this.ClearTargets_Click);
            // 
            // InvalidAccountsMessage
            // 
            this.InvalidAccountsMessage.AutoSize = true;
            this.InvalidAccountsMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InvalidAccountsMessage.ForeColor = System.Drawing.Color.Red;
            this.InvalidAccountsMessage.Location = new System.Drawing.Point(290, 533);
            this.InvalidAccountsMessage.Name = "InvalidAccountsMessage";
            this.InvalidAccountsMessage.Size = new System.Drawing.Size(251, 18);
            this.InvalidAccountsMessage.TabIndex = 29;
            this.InvalidAccountsMessage.Text = "You must enter a valid target account";
            this.InvalidAccountsMessage.Visible = false;
            // 
            // FollowPage4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 560);
            this.Controls.Add(this.InvalidAccountsMessage);
            this.Controls.Add(this.ClearTargets);
            this.Controls.Add(this.ClearDepletedAccountsButton);
            this.Controls.Add(this.AddMoreTargetAccountsButton);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.CurrentTargetAccountsListBox);
            this.Controls.Add(this.DepletedTargetAccountsLabel);
            this.Controls.Add(this.LinkToFollowPage4);
            this.Controls.Add(this.LinkToFollowPage3);
            this.Controls.Add(this.LinkToFollowPage2);
            this.Controls.Add(this.LinkToFollowPage1);
            this.Controls.Add(this.StartFollowingButton);
            this.Controls.Add(this.CurrentTargetAccountsLabel);
            this.Controls.Add(this.TipLabel);
            this.Controls.Add(this.Step4Label);
            this.Controls.Add(this.FollowSettingsLabel);
            this.Controls.Add(this.HomeButton);
            this.Controls.Add(this.logoutButton);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Bar1);
            this.Controls.Add(this.Bar2);
            this.Controls.Add(this.Bar3);
            this.Name = "FollowPage4";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Follow Page 1";
            this.Load += new System.EventHandler(this.SettingsGuideScreen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoutButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HomeButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartFollowingButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox logoutButton;
        private System.Windows.Forms.PictureBox HomeButton;
        private System.Windows.Forms.Label FollowSettingsLabel;
        private System.Windows.Forms.Label Step4Label;
        private System.Windows.Forms.Label TipLabel;
        private System.Windows.Forms.Label CurrentTargetAccountsLabel;
        private System.Windows.Forms.PictureBox StartFollowingButton;
        private System.Windows.Forms.PictureBox LinkToFollowPage1;
        private System.Windows.Forms.PictureBox LinkToFollowPage2;
        private System.Windows.Forms.PictureBox LinkToFollowPage3;
        private System.Windows.Forms.PictureBox LinkToFollowPage4;
        private System.Windows.Forms.PictureBox Bar1;
        private System.Windows.Forms.PictureBox Bar2;
        private System.Windows.Forms.PictureBox Bar3;
        private System.Windows.Forms.Label DepletedTargetAccountsLabel;
        private System.Windows.Forms.ListBox CurrentTargetAccountsListBox;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Button AddMoreTargetAccountsButton;
        private System.Windows.Forms.Button ClearDepletedAccountsButton;
        private System.Windows.Forms.Button ClearTargets;
        private System.Windows.Forms.Label InvalidAccountsMessage;
    }
}