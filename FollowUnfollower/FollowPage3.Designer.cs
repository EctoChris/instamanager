﻿namespace FollowUnfollower
{
    partial class FollowPage3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.logoutButton = new System.Windows.Forms.PictureBox();
            this.HomeButton = new System.Windows.Forms.PictureBox();
            this.FollowSettingsLabel = new System.Windows.Forms.Label();
            this.Step3Label = new System.Windows.Forms.Label();
            this.WarningLabel = new System.Windows.Forms.Label();
            this.ContinueButton = new System.Windows.Forms.PictureBox();
            this.LinkToFollowPage1 = new System.Windows.Forms.PictureBox();
            this.LinkToFollowPage2 = new System.Windows.Forms.PictureBox();
            this.LinkToFollowPage3 = new System.Windows.Forms.PictureBox();
            this.LinkToFollowPage4 = new System.Windows.Forms.PictureBox();
            this.Bar1 = new System.Windows.Forms.PictureBox();
            this.Bar2 = new System.Windows.Forms.PictureBox();
            this.Bar3 = new System.Windows.Forms.PictureBox();
            this.WarningContentLabel = new System.Windows.Forms.Label();
            this.MaxDailyFollowsLabel = new System.Windows.Forms.Label();
            this.MaxDailyFollowsTrackBar = new System.Windows.Forms.TrackBar();
            this.MinTrackBarLabel = new System.Windows.Forms.Label();
            this.MaxTrackbarLabel = new System.Windows.Forms.Label();
            this.MaxDailyAmount = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoutButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HomeButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContinueButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxDailyFollowsTrackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::FollowUnfollower.Properties.Resources.instaGradientRect;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(100, 100);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(636, 377);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // logoutButton
            // 
            this.logoutButton.BackgroundImage = global::FollowUnfollower.Properties.Resources.logoutButton;
            this.logoutButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.logoutButton.Location = new System.Drawing.Point(676, 22);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(126, 49);
            this.logoutButton.TabIndex = 2;
            this.logoutButton.TabStop = false;
            this.logoutButton.Click += new System.EventHandler(this.logoutButton_Click);
            // 
            // HomeButton
            // 
            this.HomeButton.BackgroundImage = global::FollowUnfollower.Properties.Resources.FollowGramLogo;
            this.HomeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.HomeButton.Location = new System.Drawing.Point(12, -3);
            this.HomeButton.Name = "HomeButton";
            this.HomeButton.Size = new System.Drawing.Size(103, 97);
            this.HomeButton.TabIndex = 3;
            this.HomeButton.TabStop = false;
            this.HomeButton.Click += new System.EventHandler(this.HomeButton_Click);
            // 
            // FollowSettingsLabel
            // 
            this.FollowSettingsLabel.AutoSize = true;
            this.FollowSettingsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.25F);
            this.FollowSettingsLabel.Location = new System.Drawing.Point(311, 70);
            this.FollowSettingsLabel.Name = "FollowSettingsLabel";
            this.FollowSettingsLabel.Size = new System.Drawing.Size(187, 30);
            this.FollowSettingsLabel.TabIndex = 4;
            this.FollowSettingsLabel.Text = "Follow Settings";
            // 
            // Step3Label
            // 
            this.Step3Label.AutoSize = true;
            this.Step3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.Step3Label.Location = new System.Drawing.Point(374, 126);
            this.Step3Label.Name = "Step3Label";
            this.Step3Label.Size = new System.Drawing.Size(81, 26);
            this.Step3Label.TabIndex = 5;
            this.Step3Label.Text = "Step 3:";
            // 
            // WarningLabel
            // 
            this.WarningLabel.AutoSize = true;
            this.WarningLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.WarningLabel.ForeColor = System.Drawing.Color.Red;
            this.WarningLabel.Location = new System.Drawing.Point(199, 164);
            this.WarningLabel.Name = "WarningLabel";
            this.WarningLabel.Size = new System.Drawing.Size(92, 18);
            this.WarningLabel.TabIndex = 6;
            this.WarningLabel.Text = "WARNING:";
            // 
            // ContinueButton
            // 
            this.ContinueButton.BackgroundImage = global::FollowUnfollower.Properties.Resources.Group__1_;
            this.ContinueButton.Location = new System.Drawing.Point(256, 384);
            this.ContinueButton.Name = "ContinueButton";
            this.ContinueButton.Size = new System.Drawing.Size(314, 45);
            this.ContinueButton.TabIndex = 15;
            this.ContinueButton.TabStop = false;
            this.ContinueButton.Click += new System.EventHandler(this.ContinueButton_Click);
            // 
            // LinkToFollowPage1
            // 
            this.LinkToFollowPage1.BackgroundImage = global::FollowUnfollower.Properties.Resources.Export_Page;
            this.LinkToFollowPage1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LinkToFollowPage1.Location = new System.Drawing.Point(218, 469);
            this.LinkToFollowPage1.Name = "LinkToFollowPage1";
            this.LinkToFollowPage1.Size = new System.Drawing.Size(55, 55);
            this.LinkToFollowPage1.TabIndex = 16;
            this.LinkToFollowPage1.TabStop = false;
            this.LinkToFollowPage1.Click += new System.EventHandler(this.LinkToFollowPage1_Click);
            // 
            // LinkToFollowPage2
            // 
            this.LinkToFollowPage2.BackgroundImage = global::FollowUnfollower.Properties.Resources.Coloured2;
            this.LinkToFollowPage2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LinkToFollowPage2.Location = new System.Drawing.Point(338, 469);
            this.LinkToFollowPage2.Name = "LinkToFollowPage2";
            this.LinkToFollowPage2.Size = new System.Drawing.Size(55, 55);
            this.LinkToFollowPage2.TabIndex = 17;
            this.LinkToFollowPage2.TabStop = false;
            this.LinkToFollowPage2.Click += new System.EventHandler(this.LinkToFollowPage2_Click);
            // 
            // LinkToFollowPage3
            // 
            this.LinkToFollowPage3.BackgroundImage = global::FollowUnfollower.Properties.Resources.Coloured3;
            this.LinkToFollowPage3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LinkToFollowPage3.Location = new System.Drawing.Point(440, 469);
            this.LinkToFollowPage3.Name = "LinkToFollowPage3";
            this.LinkToFollowPage3.Size = new System.Drawing.Size(55, 55);
            this.LinkToFollowPage3.TabIndex = 18;
            this.LinkToFollowPage3.TabStop = false;
            // 
            // LinkToFollowPage4
            // 
            this.LinkToFollowPage4.BackgroundImage = global::FollowUnfollower.Properties.Resources.Grey4;
            this.LinkToFollowPage4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LinkToFollowPage4.Location = new System.Drawing.Point(553, 469);
            this.LinkToFollowPage4.Name = "LinkToFollowPage4";
            this.LinkToFollowPage4.Size = new System.Drawing.Size(55, 55);
            this.LinkToFollowPage4.TabIndex = 19;
            this.LinkToFollowPage4.TabStop = false;
            // 
            // Bar1
            // 
            this.Bar1.BackgroundImage = global::FollowUnfollower.Properties.Resources.ColouredBar1To2;
            this.Bar1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Bar1.Location = new System.Drawing.Point(256, 493);
            this.Bar1.Name = "Bar1";
            this.Bar1.Size = new System.Drawing.Size(109, 14);
            this.Bar1.TabIndex = 20;
            this.Bar1.TabStop = false;
            // 
            // Bar2
            // 
            this.Bar2.BackgroundImage = global::FollowUnfollower.Properties.Resources.ColouredBar2To3;
            this.Bar2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Bar2.Location = new System.Drawing.Point(371, 493);
            this.Bar2.Name = "Bar2";
            this.Bar2.Size = new System.Drawing.Size(109, 14);
            this.Bar2.TabIndex = 21;
            this.Bar2.TabStop = false;
            // 
            // Bar3
            // 
            this.Bar3.BackgroundImage = global::FollowUnfollower.Properties.Resources.EmptyBar;
            this.Bar3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Bar3.Location = new System.Drawing.Point(474, 493);
            this.Bar3.Name = "Bar3";
            this.Bar3.Size = new System.Drawing.Size(109, 14);
            this.Bar3.TabIndex = 22;
            this.Bar3.TabStop = false;
            // 
            // WarningContentLabel
            // 
            this.WarningContentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.WarningContentLabel.ForeColor = System.Drawing.Color.Black;
            this.WarningContentLabel.Location = new System.Drawing.Point(199, 164);
            this.WarningContentLabel.Name = "WarningContentLabel";
            this.WarningContentLabel.Size = new System.Drawing.Size(458, 80);
            this.WarningContentLabel.TabIndex = 23;
            this.WarningContentLabel.Text = "                      Don\'t increase this setting too quickly ! You risk your acc" +
    "ount being permanently banned. For new accounts, start at 50 and gradually incre" +
    "ase.";
            // 
            // MaxDailyFollowsLabel
            // 
            this.MaxDailyFollowsLabel.AutoSize = true;
            this.MaxDailyFollowsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.MaxDailyFollowsLabel.Location = new System.Drawing.Point(323, 232);
            this.MaxDailyFollowsLabel.Name = "MaxDailyFollowsLabel";
            this.MaxDailyFollowsLabel.Size = new System.Drawing.Size(188, 26);
            this.MaxDailyFollowsLabel.TabIndex = 24;
            this.MaxDailyFollowsLabel.Text = "Max Daily Follows";
            // 
            // MaxDailyFollowsTrackBar
            // 
            this.MaxDailyFollowsTrackBar.LargeChange = 1;
            this.MaxDailyFollowsTrackBar.Location = new System.Drawing.Point(252, 317);
            this.MaxDailyFollowsTrackBar.Maximum = 1000;
            this.MaxDailyFollowsTrackBar.Name = "MaxDailyFollowsTrackBar";
            this.MaxDailyFollowsTrackBar.Size = new System.Drawing.Size(314, 45);
            this.MaxDailyFollowsTrackBar.TabIndex = 26;
            this.MaxDailyFollowsTrackBar.TickFrequency = 100;
            this.MaxDailyFollowsTrackBar.Scroll += new System.EventHandler(this.MaxDailyFollowsTrackBar_Scroll);
            // 
            // MinTrackBarLabel
            // 
            this.MinTrackBarLabel.AutoSize = true;
            this.MinTrackBarLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.MinTrackBarLabel.Location = new System.Drawing.Point(222, 317);
            this.MinTrackBarLabel.Name = "MinTrackBarLabel";
            this.MinTrackBarLabel.Size = new System.Drawing.Size(24, 26);
            this.MinTrackBarLabel.TabIndex = 27;
            this.MinTrackBarLabel.Text = "0";
            // 
            // MaxTrackbarLabel
            // 
            this.MaxTrackbarLabel.AutoSize = true;
            this.MaxTrackbarLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.MaxTrackbarLabel.Location = new System.Drawing.Point(572, 317);
            this.MaxTrackbarLabel.Name = "MaxTrackbarLabel";
            this.MaxTrackbarLabel.Size = new System.Drawing.Size(60, 26);
            this.MaxTrackbarLabel.TabIndex = 28;
            this.MaxTrackbarLabel.Text = "1000";
            // 
            // MaxDailyAmount
            // 
            this.MaxDailyAmount.AutoSize = true;
            this.MaxDailyAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F);
            this.MaxDailyAmount.Location = new System.Drawing.Point(400, 277);
            this.MaxDailyAmount.Name = "MaxDailyAmount";
            this.MaxDailyAmount.Size = new System.Drawing.Size(29, 31);
            this.MaxDailyAmount.TabIndex = 29;
            this.MaxDailyAmount.Text = "0";
            // 
            // FollowPage3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 560);
            this.Controls.Add(this.MaxDailyAmount);
            this.Controls.Add(this.MaxTrackbarLabel);
            this.Controls.Add(this.MinTrackBarLabel);
            this.Controls.Add(this.MaxDailyFollowsTrackBar);
            this.Controls.Add(this.MaxDailyFollowsLabel);
            this.Controls.Add(this.WarningLabel);
            this.Controls.Add(this.WarningContentLabel);
            this.Controls.Add(this.LinkToFollowPage4);
            this.Controls.Add(this.LinkToFollowPage3);
            this.Controls.Add(this.LinkToFollowPage2);
            this.Controls.Add(this.LinkToFollowPage1);
            this.Controls.Add(this.ContinueButton);
            this.Controls.Add(this.Step3Label);
            this.Controls.Add(this.FollowSettingsLabel);
            this.Controls.Add(this.HomeButton);
            this.Controls.Add(this.logoutButton);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Bar1);
            this.Controls.Add(this.Bar2);
            this.Controls.Add(this.Bar3);
            this.Name = "FollowPage3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Follow Page 1";
            this.Load += new System.EventHandler(this.SettingsGuideScreen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoutButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HomeButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContinueButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxDailyFollowsTrackBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox logoutButton;
        private System.Windows.Forms.PictureBox HomeButton;
        private System.Windows.Forms.Label FollowSettingsLabel;
        private System.Windows.Forms.Label Step3Label;
        private System.Windows.Forms.Label WarningLabel;
        private System.Windows.Forms.PictureBox ContinueButton;
        private System.Windows.Forms.PictureBox LinkToFollowPage1;
        private System.Windows.Forms.PictureBox LinkToFollowPage2;
        private System.Windows.Forms.PictureBox LinkToFollowPage3;
        private System.Windows.Forms.PictureBox LinkToFollowPage4;
        private System.Windows.Forms.PictureBox Bar1;
        private System.Windows.Forms.PictureBox Bar2;
        private System.Windows.Forms.PictureBox Bar3;
        private System.Windows.Forms.Label WarningContentLabel;
        private System.Windows.Forms.Label MaxDailyFollowsLabel;
        private System.Windows.Forms.TrackBar MaxDailyFollowsTrackBar;
        private System.Windows.Forms.Label MinTrackBarLabel;
        private System.Windows.Forms.Label MaxTrackbarLabel;
        private System.Windows.Forms.Label MaxDailyAmount;
    }
}