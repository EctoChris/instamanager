﻿namespace FollowUnfollower
{
    partial class UnfollowPage4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.logoutButton = new System.Windows.Forms.PictureBox();
            this.HomeButton = new System.Windows.Forms.PictureBox();
            this.UnfollowSettingsLabel = new System.Windows.Forms.Label();
            this.Step4Label = new System.Windows.Forms.Label();
            this.TipLabel = new System.Windows.Forms.Label();
            this.IgnoreListLabel = new System.Windows.Forms.Label();
            this.StartFollowingButton = new System.Windows.Forms.PictureBox();
            this.LinkToFollowPage1 = new System.Windows.Forms.PictureBox();
            this.LinkToFollowPage2 = new System.Windows.Forms.PictureBox();
            this.LinkToFollowPage3 = new System.Windows.Forms.PictureBox();
            this.LinkToFollowPage4 = new System.Windows.Forms.PictureBox();
            this.Bar1 = new System.Windows.Forms.PictureBox();
            this.Bar2 = new System.Windows.Forms.PictureBox();
            this.Bar3 = new System.Windows.Forms.PictureBox();
            this.IgnoreListListBox = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.ClearDepletedAccountsButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoutButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HomeButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartFollowingButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar3)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::FollowUnfollower.Properties.Resources.instaGradientRect;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(100, 100);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(636, 377);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // logoutButton
            // 
            this.logoutButton.BackgroundImage = global::FollowUnfollower.Properties.Resources.logoutButton;
            this.logoutButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.logoutButton.Location = new System.Drawing.Point(676, 22);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(126, 49);
            this.logoutButton.TabIndex = 2;
            this.logoutButton.TabStop = false;
            // 
            // HomeButton
            // 
            this.HomeButton.BackgroundImage = global::FollowUnfollower.Properties.Resources.FollowGramLogo;
            this.HomeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.HomeButton.Location = new System.Drawing.Point(12, -3);
            this.HomeButton.Name = "HomeButton";
            this.HomeButton.Size = new System.Drawing.Size(103, 97);
            this.HomeButton.TabIndex = 3;
            this.HomeButton.TabStop = false;
            this.HomeButton.Click += new System.EventHandler(this.HomeButton_Click);
            // 
            // UnfollowSettingsLabel
            // 
            this.UnfollowSettingsLabel.AutoSize = true;
            this.UnfollowSettingsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.25F);
            this.UnfollowSettingsLabel.Location = new System.Drawing.Point(320, 74);
            this.UnfollowSettingsLabel.Name = "UnfollowSettingsLabel";
            this.UnfollowSettingsLabel.Size = new System.Drawing.Size(211, 30);
            this.UnfollowSettingsLabel.TabIndex = 4;
            this.UnfollowSettingsLabel.Text = "Unfollow Settings";
            // 
            // Step4Label
            // 
            this.Step4Label.AutoSize = true;
            this.Step4Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.Step4Label.Location = new System.Drawing.Point(374, 126);
            this.Step4Label.Name = "Step4Label";
            this.Step4Label.Size = new System.Drawing.Size(81, 26);
            this.Step4Label.TabIndex = 5;
            this.Step4Label.Text = "Step 4:";
            // 
            // TipLabel
            // 
            this.TipLabel.AutoSize = true;
            this.TipLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.TipLabel.Location = new System.Drawing.Point(173, 165);
            this.TipLabel.Name = "TipLabel";
            this.TipLabel.Size = new System.Drawing.Size(480, 18);
            this.TipLabel.TabIndex = 6;
            this.TipLabel.Text = "Tip: The ignore list is a list of accounts that you DON\'T want to unfollow.";
            // 
            // IgnoreListLabel
            // 
            this.IgnoreListLabel.AutoSize = true;
            this.IgnoreListLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.IgnoreListLabel.Location = new System.Drawing.Point(365, 195);
            this.IgnoreListLabel.Name = "IgnoreListLabel";
            this.IgnoreListLabel.Size = new System.Drawing.Size(96, 24);
            this.IgnoreListLabel.TabIndex = 7;
            this.IgnoreListLabel.Text = "Ignore List";
            // 
            // StartFollowingButton
            // 
            this.StartFollowingButton.BackgroundImage = global::FollowUnfollower.Properties.Resources.startUnfollowingButton;
            this.StartFollowingButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.StartFollowingButton.Location = new System.Drawing.Point(294, 384);
            this.StartFollowingButton.Name = "StartFollowingButton";
            this.StartFollowingButton.Size = new System.Drawing.Size(239, 57);
            this.StartFollowingButton.TabIndex = 15;
            this.StartFollowingButton.TabStop = false;
            this.StartFollowingButton.Click += new System.EventHandler(this.ContinueButton_Click);
            // 
            // LinkToFollowPage1
            // 
            this.LinkToFollowPage1.BackgroundImage = global::FollowUnfollower.Properties.Resources.Export_Page;
            this.LinkToFollowPage1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LinkToFollowPage1.Location = new System.Drawing.Point(218, 469);
            this.LinkToFollowPage1.Name = "LinkToFollowPage1";
            this.LinkToFollowPage1.Size = new System.Drawing.Size(55, 55);
            this.LinkToFollowPage1.TabIndex = 16;
            this.LinkToFollowPage1.TabStop = false;
            // 
            // LinkToFollowPage2
            // 
            this.LinkToFollowPage2.BackgroundImage = global::FollowUnfollower.Properties.Resources.Coloured2;
            this.LinkToFollowPage2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LinkToFollowPage2.Location = new System.Drawing.Point(338, 469);
            this.LinkToFollowPage2.Name = "LinkToFollowPage2";
            this.LinkToFollowPage2.Size = new System.Drawing.Size(55, 55);
            this.LinkToFollowPage2.TabIndex = 17;
            this.LinkToFollowPage2.TabStop = false;
            // 
            // LinkToFollowPage3
            // 
            this.LinkToFollowPage3.BackgroundImage = global::FollowUnfollower.Properties.Resources.Coloured3;
            this.LinkToFollowPage3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LinkToFollowPage3.Location = new System.Drawing.Point(440, 469);
            this.LinkToFollowPage3.Name = "LinkToFollowPage3";
            this.LinkToFollowPage3.Size = new System.Drawing.Size(55, 55);
            this.LinkToFollowPage3.TabIndex = 18;
            this.LinkToFollowPage3.TabStop = false;
            // 
            // LinkToFollowPage4
            // 
            this.LinkToFollowPage4.BackgroundImage = global::FollowUnfollower.Properties.Resources.Coloured4;
            this.LinkToFollowPage4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LinkToFollowPage4.Location = new System.Drawing.Point(553, 469);
            this.LinkToFollowPage4.Name = "LinkToFollowPage4";
            this.LinkToFollowPage4.Size = new System.Drawing.Size(55, 55);
            this.LinkToFollowPage4.TabIndex = 19;
            this.LinkToFollowPage4.TabStop = false;
            // 
            // Bar1
            // 
            this.Bar1.BackgroundImage = global::FollowUnfollower.Properties.Resources.ColouredBar1To2;
            this.Bar1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Bar1.Location = new System.Drawing.Point(256, 493);
            this.Bar1.Name = "Bar1";
            this.Bar1.Size = new System.Drawing.Size(109, 14);
            this.Bar1.TabIndex = 20;
            this.Bar1.TabStop = false;
            // 
            // Bar2
            // 
            this.Bar2.BackgroundImage = global::FollowUnfollower.Properties.Resources.ColouredBar2To3;
            this.Bar2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Bar2.Location = new System.Drawing.Point(371, 493);
            this.Bar2.Name = "Bar2";
            this.Bar2.Size = new System.Drawing.Size(109, 14);
            this.Bar2.TabIndex = 21;
            this.Bar2.TabStop = false;
            // 
            // Bar3
            // 
            this.Bar3.BackgroundImage = global::FollowUnfollower.Properties.Resources.ColouredBar3To4;
            this.Bar3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Bar3.Location = new System.Drawing.Point(474, 493);
            this.Bar3.Name = "Bar3";
            this.Bar3.Size = new System.Drawing.Size(109, 14);
            this.Bar3.TabIndex = 22;
            this.Bar3.TabStop = false;
            // 
            // IgnoreListListBox
            // 
            this.IgnoreListListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.IgnoreListListBox.FormattingEnabled = true;
            this.IgnoreListListBox.ItemHeight = 18;
            this.IgnoreListListBox.Location = new System.Drawing.Point(241, 222);
            this.IgnoreListListBox.Name = "IgnoreListListBox";
            this.IgnoreListListBox.Size = new System.Drawing.Size(345, 112);
            this.IgnoreListListBox.TabIndex = 25;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.button1.Location = new System.Drawing.Point(241, 336);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(164, 42);
            this.button1.TabIndex = 26;
            this.button1.Text = "+ Add more accounts";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // ClearDepletedAccountsButton
            // 
            this.ClearDepletedAccountsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.ClearDepletedAccountsButton.Location = new System.Drawing.Point(411, 336);
            this.ClearDepletedAccountsButton.Name = "ClearDepletedAccountsButton";
            this.ClearDepletedAccountsButton.Size = new System.Drawing.Size(175, 42);
            this.ClearDepletedAccountsButton.TabIndex = 27;
            this.ClearDepletedAccountsButton.Text = "Delete selected account";
            this.ClearDepletedAccountsButton.UseVisualStyleBackColor = true;
            // 
            // UnfollowPage4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 560);
            this.Controls.Add(this.ClearDepletedAccountsButton);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.IgnoreListListBox);
            this.Controls.Add(this.LinkToFollowPage4);
            this.Controls.Add(this.LinkToFollowPage3);
            this.Controls.Add(this.LinkToFollowPage2);
            this.Controls.Add(this.LinkToFollowPage1);
            this.Controls.Add(this.StartFollowingButton);
            this.Controls.Add(this.IgnoreListLabel);
            this.Controls.Add(this.TipLabel);
            this.Controls.Add(this.Step4Label);
            this.Controls.Add(this.UnfollowSettingsLabel);
            this.Controls.Add(this.HomeButton);
            this.Controls.Add(this.logoutButton);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Bar1);
            this.Controls.Add(this.Bar2);
            this.Controls.Add(this.Bar3);
            this.Name = "UnfollowPage4";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Follow Page 1";
            this.Load += new System.EventHandler(this.SettingsGuideScreen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoutButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HomeButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartFollowingButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox logoutButton;
        private System.Windows.Forms.PictureBox HomeButton;
        private System.Windows.Forms.Label UnfollowSettingsLabel;
        private System.Windows.Forms.Label Step4Label;
        private System.Windows.Forms.Label TipLabel;
        private System.Windows.Forms.Label IgnoreListLabel;
        private System.Windows.Forms.PictureBox StartFollowingButton;
        private System.Windows.Forms.PictureBox LinkToFollowPage1;
        private System.Windows.Forms.PictureBox LinkToFollowPage2;
        private System.Windows.Forms.PictureBox LinkToFollowPage3;
        private System.Windows.Forms.PictureBox LinkToFollowPage4;
        private System.Windows.Forms.PictureBox Bar1;
        private System.Windows.Forms.PictureBox Bar2;
        private System.Windows.Forms.PictureBox Bar3;
        private System.Windows.Forms.ListBox IgnoreListListBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button ClearDepletedAccountsButton;
    }
}