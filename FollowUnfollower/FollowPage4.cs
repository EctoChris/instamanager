﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FollowUnfollower.Properties;
using System.Collections.Specialized;
using System.Threading;

namespace FollowUnfollower
{
    public partial class FollowPage4 : Form
    {
        public FollowPage4()
        {
            InitializeComponent();
            refreshTargetAccounts();
        }

        public void refreshTargetAccounts()
        {
            StringCollection savedList = (StringCollection)Settings.Default["CurrentTargetAccounts"];
            var list = savedList.Cast<string>().ToList();
            CurrentTargetAccountsListBox.DataSource = list;
            CurrentTargetAccountsListBox.Refresh();
        }
        private void SettingsGuideScreen_Load(object sender, EventArgs e)
        {
            //comment test
        }

        private async void ContinueButton_Click(object sender, EventArgs e)
        {
            StringCollection savedList = (StringCollection)Settings.Default["CurrentTargetAccounts"];
            var list = savedList.Cast<string>().ToList();
            bool validTargetAccounts = false;
            if (list.Count > 1 || ((list.Count == 1) && (list[0][0] == '@')))
                validTargetAccounts = true;

            if(validTargetAccounts)
            {
                this.Hide();
                if (Globals.followingPage == null)
                    Globals.followingPage = new FollowingPage();
                Globals.followingPage.Show();
            }
            else
            {
                 await Task.Run(async () => await BrieflyShowLabel(InvalidAccountsMessage, Color.Red, 3.5));
           //     await Task.Run(() => BrieflyShowLabel(InvalidAccountsMessage, Color.Red, 4.5));
            }
        }

        private void HomeButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            Globals.homePage.Show();
        }

        private void LinkToFollowPage3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Globals.followPage3.Show();
        }

        private void LinkToFollowPage2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Globals.followPage2.Show();
        }

        private void LinkToFollowPage1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Globals.followPage1.Show();
        }

        private void AddMoreTargetAccountsButton_Click(object sender, EventArgs e)
        {
            using (AddMoreTargetsForm addTargets = new AddMoreTargetsForm())
            {
                var result = addTargets.ShowDialog();
                if(result == DialogResult.OK)
                {
                    refreshTargetAccounts();
                }
            }
        }

        private void ClearTargets_Click(object sender, EventArgs e)
        {
            //Clear Current Target Accounts
            List<string> list = new List<string>();
            list.Add("Click below to add more accounts");
            StringCollection collection = new StringCollection();
            collection.AddRange(list.ToArray());
            Settings.Default["CurrentTargetAccounts"] = collection;
            Settings.Default.Save();
            refreshTargetAccounts();
        }

        private async Task BrieflyShowLabel(Label label, Color colour, double time)
        {
            //Show label, Set Text, Set Color
            Invoke(new Action(() =>
            {
                label.Visible = true;
                label.ForeColor = colour;
                AddMoreTargetAccountsButton.FlatAppearance.BorderColor = Color.Red;
                AddMoreTargetAccountsButton.FlatAppearance.BorderSize = 2;
            }));

            //Wait for a time interval
            int sleepTime = (int)time * 1000;
            Thread.Sleep(sleepTime);

            //Hide label
            Invoke(new Action(() =>
            {
                label.Visible = false;
                AddMoreTargetAccountsButton.FlatAppearance.BorderColor = Color.Black;
                AddMoreTargetAccountsButton.FlatAppearance.BorderSize = 1;
            }));
        }
    }
}
