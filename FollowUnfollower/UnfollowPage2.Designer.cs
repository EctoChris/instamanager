﻿namespace FollowUnfollower
{
    partial class UnfollowPage2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UnfollowPage2));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.logoutButton = new System.Windows.Forms.PictureBox();
            this.HomeButton = new System.Windows.Forms.PictureBox();
            this.UnfollowSettingsLabel = new System.Windows.Forms.Label();
            this.Step2Label = new System.Windows.Forms.Label();
            this.SessionsExplanationLabel = new System.Windows.Forms.Label();
            this.SessionsToRunLabel = new System.Windows.Forms.Label();
            this.ToLabel = new System.Windows.Forms.Label();
            this.ToLabel2 = new System.Windows.Forms.Label();
            this.ContinueButton = new System.Windows.Forms.PictureBox();
            this.ExplainedExampleButton = new System.Windows.Forms.Button();
            this.TimeBetweenSessionsLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TimeBetweenSessionsFrom = new System.Windows.Forms.NumericUpDown();
            this.TimeBetweenSessionsTo = new System.Windows.Forms.NumericUpDown();
            this.UnfollowsPerSessionTo = new System.Windows.Forms.NumericUpDown();
            this.UnfollowsPerSessionFrom = new System.Windows.Forms.NumericUpDown();
            this.TimeBetweenUnfollowsTo = new System.Windows.Forms.NumericUpDown();
            this.TimeBetweenUnfollowsFrom = new System.Windows.Forms.NumericUpDown();
            this.LinkToFollowPage4 = new System.Windows.Forms.PictureBox();
            this.LinkToFollowPage3 = new System.Windows.Forms.PictureBox();
            this.LinkToFollowPage2 = new System.Windows.Forms.PictureBox();
            this.LinkToFollowPage1 = new System.Windows.Forms.PictureBox();
            this.Bar1 = new System.Windows.Forms.PictureBox();
            this.Bar2 = new System.Windows.Forms.PictureBox();
            this.Bar3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoutButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HomeButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContinueButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeBetweenSessionsFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeBetweenSessionsTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnfollowsPerSessionTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnfollowsPerSessionFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeBetweenUnfollowsTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeBetweenUnfollowsFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar3)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::FollowUnfollower.Properties.Resources.instaGradientRect;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(94, 91);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(636, 377);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // logoutButton
            // 
            this.logoutButton.BackgroundImage = global::FollowUnfollower.Properties.Resources.logoutButton;
            this.logoutButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.logoutButton.Location = new System.Drawing.Point(676, 22);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(126, 49);
            this.logoutButton.TabIndex = 2;
            this.logoutButton.TabStop = false;
            this.logoutButton.Click += new System.EventHandler(this.logoutButton_Click);
            // 
            // HomeButton
            // 
            this.HomeButton.BackgroundImage = global::FollowUnfollower.Properties.Resources.FollowGramLogo;
            this.HomeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.HomeButton.Location = new System.Drawing.Point(12, -3);
            this.HomeButton.Name = "HomeButton";
            this.HomeButton.Size = new System.Drawing.Size(103, 97);
            this.HomeButton.TabIndex = 3;
            this.HomeButton.TabStop = false;
            this.HomeButton.Click += new System.EventHandler(this.HomeButton_Click);
            // 
            // UnfollowSettingsLabel
            // 
            this.UnfollowSettingsLabel.AutoSize = true;
            this.UnfollowSettingsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.25F);
            this.UnfollowSettingsLabel.Location = new System.Drawing.Point(311, 70);
            this.UnfollowSettingsLabel.Name = "UnfollowSettingsLabel";
            this.UnfollowSettingsLabel.Size = new System.Drawing.Size(211, 30);
            this.UnfollowSettingsLabel.TabIndex = 4;
            this.UnfollowSettingsLabel.Text = "Unfollow Settings";
            // 
            // Step2Label
            // 
            this.Step2Label.AutoSize = true;
            this.Step2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.Step2Label.Location = new System.Drawing.Point(374, 126);
            this.Step2Label.Name = "Step2Label";
            this.Step2Label.Size = new System.Drawing.Size(81, 26);
            this.Step2Label.TabIndex = 5;
            this.Step2Label.Text = "Step 2:";
            // 
            // SessionsExplanationLabel
            // 
            this.SessionsExplanationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.SessionsExplanationLabel.Location = new System.Drawing.Point(163, 192);
            this.SessionsExplanationLabel.Name = "SessionsExplanationLabel";
            this.SessionsExplanationLabel.Size = new System.Drawing.Size(292, 151);
            this.SessionsExplanationLabel.TabIndex = 6;
            this.SessionsExplanationLabel.Text = resources.GetString("SessionsExplanationLabel.Text");
            // 
            // SessionsToRunLabel
            // 
            this.SessionsToRunLabel.AutoSize = true;
            this.SessionsToRunLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.SessionsToRunLabel.Location = new System.Drawing.Point(162, 159);
            this.SessionsToRunLabel.Name = "SessionsToRunLabel";
            this.SessionsToRunLabel.Size = new System.Drawing.Size(258, 24);
            this.SessionsToRunLabel.TabIndex = 7;
            this.SessionsToRunLabel.Text = "Sessions To Run Followgram";
            // 
            // ToLabel
            // 
            this.ToLabel.AutoSize = true;
            this.ToLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.ToLabel.Location = new System.Drawing.Point(559, 192);
            this.ToLabel.Name = "ToLabel";
            this.ToLabel.Size = new System.Drawing.Size(25, 24);
            this.ToLabel.TabIndex = 9;
            this.ToLabel.Text = "to";
            // 
            // ToLabel2
            // 
            this.ToLabel2.AutoSize = true;
            this.ToLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.ToLabel2.Location = new System.Drawing.Point(559, 260);
            this.ToLabel2.Name = "ToLabel2";
            this.ToLabel2.Size = new System.Drawing.Size(25, 24);
            this.ToLabel2.TabIndex = 14;
            this.ToLabel2.Text = "to";
            // 
            // ContinueButton
            // 
            this.ContinueButton.BackgroundImage = global::FollowUnfollower.Properties.Resources.Group__1_;
            this.ContinueButton.Location = new System.Drawing.Point(256, 384);
            this.ContinueButton.Name = "ContinueButton";
            this.ContinueButton.Size = new System.Drawing.Size(314, 45);
            this.ContinueButton.TabIndex = 15;
            this.ContinueButton.TabStop = false;
            this.ContinueButton.Click += new System.EventHandler(this.ContinueButton_Click);
            // 
            // ExplainedExampleButton
            // 
            this.ExplainedExampleButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.ExplainedExampleButton.Location = new System.Drawing.Point(166, 308);
            this.ExplainedExampleButton.Name = "ExplainedExampleButton";
            this.ExplainedExampleButton.Size = new System.Drawing.Size(281, 42);
            this.ExplainedExampleButton.TabIndex = 16;
            this.ExplainedExampleButton.Text = "Click here for explained example";
            this.ExplainedExampleButton.UseVisualStyleBackColor = true;
            // 
            // TimeBetweenSessionsLabel
            // 
            this.TimeBetweenSessionsLabel.AutoSize = true;
            this.TimeBetweenSessionsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.TimeBetweenSessionsLabel.Location = new System.Drawing.Point(462, 163);
            this.TimeBetweenSessionsLabel.Name = "TimeBetweenSessionsLabel";
            this.TimeBetweenSessionsLabel.Size = new System.Drawing.Size(232, 18);
            this.TimeBetweenSessionsLabel.TabIndex = 17;
            this.TimeBetweenSessionsLabel.Text = "Time between Sessions (minutes)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label1.Location = new System.Drawing.Point(465, 231);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 18);
            this.label1.TabIndex = 18;
            this.label1.Text = "Unfollows per Session";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label2.Location = new System.Drawing.Point(465, 294);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(241, 18);
            this.label2.TabIndex = 19;
            this.label2.Text = "Time between Unfollows (seconds)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label3.Location = new System.Drawing.Point(559, 323);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 24);
            this.label3.TabIndex = 20;
            this.label3.Text = "to";
            // 
            // TimeBetweenSessionsFrom
            // 
            this.TimeBetweenSessionsFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.TimeBetweenSessionsFrom.Location = new System.Drawing.Point(468, 194);
            this.TimeBetweenSessionsFrom.Name = "TimeBetweenSessionsFrom";
            this.TimeBetweenSessionsFrom.Size = new System.Drawing.Size(62, 27);
            this.TimeBetweenSessionsFrom.TabIndex = 21;
            this.TimeBetweenSessionsFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TimeBetweenSessionsFrom.ValueChanged += new System.EventHandler(this.TimeBetweenSessionsFrom_ValueChanged);
            // 
            // TimeBetweenSessionsTo
            // 
            this.TimeBetweenSessionsTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.TimeBetweenSessionsTo.Location = new System.Drawing.Point(610, 194);
            this.TimeBetweenSessionsTo.Name = "TimeBetweenSessionsTo";
            this.TimeBetweenSessionsTo.Size = new System.Drawing.Size(62, 27);
            this.TimeBetweenSessionsTo.TabIndex = 22;
            this.TimeBetweenSessionsTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TimeBetweenSessionsTo.ValueChanged += new System.EventHandler(this.TimeBetweenSessionsTo_ValueChanged);
            // 
            // UnfollowsPerSessionTo
            // 
            this.UnfollowsPerSessionTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.UnfollowsPerSessionTo.Location = new System.Drawing.Point(610, 257);
            this.UnfollowsPerSessionTo.Name = "UnfollowsPerSessionTo";
            this.UnfollowsPerSessionTo.Size = new System.Drawing.Size(62, 27);
            this.UnfollowsPerSessionTo.TabIndex = 24;
            this.UnfollowsPerSessionTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.UnfollowsPerSessionTo.ValueChanged += new System.EventHandler(this.UnfollowsPerSessionTo_ValueChanged);
            // 
            // UnfollowsPerSessionFrom
            // 
            this.UnfollowsPerSessionFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.UnfollowsPerSessionFrom.Location = new System.Drawing.Point(468, 257);
            this.UnfollowsPerSessionFrom.Name = "UnfollowsPerSessionFrom";
            this.UnfollowsPerSessionFrom.Size = new System.Drawing.Size(62, 27);
            this.UnfollowsPerSessionFrom.TabIndex = 23;
            this.UnfollowsPerSessionFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.UnfollowsPerSessionFrom.ValueChanged += new System.EventHandler(this.UnfollowsPerSessionFrom_ValueChanged);
            // 
            // TimeBetweenUnfollowsTo
            // 
            this.TimeBetweenUnfollowsTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.TimeBetweenUnfollowsTo.Location = new System.Drawing.Point(610, 323);
            this.TimeBetweenUnfollowsTo.Name = "TimeBetweenUnfollowsTo";
            this.TimeBetweenUnfollowsTo.Size = new System.Drawing.Size(62, 27);
            this.TimeBetweenUnfollowsTo.TabIndex = 26;
            this.TimeBetweenUnfollowsTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TimeBetweenUnfollowsTo.ValueChanged += new System.EventHandler(this.TimeBetweenUnfollowsTo_ValueChanged);
            // 
            // TimeBetweenUnfollowsFrom
            // 
            this.TimeBetweenUnfollowsFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.TimeBetweenUnfollowsFrom.Location = new System.Drawing.Point(468, 323);
            this.TimeBetweenUnfollowsFrom.Name = "TimeBetweenUnfollowsFrom";
            this.TimeBetweenUnfollowsFrom.Size = new System.Drawing.Size(62, 27);
            this.TimeBetweenUnfollowsFrom.TabIndex = 25;
            this.TimeBetweenUnfollowsFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TimeBetweenUnfollowsFrom.ValueChanged += new System.EventHandler(this.TimeBetweenUnfollowsFrom_ValueChanged);
            // 
            // LinkToFollowPage4
            // 
            this.LinkToFollowPage4.BackgroundImage = global::FollowUnfollower.Properties.Resources.Grey4;
            this.LinkToFollowPage4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LinkToFollowPage4.Location = new System.Drawing.Point(556, 458);
            this.LinkToFollowPage4.Name = "LinkToFollowPage4";
            this.LinkToFollowPage4.Size = new System.Drawing.Size(55, 55);
            this.LinkToFollowPage4.TabIndex = 30;
            this.LinkToFollowPage4.TabStop = false;
            // 
            // LinkToFollowPage3
            // 
            this.LinkToFollowPage3.BackgroundImage = global::FollowUnfollower.Properties.Resources.Grey3;
            this.LinkToFollowPage3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LinkToFollowPage3.Location = new System.Drawing.Point(443, 458);
            this.LinkToFollowPage3.Name = "LinkToFollowPage3";
            this.LinkToFollowPage3.Size = new System.Drawing.Size(55, 55);
            this.LinkToFollowPage3.TabIndex = 29;
            this.LinkToFollowPage3.TabStop = false;
            // 
            // LinkToFollowPage2
            // 
            this.LinkToFollowPage2.BackgroundImage = global::FollowUnfollower.Properties.Resources.Coloured2;
            this.LinkToFollowPage2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LinkToFollowPage2.Location = new System.Drawing.Point(341, 458);
            this.LinkToFollowPage2.Name = "LinkToFollowPage2";
            this.LinkToFollowPage2.Size = new System.Drawing.Size(55, 55);
            this.LinkToFollowPage2.TabIndex = 28;
            this.LinkToFollowPage2.TabStop = false;
            // 
            // LinkToFollowPage1
            // 
            this.LinkToFollowPage1.BackgroundImage = global::FollowUnfollower.Properties.Resources.Export_Page;
            this.LinkToFollowPage1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LinkToFollowPage1.Location = new System.Drawing.Point(221, 458);
            this.LinkToFollowPage1.Name = "LinkToFollowPage1";
            this.LinkToFollowPage1.Size = new System.Drawing.Size(55, 55);
            this.LinkToFollowPage1.TabIndex = 27;
            this.LinkToFollowPage1.TabStop = false;
            // 
            // Bar1
            // 
            this.Bar1.BackgroundImage = global::FollowUnfollower.Properties.Resources.ColouredBar1To2;
            this.Bar1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Bar1.Location = new System.Drawing.Point(259, 482);
            this.Bar1.Name = "Bar1";
            this.Bar1.Size = new System.Drawing.Size(109, 14);
            this.Bar1.TabIndex = 31;
            this.Bar1.TabStop = false;
            // 
            // Bar2
            // 
            this.Bar2.BackgroundImage = global::FollowUnfollower.Properties.Resources.EmptyBar;
            this.Bar2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Bar2.Location = new System.Drawing.Point(374, 482);
            this.Bar2.Name = "Bar2";
            this.Bar2.Size = new System.Drawing.Size(109, 14);
            this.Bar2.TabIndex = 32;
            this.Bar2.TabStop = false;
            // 
            // Bar3
            // 
            this.Bar3.BackgroundImage = global::FollowUnfollower.Properties.Resources.EmptyBar;
            this.Bar3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Bar3.Location = new System.Drawing.Point(477, 482);
            this.Bar3.Name = "Bar3";
            this.Bar3.Size = new System.Drawing.Size(109, 14);
            this.Bar3.TabIndex = 33;
            this.Bar3.TabStop = false;
            // 
            // UnfollowPage2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 560);
            this.Controls.Add(this.LinkToFollowPage4);
            this.Controls.Add(this.LinkToFollowPage3);
            this.Controls.Add(this.LinkToFollowPage2);
            this.Controls.Add(this.LinkToFollowPage1);
            this.Controls.Add(this.Bar1);
            this.Controls.Add(this.Bar2);
            this.Controls.Add(this.Bar3);
            this.Controls.Add(this.TimeBetweenUnfollowsTo);
            this.Controls.Add(this.TimeBetweenUnfollowsFrom);
            this.Controls.Add(this.UnfollowsPerSessionTo);
            this.Controls.Add(this.UnfollowsPerSessionFrom);
            this.Controls.Add(this.TimeBetweenSessionsTo);
            this.Controls.Add(this.TimeBetweenSessionsFrom);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TimeBetweenSessionsLabel);
            this.Controls.Add(this.ExplainedExampleButton);
            this.Controls.Add(this.ContinueButton);
            this.Controls.Add(this.ToLabel2);
            this.Controls.Add(this.ToLabel);
            this.Controls.Add(this.SessionsToRunLabel);
            this.Controls.Add(this.SessionsExplanationLabel);
            this.Controls.Add(this.Step2Label);
            this.Controls.Add(this.UnfollowSettingsLabel);
            this.Controls.Add(this.HomeButton);
            this.Controls.Add(this.logoutButton);
            this.Controls.Add(this.pictureBox1);
            this.Name = "UnfollowPage2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SettingsGuideScreen";
            this.Load += new System.EventHandler(this.SettingsGuideScreen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoutButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HomeButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContinueButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeBetweenSessionsFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeBetweenSessionsTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnfollowsPerSessionTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnfollowsPerSessionFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeBetweenUnfollowsTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeBetweenUnfollowsFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox logoutButton;
        private System.Windows.Forms.PictureBox HomeButton;
        private System.Windows.Forms.Label UnfollowSettingsLabel;
        private System.Windows.Forms.Label Step2Label;
        private System.Windows.Forms.Label SessionsExplanationLabel;
        private System.Windows.Forms.Label SessionsToRunLabel;
        private System.Windows.Forms.Label ToLabel;
        private System.Windows.Forms.Label ToLabel2;
        private System.Windows.Forms.PictureBox ContinueButton;
        private System.Windows.Forms.Button ExplainedExampleButton;
        private System.Windows.Forms.Label TimeBetweenSessionsLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown TimeBetweenSessionsFrom;
        private System.Windows.Forms.NumericUpDown TimeBetweenSessionsTo;
        private System.Windows.Forms.NumericUpDown UnfollowsPerSessionTo;
        private System.Windows.Forms.NumericUpDown UnfollowsPerSessionFrom;
        private System.Windows.Forms.NumericUpDown TimeBetweenUnfollowsTo;
        private System.Windows.Forms.NumericUpDown TimeBetweenUnfollowsFrom;
        private System.Windows.Forms.PictureBox LinkToFollowPage4;
        private System.Windows.Forms.PictureBox LinkToFollowPage3;
        private System.Windows.Forms.PictureBox LinkToFollowPage2;
        private System.Windows.Forms.PictureBox LinkToFollowPage1;
        private System.Windows.Forms.PictureBox Bar1;
        private System.Windows.Forms.PictureBox Bar2;
        private System.Windows.Forms.PictureBox Bar3;
    }
}