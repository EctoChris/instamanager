﻿namespace FollowUnfollower
{
    partial class FollowPage1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.logoutButton = new System.Windows.Forms.PictureBox();
            this.HomeButton = new System.Windows.Forms.PictureBox();
            this.FollowSettingsLabel = new System.Windows.Forms.Label();
            this.Step1Label = new System.Windows.Forms.Label();
            this.TipLabel = new System.Windows.Forms.Label();
            this.TimeOfDayToRunLabel = new System.Windows.Forms.Label();
            this.TimeOfDayFromPicker1 = new System.Windows.Forms.DateTimePicker();
            this.ToLabel = new System.Windows.Forms.Label();
            this.TimeOfDayToPicker1 = new System.Windows.Forms.DateTimePicker();
            this.AddTimesButton = new System.Windows.Forms.Button();
            this.TimeOfDayFromPicker2 = new System.Windows.Forms.DateTimePicker();
            this.TimeOfDayToPicker2 = new System.Windows.Forms.DateTimePicker();
            this.ToLabel2 = new System.Windows.Forms.Label();
            this.ContinueButton = new System.Windows.Forms.PictureBox();
            this.LinkToFollowPage1 = new System.Windows.Forms.PictureBox();
            this.LinkToFollowPage2 = new System.Windows.Forms.PictureBox();
            this.LinkToFollowPage3 = new System.Windows.Forms.PictureBox();
            this.LinkToFollowPage4 = new System.Windows.Forms.PictureBox();
            this.Bar1 = new System.Windows.Forms.PictureBox();
            this.Bar2 = new System.Windows.Forms.PictureBox();
            this.Bar3 = new System.Windows.Forms.PictureBox();
            this.RemoveTimesButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoutButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HomeButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContinueButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar3)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::FollowUnfollower.Properties.Resources.instaGradientRect;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(100, 100);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(636, 377);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // logoutButton
            // 
            this.logoutButton.BackgroundImage = global::FollowUnfollower.Properties.Resources.logoutButton;
            this.logoutButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.logoutButton.Location = new System.Drawing.Point(676, 22);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(126, 49);
            this.logoutButton.TabIndex = 2;
            this.logoutButton.TabStop = false;
            // 
            // HomeButton
            // 
            this.HomeButton.BackgroundImage = global::FollowUnfollower.Properties.Resources.FollowGramLogo;
            this.HomeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.HomeButton.Location = new System.Drawing.Point(12, -3);
            this.HomeButton.Name = "HomeButton";
            this.HomeButton.Size = new System.Drawing.Size(103, 97);
            this.HomeButton.TabIndex = 3;
            this.HomeButton.TabStop = false;
            this.HomeButton.Click += new System.EventHandler(this.HomeButton_Click);
            // 
            // FollowSettingsLabel
            // 
            this.FollowSettingsLabel.AutoSize = true;
            this.FollowSettingsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.25F);
            this.FollowSettingsLabel.Location = new System.Drawing.Point(311, 70);
            this.FollowSettingsLabel.Name = "FollowSettingsLabel";
            this.FollowSettingsLabel.Size = new System.Drawing.Size(187, 30);
            this.FollowSettingsLabel.TabIndex = 4;
            this.FollowSettingsLabel.Text = "Follow Settings";
            // 
            // Step1Label
            // 
            this.Step1Label.AutoSize = true;
            this.Step1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.Step1Label.Location = new System.Drawing.Point(374, 126);
            this.Step1Label.Name = "Step1Label";
            this.Step1Label.Size = new System.Drawing.Size(81, 26);
            this.Step1Label.TabIndex = 5;
            this.Step1Label.Text = "Step 1:";
            // 
            // TipLabel
            // 
            this.TipLabel.AutoSize = true;
            this.TipLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.TipLabel.Location = new System.Drawing.Point(238, 165);
            this.TipLabel.Name = "TipLabel";
            this.TipLabel.Size = new System.Drawing.Size(371, 18);
            this.TipLabel.TabIndex = 6;
            this.TipLabel.Text = "Tip: Try to pick times you would naturally use instagram";
            // 
            // TimeOfDayToRunLabel
            // 
            this.TimeOfDayToRunLabel.AutoSize = true;
            this.TimeOfDayToRunLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.TimeOfDayToRunLabel.Location = new System.Drawing.Point(331, 218);
            this.TimeOfDayToRunLabel.Name = "TimeOfDayToRunLabel";
            this.TimeOfDayToRunLabel.Size = new System.Drawing.Size(187, 24);
            this.TimeOfDayToRunLabel.TabIndex = 7;
            this.TimeOfDayToRunLabel.Text = "Time Of Day To Run:";
            // 
            // TimeOfDayFromPicker1
            // 
            this.TimeOfDayFromPicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.TimeOfDayFromPicker1.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.TimeOfDayFromPicker1.Location = new System.Drawing.Point(256, 252);
            this.TimeOfDayFromPicker1.Name = "TimeOfDayFromPicker1";
            this.TimeOfDayFromPicker1.ShowUpDown = true;
            this.TimeOfDayFromPicker1.Size = new System.Drawing.Size(126, 29);
            this.TimeOfDayFromPicker1.TabIndex = 8;
            this.TimeOfDayFromPicker1.Value = new System.DateTime(2018, 11, 7, 9, 0, 0, 0);
            // 
            // ToLabel
            // 
            this.ToLabel.AutoSize = true;
            this.ToLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.ToLabel.Location = new System.Drawing.Point(399, 257);
            this.ToLabel.Name = "ToLabel";
            this.ToLabel.Size = new System.Drawing.Size(25, 24);
            this.ToLabel.TabIndex = 9;
            this.ToLabel.Text = "to";
            // 
            // TimeOfDayToPicker1
            // 
            this.TimeOfDayToPicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.TimeOfDayToPicker1.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.TimeOfDayToPicker1.Location = new System.Drawing.Point(442, 252);
            this.TimeOfDayToPicker1.Name = "TimeOfDayToPicker1";
            this.TimeOfDayToPicker1.ShowUpDown = true;
            this.TimeOfDayToPicker1.Size = new System.Drawing.Size(128, 29);
            this.TimeOfDayToPicker1.TabIndex = 10;
            this.TimeOfDayToPicker1.Value = new System.DateTime(2018, 11, 7, 11, 0, 0, 0);
            // 
            // AddTimesButton
            // 
            this.AddTimesButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.AddTimesButton.Location = new System.Drawing.Point(442, 296);
            this.AddTimesButton.Name = "AddTimesButton";
            this.AddTimesButton.Size = new System.Drawing.Size(128, 30);
            this.AddTimesButton.TabIndex = 11;
            this.AddTimesButton.Text = "+ Times";
            this.AddTimesButton.UseVisualStyleBackColor = true;
            this.AddTimesButton.Click += new System.EventHandler(this.AddTimesButton_Click);
            // 
            // TimeOfDayFromPicker2
            // 
            this.TimeOfDayFromPicker2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.TimeOfDayFromPicker2.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.TimeOfDayFromPicker2.Location = new System.Drawing.Point(256, 295);
            this.TimeOfDayFromPicker2.Name = "TimeOfDayFromPicker2";
            this.TimeOfDayFromPicker2.ShowUpDown = true;
            this.TimeOfDayFromPicker2.Size = new System.Drawing.Size(126, 29);
            this.TimeOfDayFromPicker2.TabIndex = 12;
            this.TimeOfDayFromPicker2.Value = new System.DateTime(2018, 11, 7, 16, 0, 0, 0);
            this.TimeOfDayFromPicker2.Visible = false;
            // 
            // TimeOfDayToPicker2
            // 
            this.TimeOfDayToPicker2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.TimeOfDayToPicker2.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.TimeOfDayToPicker2.Location = new System.Drawing.Point(442, 296);
            this.TimeOfDayToPicker2.Name = "TimeOfDayToPicker2";
            this.TimeOfDayToPicker2.ShowUpDown = true;
            this.TimeOfDayToPicker2.Size = new System.Drawing.Size(128, 29);
            this.TimeOfDayToPicker2.TabIndex = 13;
            this.TimeOfDayToPicker2.Value = new System.DateTime(2018, 11, 7, 23, 0, 0, 0);
            this.TimeOfDayToPicker2.Visible = false;
            // 
            // ToLabel2
            // 
            this.ToLabel2.AutoSize = true;
            this.ToLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.ToLabel2.Location = new System.Drawing.Point(399, 296);
            this.ToLabel2.Name = "ToLabel2";
            this.ToLabel2.Size = new System.Drawing.Size(25, 24);
            this.ToLabel2.TabIndex = 14;
            this.ToLabel2.Text = "to";
            // 
            // ContinueButton
            // 
            this.ContinueButton.BackgroundImage = global::FollowUnfollower.Properties.Resources.Group__1_;
            this.ContinueButton.Location = new System.Drawing.Point(256, 384);
            this.ContinueButton.Name = "ContinueButton";
            this.ContinueButton.Size = new System.Drawing.Size(314, 45);
            this.ContinueButton.TabIndex = 15;
            this.ContinueButton.TabStop = false;
            this.ContinueButton.Click += new System.EventHandler(this.ContinueButton_Click);
            // 
            // LinkToFollowPage1
            // 
            this.LinkToFollowPage1.BackgroundImage = global::FollowUnfollower.Properties.Resources.Export_Page;
            this.LinkToFollowPage1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LinkToFollowPage1.Location = new System.Drawing.Point(218, 469);
            this.LinkToFollowPage1.Name = "LinkToFollowPage1";
            this.LinkToFollowPage1.Size = new System.Drawing.Size(55, 55);
            this.LinkToFollowPage1.TabIndex = 16;
            this.LinkToFollowPage1.TabStop = false;
            // 
            // LinkToFollowPage2
            // 
            this.LinkToFollowPage2.BackgroundImage = global::FollowUnfollower.Properties.Resources.Grey2;
            this.LinkToFollowPage2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LinkToFollowPage2.Location = new System.Drawing.Point(338, 469);
            this.LinkToFollowPage2.Name = "LinkToFollowPage2";
            this.LinkToFollowPage2.Size = new System.Drawing.Size(55, 55);
            this.LinkToFollowPage2.TabIndex = 17;
            this.LinkToFollowPage2.TabStop = false;
            // 
            // LinkToFollowPage3
            // 
            this.LinkToFollowPage3.BackgroundImage = global::FollowUnfollower.Properties.Resources.Grey3;
            this.LinkToFollowPage3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LinkToFollowPage3.Location = new System.Drawing.Point(440, 469);
            this.LinkToFollowPage3.Name = "LinkToFollowPage3";
            this.LinkToFollowPage3.Size = new System.Drawing.Size(55, 55);
            this.LinkToFollowPage3.TabIndex = 18;
            this.LinkToFollowPage3.TabStop = false;
            // 
            // LinkToFollowPage4
            // 
            this.LinkToFollowPage4.BackgroundImage = global::FollowUnfollower.Properties.Resources.Grey4;
            this.LinkToFollowPage4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LinkToFollowPage4.Location = new System.Drawing.Point(553, 469);
            this.LinkToFollowPage4.Name = "LinkToFollowPage4";
            this.LinkToFollowPage4.Size = new System.Drawing.Size(55, 55);
            this.LinkToFollowPage4.TabIndex = 19;
            this.LinkToFollowPage4.TabStop = false;
            // 
            // Bar1
            // 
            this.Bar1.BackgroundImage = global::FollowUnfollower.Properties.Resources.EmptyBar;
            this.Bar1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Bar1.Location = new System.Drawing.Point(256, 493);
            this.Bar1.Name = "Bar1";
            this.Bar1.Size = new System.Drawing.Size(109, 14);
            this.Bar1.TabIndex = 20;
            this.Bar1.TabStop = false;
            // 
            // Bar2
            // 
            this.Bar2.BackgroundImage = global::FollowUnfollower.Properties.Resources.EmptyBar;
            this.Bar2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Bar2.Location = new System.Drawing.Point(371, 493);
            this.Bar2.Name = "Bar2";
            this.Bar2.Size = new System.Drawing.Size(109, 14);
            this.Bar2.TabIndex = 21;
            this.Bar2.TabStop = false;
            // 
            // Bar3
            // 
            this.Bar3.BackgroundImage = global::FollowUnfollower.Properties.Resources.EmptyBar;
            this.Bar3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Bar3.Location = new System.Drawing.Point(474, 493);
            this.Bar3.Name = "Bar3";
            this.Bar3.Size = new System.Drawing.Size(109, 14);
            this.Bar3.TabIndex = 22;
            this.Bar3.TabStop = false;
            // 
            // RemoveTimesButton
            // 
            this.RemoveTimesButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.RemoveTimesButton.Location = new System.Drawing.Point(442, 331);
            this.RemoveTimesButton.Name = "RemoveTimesButton";
            this.RemoveTimesButton.Size = new System.Drawing.Size(128, 30);
            this.RemoveTimesButton.TabIndex = 23;
            this.RemoveTimesButton.Text = "Remove Times";
            this.RemoveTimesButton.UseVisualStyleBackColor = true;
            this.RemoveTimesButton.Visible = false;
            this.RemoveTimesButton.Click += new System.EventHandler(this.RemoveTimesButton_Click);
            // 
            // FollowPage1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 560);
            this.Controls.Add(this.RemoveTimesButton);
            this.Controls.Add(this.LinkToFollowPage4);
            this.Controls.Add(this.LinkToFollowPage3);
            this.Controls.Add(this.LinkToFollowPage2);
            this.Controls.Add(this.LinkToFollowPage1);
            this.Controls.Add(this.ContinueButton);
            this.Controls.Add(this.ToLabel2);
            this.Controls.Add(this.AddTimesButton);
            this.Controls.Add(this.TimeOfDayToPicker2);
            this.Controls.Add(this.TimeOfDayFromPicker2);
            this.Controls.Add(this.TimeOfDayToPicker1);
            this.Controls.Add(this.ToLabel);
            this.Controls.Add(this.TimeOfDayFromPicker1);
            this.Controls.Add(this.TimeOfDayToRunLabel);
            this.Controls.Add(this.TipLabel);
            this.Controls.Add(this.Step1Label);
            this.Controls.Add(this.FollowSettingsLabel);
            this.Controls.Add(this.HomeButton);
            this.Controls.Add(this.logoutButton);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Bar1);
            this.Controls.Add(this.Bar2);
            this.Controls.Add(this.Bar3);
            this.Name = "FollowPage1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Follow Page 1";
            this.Load += new System.EventHandler(this.SettingsGuideScreen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoutButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HomeButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContinueButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkToFollowPage4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bar3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox logoutButton;
        private System.Windows.Forms.PictureBox HomeButton;
        private System.Windows.Forms.Label FollowSettingsLabel;
        private System.Windows.Forms.Label Step1Label;
        private System.Windows.Forms.Label TipLabel;
        private System.Windows.Forms.Label TimeOfDayToRunLabel;
        private System.Windows.Forms.DateTimePicker TimeOfDayFromPicker1;
        private System.Windows.Forms.Label ToLabel;
        private System.Windows.Forms.DateTimePicker TimeOfDayToPicker1;
        private System.Windows.Forms.Button AddTimesButton;
        private System.Windows.Forms.DateTimePicker TimeOfDayFromPicker2;
        private System.Windows.Forms.DateTimePicker TimeOfDayToPicker2;
        private System.Windows.Forms.Label ToLabel2;
        private System.Windows.Forms.PictureBox ContinueButton;
        private System.Windows.Forms.PictureBox LinkToFollowPage1;
        private System.Windows.Forms.PictureBox LinkToFollowPage2;
        private System.Windows.Forms.PictureBox LinkToFollowPage3;
        private System.Windows.Forms.PictureBox LinkToFollowPage4;
        private System.Windows.Forms.PictureBox Bar1;
        private System.Windows.Forms.PictureBox Bar2;
        private System.Windows.Forms.PictureBox Bar3;
        private System.Windows.Forms.Button RemoveTimesButton;
    }
}