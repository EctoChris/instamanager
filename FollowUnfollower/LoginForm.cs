﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using OpenQA.Selenium.Chrome;
using System.IO;

//--------------Packages Used--------------------

//Install-Package phantomjs.exe
//Install-Package Selenium.WebDriver -Version 3.6.0
//Install-Package Selenium.Support -Version 3.6.0

//----------------------------------------------


namespace FollowUnfollower
{
    public partial class LoginForm : Form
    {
        public string LoginPassword;
        public string LoginUsername;
        public bool loginSwitch = false;

        public LoginForm()
        {
            ChromeDriverSetUp();                                                                  //-uncomment this to work
            InitializeComponent();
            WhiteFollowGramLogo.Parent = backgroundGradient;
            AutomationLabel.Parent = backgroundGradient;
            LoginSecurelyLabel.Parent = backgroundGradient;
            usernameInput.Text = "Username";
            usernameInput.Cursor = Cursors.IBeam;
            passwordInput.Text = "Password";

            string RunningPath = AppDomain.CurrentDomain.BaseDirectory;
            Const.resourceFolderPath = Path.GetFullPath(Path.Combine(RunningPath, @"..\..\"));
            Const.resourceFolderPath += @"Resources\";
        }

        public async Task login()
        {
            //Username Input (Find label that contains text() 'username' -> get 'for' attribute value and sendKeys to corresponding input element)
            string usernameId = Const.LoginInput;
            Utils.FindAndSendKeys(usernameId, LoginUsername);

            //Password Input
            string passwordId = Const.LoginInputPass;
            Utils.FindAndSendKeys(passwordId, LoginPassword);

            //Login Button Submit
            Utils.FindAndClick(Const.LoginButton);

            Thread.Sleep(2000);

            Utils.GoToUrl("https://www.instagram.com/" + LoginUsername + @"/");
            if (Globals.pDriver.Url == "https://www.instagram.com/#reactivated") 
                Utils.FindAndClick(Const.reactivatedLogin);                     
        }

        public void TakeScreenShot(string nameOfScreenshot)
        {
            //a.pDriver.TakeScreenshot().SaveAsFile(@"C:\tempImages\" + nameOfScreenshot + ".png", OpenQA.Selenium.ScreenshotImageFormat.Png);
            //((JavascriptExecutor) driver).executeScript("window.scrollBy(0,250)", "");
        }

        public void ChromeDriverSetUp()
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddUserProfilePreference("download.default_directory", @"C:\Users\Chris\Downloads\Pinterest\RickandMorty");
            //chromeOptions.AddUserProfilePreference("intl.accept_languages", "nl");
            //chromeOptions.AddArguments("headless");
            chromeOptions.AddUserProfilePreference("disable-popup-blocking", "true");
            Globals.pDriver = new ChromeDriver("Resources", chromeOptions);
            Globals.pDriver.Manage().Window.Maximize();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            Utils.GoToUrl("https://www.instagram.com/accounts/login/");                                           //-uncomment this to work
            Thread.Sleep(2500);                                                                                    //-Uncomment this to work
            //button1.PerformClick(); // steven added this beautiful line
        }

        private async void loginButton_Click(object sender, EventArgs e)
        {
            LoginTimer.Start();
            await Task.Run(async () => await UpdateLoginLabel(loginStatusLabel, "Logging in...", Color.Green));
            //  LoginUsername = usernameInput.Text;
            //  LoginPassword = passwordInput.Text;
            // LoginUsername = "Decarabia13";
            // LoginPassword = "Soccerpro2";
            LoginUsername = "trichohybrids";
            LoginPassword = "ectomorph2";
            //LoginUsername = usernameInput.Text;
            //LoginPassword = passwordInput.Text;

            Thread.Sleep(2000); //this motherfucker, was so instagram let me logg in

            await Task.Run(async () => await login());                                                    //uncomment this to work

            this.Hide();
            Globals.homePage = new HomePage(LoginUsername);
            Globals.homePage.Show();
            LoginTimer.Stop();
        }

        private void usernameInput_Click(object sender, EventArgs e)
        {
            usernameInput.ForeColor = Color.Black;
            usernameInput.Text = "";
        }

        private void passwordInput_Click(object sender, EventArgs e)
        {
            passwordInput.ForeColor = Color.Black;
            passwordInput.Text = "";
            passwordInput.PasswordChar = '*';
        }

        private async void LoginTimer_Tick(object sender, EventArgs e)
        {
            loginSwitch = !loginSwitch;
            if (loginSwitch)
                await Task.Run(async () => await UpdateLoginLabel(loginStatusLabel, "Logging in...", Color.Green));
            else
                loginStatusLabel.Text = "Logging in..";
        }


        public async Task UpdateLoginLabel(Label label, string message, Color color)
        {
            //Show label, Set Text, Set Color
            Invoke(new Action(() =>
            {
                label.Visible = true;
                label.Text = message;
                label.ForeColor = color;
            }));
        }
    }
}
