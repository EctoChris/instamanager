﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using OpenQA.Selenium.Chrome;

namespace FollowUnfollower
{
   public class Const
    {
        //Newer Variables:
        public const string LoginInput = "//input[@name='username']";
        public const string LoginInputPass = "//input[@name='password']";
        public const string LoginButton = "//div[contains(text(), 'Log')]";
        public const string searchBar = "//input[@placeholder='Search']";
       // public const string FollowersOfAccount = "//*text()[contains(., followers')]]";
        public const string FollowersOfAccount = "//*[text()[contains(.,'followers')]]";
        public const string visibleFollowButtons = "//button[text()='Follow']";
        public const string FollowDialog = "//div[@role='dialog']";
        public const string FollowList = ".//ul[1]/..";
        public const string followDialogList = "//descendant::ul[1]/div";
                    //public const string UnfollowFormScrolling = ".//descendant::ul[1]";

        //End of newer variables

        public const string SearchBarXPATH = "/section/nav[1]/div[2]/div/div/div[2]/input";
        //public const string LoginXPATH     = "//*[@id='react-root']/section/main/div/article/div/div[1]/div/form/div[1]/div/input";
        //public const string PasswordXPATH  = "//*[@id='react-root']/section/main/div/article/div/div[1]/div/form/div[2]/div/input";
       // public const string test01 = "//*[@name='username']";
        public const string LoginXPATH = "//label[contains(text(), 'username')]";
        public const string PasswordXPATH =  "//label[contains(text(), 'Password')]";
        public const string LoginButtonXPATH = "//*[@id='react-root']/section/main/div/article/div/div[1]/div/form.//button[1]";
        //*[@id="react-root"]/section/main/div/article/div/div[1]/div/form/div[3]/button
        //  public const string followingCountParentXPATH = "//a[@href = "

        public const string HomePopUpCloseButton = "/html/body/div[1]/div/div/div[2]/div/div/button";
       // public const string followersButton3 = "//*[@id='react-root']/section/main/article/header/div[2]/ul/li[2]/a";
        public const string followersButton3 =  "//*[@id='react-root']/section/main/article/header/section/ul/li[2]/a";
        public const string UnfollowForm = "/html/body/div[4]/div/div/div[2]/div/div[2]/ul";
        public const string visibleUnfollowButtons = "//button[text()='Following']";
        public const string UnfollowProfileNameLi = "./ancestor::li[1]";
        public const string UnfollowProfileName = ".//descendant::a[2]";

        public const string PeopleFollowing = "//*[text()[contains(.,'following')]]";
        public const string UnfollowSpan2 = "/div/div[2]/span/button/text()"; //original (failed)
        //public const string UnfollowSpan3 = "/div/div[2]/span/button
        //By.xpath("//button[contains(.,'Add Strategy')]")
        ///html/body/div[4]/div/div/div[2]/div/div[2]/ul/div/li[1]/div/div[1]/div/div[1]/a
        public const string UnfollowSpan = "./div/div[2]/span/button"; //add a '.' when searching in the context of an element
                                                                       // public const string UnfollowProfileName = "./div/div[1]/div/div[1]/a"; 
                                                                       // public const string UnfollowFormScrolling = "/html/body/div[4]/div/div/div[2]/div/div[2]";  //this is old one
        public const string UnfollowFormScrollingHeader = "//div[text()='Followers']";
        public const string DynamicLoginButtonXPath = "//div[text()='Log in']";
        public const string UnfollowFormScrollingParent = "./..";
        public const string UnfollowFormScrolling = ".//descendant::ul[1]";
        public const string reactivatedLogin = "//*[@id='react-root']/div/div[2]/a[2]";
        public const string profilePic = "//img[@alt='Change Profile Photo']";
        public static string resourceFolderPath;
    }
    public static class Globals
    {
        // = new ChromeDriver(@"C:\Users\Chris\Documents\chromerDriver");
        // (@"d:\Users\6258\Documents\chromerDriver");
        public static IWebDriver pDriver;
        public enum Status
        {
            Active,
            InBetweenSessions,
            Paused,
            MaxActions,
            Idle
        }
        public static Status ProgramStatus = Status.Idle;
        public static LoginForm loginForm;
        public static HomePage homePage;
        public static FollowPage1 followPage1;
        public static FollowPage2 followPage2;
        public static FollowPage3 followPage3;
        public static FollowPage4 followPage4;
        public static FollowingPage followingPage;
        public static UnfollowPage1 unfollowPage1;
        public static UnfollowPage2 unfollowPage2;
        public static UnfollowPage3 unfollowPage3;
        public static UnfollowPage4 unfollowPage4;
        public static UnfollowingPage unfollowingPage;

        public static bool secondTimeSlotExists = false;
    }
}

