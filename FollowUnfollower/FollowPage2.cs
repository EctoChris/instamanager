﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FollowUnfollower.Properties;

namespace FollowUnfollower
{
    public partial class FollowPage2 : Form
    {
        public FollowPage2()
        {
            InitializeComponent();
            TimeBetweenSessionsFrom.Value = (int)Settings.Default["TimeBetweenSessionsFrom"];
            TimeBetweenSessionsTo.Value = (int)Settings.Default["TimeBetweenSessionsTo"];
            FollowsPerSessionFrom.Value = (int)Settings.Default["FollowsPerSessionFrom"];
            FollowsPerSessionTo.Value = (int)Settings.Default["FollowsPerSessionTo"];
            TimeBetweenFollowsFrom.Value = (int)Settings.Default["TimeBetweenFollowsFrom"];
            TimeBetweenFollowsTo.Value = (int)Settings.Default["TimeBetweenFollowsTo"];
        }

        private void SettingsGuideScreen_Load(object sender, EventArgs e)
        {
            //comment test
        }

        private void ContinueButton_Click(object sender, EventArgs e)
        {
            Settings.Default["TimeBetweenSessionsFrom"] = (int)TimeBetweenSessionsFrom.Value;
            Settings.Default["TimeBetweenSessionsTo"]   = (int)TimeBetweenSessionsTo.Value;
            Settings.Default["FollowsPerSessionFrom"]   = (int)FollowsPerSessionFrom.Value;
            Settings.Default["FollowsPerSessionTo"]     = (int)FollowsPerSessionTo.Value;
            Settings.Default["TimeBetweenFollowsFrom"]  = (int)TimeBetweenFollowsFrom.Value;
            Settings.Default["TimeBetweenFollowsTo"]    = (int) TimeBetweenFollowsTo.Value;
            Settings.Default.Save();

            this.Hide();
            if (Globals.followPage3 == null)
                Globals.followPage3 = new FollowPage3();

            Globals.followPage3.Show();

        }

        private void HomeButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            Globals.homePage.Show();
        }

        private void TimeBetweenSessionsFrom_ValueChanged(object sender, EventArgs e)
        {
            if (TimeBetweenSessionsTo.Value <= TimeBetweenSessionsFrom.Value)
                TimeBetweenSessionsTo.Value = TimeBetweenSessionsFrom.Value;
        }

        private void TimeBetweenSessionsTo_ValueChanged(object sender, EventArgs e)
        {
            if (TimeBetweenSessionsFrom.Value > TimeBetweenSessionsTo.Value)
                TimeBetweenSessionsFrom.Value = TimeBetweenSessionsTo.Value;
        }

        private void FollowPerSessionFrom_ValueChanged(object sender, EventArgs e)
        {
            if (FollowsPerSessionTo.Value < FollowsPerSessionFrom.Value)
                FollowsPerSessionTo.Value = FollowsPerSessionFrom.Value;
        }

        private void FollowsPerSessionTo_ValueChanged(object sender, EventArgs e)
        {
            if (FollowsPerSessionFrom.Value > FollowsPerSessionTo.Value)
                FollowsPerSessionFrom.Value = FollowsPerSessionTo.Value;
        }

        private void TimeBetweenFollowsFrom_ValueChanged(object sender, EventArgs e)
        {
            if (TimeBetweenFollowsTo.Value < TimeBetweenFollowsFrom.Value)
                TimeBetweenFollowsTo.Value = TimeBetweenFollowsFrom.Value;
        }

        private void TimeBetweenFollowsTo_ValueChanged(object sender, EventArgs e)
        {
            if (TimeBetweenFollowsFrom.Value > TimeBetweenFollowsTo.Value)
                TimeBetweenFollowsFrom.Value = TimeBetweenFollowsTo.Value;
        }
    }
}
