﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using FollowUnfollower.Properties;
using OpenQA.Selenium;

namespace FollowUnfollower
{
    public partial class FollowingPage : Form
    {
        public CancellationTokenSource cts = new CancellationTokenSource();

        public bool isFollowing = false;
        public bool running = false;
        public int followCounter = 0;
        public int Seconds = 00;
        public int Minutes = 00;
        public int Hours = 00;
        public int Milliseconds = 00;
        public int randomSessionAmount;
        public FollowingPage()
        {
            InitializeComponent();
        }

        private void listBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            ListBox List = (ListBox)sender;
            if (e.Index > -1)
            {
                object item = List.Items[e.Index];
                e.DrawBackground();
                e.DrawFocusRectangle();
                Brush brush = new SolidBrush(e.ForeColor);
                SizeF size = e.Graphics.MeasureString(item.ToString(), e.Font);
                e.Graphics.DrawString(item.ToString(), e.Font, brush, e.Bounds.Left + (e.Bounds.Width / 2 - size.Width / 2), e.Bounds.Top + (e.Bounds.Height / 2 - size.Height / 2));
            }
        }
        private void SettingsGuideScreen_Load(object sender, EventArgs e)
        {
            //comment test
            followTimer.Start();
        }
        

        private void EditSettingsButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            Globals.followPage1.Show();

        }

        private async void PlayPauseButton_Click(object sender, EventArgs e)
        {
            isFollowing = !isFollowing;
            PlayPauseButton.BackgroundImage = null;

            string newImage;

            if (isFollowing)
            {
                cts = new CancellationTokenSource();
                newImage = Const.resourceFolderPath + "InProgressButton.png";
                Globals.ProgramStatus = Globals.Status.Active;
                followTimer.Start();
            }
            else
            {
                newImage = Const.resourceFolderPath + "PausedButton.png";
                cts.Cancel();
                followTimer.Stop();
                StatusLabel.ForeColor = Color.Red;
                StatusLabel.Text = "Program Paused.";
                Globals.ProgramStatus = Globals.Status.Paused;
            }

            Image image = Image.FromFile(newImage);
            PlayPauseButton.BackgroundImage = image;
        }


        public async Task FollowAction()
        {
            Random r = new Random();
            randomSessionAmount =  r.Next(Settings.Default.FollowsPerSessionFrom, Settings.Default.FollowsPerSessionTo);
            //Gather Relevant Lists from Settings Variables:
            StringCollection savedTargetList = (StringCollection)Settings.Default["CurrentTargetAccounts"];
            var targetList = savedTargetList.Cast<string>().ToList();

            //Verify Target List Exists, if none exist stop following function:
            if (targetList == null || targetList.Count == 0) {
                Invoke(new Action(() => 
                {
                    StatusLabel.Visible = true;
                    StatusLabel.Text = "No Target Accounts Left";
                }));
                return;
            }

            bool currentTargetDepleted = false;
            int tempFollowCounter = 0;
            for (int i = 0; i < targetList.Count; i++)
            {
                currentTargetDepleted = false;
                //Remove '@'
                string urlAcc = targetList[i].Remove(0, 1);

                //Go To Current User:
                Utils.GoToUrl("https://www.instagram.com/" + urlAcc + @"/");
                Thread.Sleep(1500);

                //Click on 'Followers':
                Utils.FindAndClick(Const.FollowersOfAccount);
                Thread.Sleep(1500);
                int scrollCount = 0;
                while (currentTargetDepleted == false)
                {
                    //Loop through followers:
                    IList<IWebElement> followButtonsVisible = Utils.GetListOfButtons(Const.visibleFollowButtons);
                    Console.WriteLine(followButtonsVisible.Count + " is the amount of buttons");
                    //If buttons exist, start following
                    if(followButtonsVisible != null && followButtonsVisible.Count != 0)
                    {
                        foreach(IWebElement e in followButtonsVisible)
                        {
                            try
                            {
                                if (e.Displayed)
                                {
                                    Console.WriteLine("Followed somebody");
                                    scrollCount = 0;
                                    e.SendKeys(OpenQA.Selenium.Keys.Enter);
                                    //Add 1 to counters:
                                    tempFollowCounter++;
                                    followCounter++;
                                    Settings.Default.TotalFollows = Settings.Default.TotalFollows += 1;
                                    Settings.Default.Save();
                                    //Update Label:
                                    Invoke(new Action(() =>
                                    {
                                        FollowsTodayLabel.Text = followCounter.ToString();
                                    }));
                                  

                                    if (followCounter >= Settings.Default.MaxDailyAmount)
                                    {
                                        Globals.ProgramStatus = Globals.Status.MaxActions;
                                        return;
                                    }
                                    if (cts.IsCancellationRequested == true)
                                    {
                                        Globals.ProgramStatus = Globals.Status.Paused;
                                        return;
                                    }
                                }
                            } catch(Exception message)
                            {
                                Console.WriteLine("Found the problem");
                                Console.WriteLine(message);
                            }
                          
                            //Time Between Follows:
                            Console.WriteLine("Waiting between follows");
                            Utils.RandomSleepInterval(Settings.Default.TimeBetweenFollowsFrom, Settings.Default.TimeBetweenFollowsTo);
                            if (tempFollowCounter >= randomSessionAmount)
                            {
                                Console.WriteLine("Waiting between sessions");
                                //Wait for time between sessions:
                                Globals.ProgramStatus = Globals.Status.InBetweenSessions;
                                Utils.RandomSleepInterval(Settings.Default.TimeBetweenSessionsFrom * 60, Settings.Default.TimeBetweenSessionsTo * 60);
                                //Reset timer:
                                randomSessionAmount = 0;
                            }
                            Globals.ProgramStatus = Globals.Status.Active;
                        }
                    } else {
                        //Scroll:
                        scrollCount++;
                        Console.WriteLine("Scrolled down");
                        unfollowScrollDown();
                    }

                    if (scrollCount == 100)
                    {
                        //Update Label:
                        Invoke(new Action(() =>
                        {
                            StatusLabel.Visible = true;
                            StatusLabel.Text = "Followed All Possible Users";
                        }));

                        //Remove Target Account from Targets List:
                        savedTargetList.Remove(targetList[i]);
                        Settings.Default.CurrentTargetAccounts = savedTargetList;

                        //Add Used Target Account to Depleted List:
                        StringCollection savedDepletedList = (StringCollection)Settings.Default["DepletedTargetAccounts"];
                        if(savedDepletedList == null)
                        {
                            savedDepletedList = new StringCollection();
                        }
                        savedDepletedList.Add(targetList[i]);
                        Settings.Default.DepletedTargetAccounts = savedDepletedList;
                        Settings.Default.Save();

                        currentTargetDepleted = true;
                        MessageBox.Show("Depleted target account");
                    }
                }
            }
            return;
        }

        public void unfollowScrollDown()
        {
            try
            {
                IWebElement dialog = Utils.FindElement(Const.FollowDialog);
                IWebElement followList = Utils.FindElement(dialog, Const.FollowList);
                ((IJavaScriptExecutor)Globals.pDriver).ExecuteScript("arguments[0].scrollBy(0, 400)", followList);
            } catch(Exception e)
            {
                IWebElement dialog = Utils.FindElement(Const.FollowDialog);
                IWebElement followList = Utils.FindElement(dialog, Const.FollowList);
                ((IJavaScriptExecutor)Globals.pDriver).ExecuteScript("arguments[0].scrollBy(0, 400)", followList);
                MessageBox.Show("Failed Scrolling");
            }

        }

        private void HomeButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            Globals.homePage.Show();
        }

        private async void followTimer_Tick(object sender, EventArgs e)
        {
            string running1 = "";
            string running2 = "";
            if (Globals.ProgramStatus == Globals.Status.Active)
            {
                StatusLabel.ForeColor = Color.Green; ;
                running1 = "Running.";
                running2 = "Running..";
            } else if (Globals.ProgramStatus == Globals.Status.InBetweenSessions)
            {
                StatusLabel.ForeColor = Color.Orange;
                running1 = "In Between Follow Sessions.";
                running2 = "In Between Follow Sessions..";
            } else if (Globals.ProgramStatus == Globals.Status.Idle)
            {
                StatusLabel.ForeColor = Color.Orange;
                running1 = "Program Idle (not set to run at this time of day).";
                running2 = "Program Idle (not set to run at this time of day)..";
            }
            running = !running;
            if (running == true) { StatusLabel.Text = running1; }
            else { StatusLabel.Text = running2; }

            Secs.Text = Seconds.ToString();
            Mins.Text = Minutes.ToString();
            Hrs.Text = Hours.ToString();
            Seconds++;
            if (Seconds > 59)
            {
                Minutes++;
                Seconds = 0;
            }
            if (Minutes > 59)
            {
                Hours++;
                Minutes = 0;
            }

            bool ready = Utils.CheckConditions(Settings.Default.TimeFrom1, Settings.Default.TimeTo1, Settings.Default.TimeFrom2, Settings.Default.TimeTo2, followCounter, Settings.Default.MaxDailyAmount, isFollowing);
            if (ready)
            {
                Console.WriteLine("Time From 1: " + Settings.Default.TimeFrom1.ToString());
                Console.WriteLine("Time to 1: " + Settings.Default.TimeTo1.ToString());

                Globals.ProgramStatus = Globals.Status.Active;
                isFollowing = true;
                await Task.Run(async () => await FollowAction());
            }
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            cts.Cancel();
            followTimer.Stop();
            this.Hide();
            Utils.ExitProgram();
        }

        private void updateLabel(Label label, string text, Color? color)
        {

        }
    }
}
