﻿namespace FollowUnfollower
{
    partial class HomePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.startFollowingButton = new System.Windows.Forms.PictureBox();
            this.startUnfollowingButton = new System.Windows.Forms.PictureBox();
            this.followgramLogoBanner = new System.Windows.Forms.PictureBox();
            this.profilePicSource = new System.Windows.Forms.PictureBox();
            this.logoutButton = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.posts = new System.Windows.Forms.Label();
            this.following = new System.Windows.Forms.Label();
            this.followers = new System.Windows.Forms.Label();
            this.postsLabel = new System.Windows.Forms.Label();
            this.followersLabel = new System.Windows.Forms.Label();
            this.followingLabel = new System.Windows.Forms.Label();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.totalFollowCountLabel = new System.Windows.Forms.Label();
            this.totalUnfollowCountLabel = new System.Windows.Forms.Label();
            this.unfollowcount = new System.Windows.Forms.Label();
            this.followcount = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startFollowingButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startUnfollowingButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.followgramLogoBanner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.profilePicSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoutButton)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::FollowUnfollower.Properties.Resources.instagramGradient;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(25, 277);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(777, 13);
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // startFollowingButton
            // 
            this.startFollowingButton.BackgroundImage = global::FollowUnfollower.Properties.Resources.startFollowingButton;
            this.startFollowingButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.startFollowingButton.Location = new System.Drawing.Point(89, 377);
            this.startFollowingButton.Name = "startFollowingButton";
            this.startFollowingButton.Size = new System.Drawing.Size(222, 60);
            this.startFollowingButton.TabIndex = 4;
            this.startFollowingButton.TabStop = false;
            this.startFollowingButton.Click += new System.EventHandler(this.startFollowingButton_Click);
            // 
            // startUnfollowingButton
            // 
            this.startUnfollowingButton.BackgroundImage = global::FollowUnfollower.Properties.Resources.startUnfollowingButton;
            this.startUnfollowingButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.startUnfollowingButton.Location = new System.Drawing.Point(517, 377);
            this.startUnfollowingButton.Name = "startUnfollowingButton";
            this.startUnfollowingButton.Size = new System.Drawing.Size(232, 60);
            this.startUnfollowingButton.TabIndex = 3;
            this.startUnfollowingButton.TabStop = false;
            this.startUnfollowingButton.Click += new System.EventHandler(this.startUnfollowingButton_Click);
            // 
            // followgramLogoBanner
            // 
            this.followgramLogoBanner.BackgroundImage = global::FollowUnfollower.Properties.Resources.followGramLogoBanner;
            this.followgramLogoBanner.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.followgramLogoBanner.Location = new System.Drawing.Point(25, 31);
            this.followgramLogoBanner.Name = "followgramLogoBanner";
            this.followgramLogoBanner.Size = new System.Drawing.Size(192, 36);
            this.followgramLogoBanner.TabIndex = 2;
            this.followgramLogoBanner.TabStop = false;
            // 
            // profilePicSource
            // 
            this.profilePicSource.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.profilePicSource.Location = new System.Drawing.Point(359, 12);
            this.profilePicSource.Name = "profilePicSource";
            this.profilePicSource.Size = new System.Drawing.Size(114, 108);
            this.profilePicSource.TabIndex = 1;
            this.profilePicSource.TabStop = false;
            // 
            // logoutButton
            // 
            this.logoutButton.BackgroundImage = global::FollowUnfollower.Properties.Resources.logoutButton;
            this.logoutButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.logoutButton.Location = new System.Drawing.Point(676, 22);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(126, 49);
            this.logoutButton.TabIndex = 0;
            this.logoutButton.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(105, 305);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(625, 36);
            this.label1.TabIndex = 6;
            this.label1.Text = "FollowGram works by following people automatically and then unfollowing them at a" +
    " later date.\r\nEach day set FollowGram to either follow or unfollow users.";
            // 
            // posts
            // 
            this.posts.AutoSize = true;
            this.posts.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.posts.Location = new System.Drawing.Point(122, 237);
            this.posts.Name = "posts";
            this.posts.Size = new System.Drawing.Size(53, 22);
            this.posts.TabIndex = 7;
            this.posts.Text = "posts";
            // 
            // following
            // 
            this.following.AutoSize = true;
            this.following.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.following.Location = new System.Drawing.Point(620, 237);
            this.following.Name = "following";
            this.following.Size = new System.Drawing.Size(80, 22);
            this.following.TabIndex = 8;
            this.following.Text = "following";
            // 
            // followers
            // 
            this.followers.AutoSize = true;
            this.followers.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.followers.Location = new System.Drawing.Point(374, 237);
            this.followers.Name = "followers";
            this.followers.Size = new System.Drawing.Size(81, 22);
            this.followers.TabIndex = 9;
            this.followers.Text = "followers";
            // 
            // postsLabel
            // 
            this.postsLabel.AutoSize = true;
            this.postsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.postsLabel.Location = new System.Drawing.Point(114, 167);
            this.postsLabel.Name = "postsLabel";
            this.postsLabel.Size = new System.Drawing.Size(68, 73);
            this.postsLabel.TabIndex = 10;
            this.postsLabel.Text = "0";
            this.postsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // followersLabel
            // 
            this.followersLabel.AutoSize = true;
            this.followersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.followersLabel.Location = new System.Drawing.Point(365, 164);
            this.followersLabel.Name = "followersLabel";
            this.followersLabel.Size = new System.Drawing.Size(68, 73);
            this.followersLabel.TabIndex = 11;
            this.followersLabel.Text = "0";
            this.followersLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // followingLabel
            // 
            this.followingLabel.AutoSize = true;
            this.followingLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.followingLabel.Location = new System.Drawing.Point(616, 167);
            this.followingLabel.Name = "followingLabel";
            this.followingLabel.Size = new System.Drawing.Size(68, 73);
            this.followingLabel.TabIndex = 12;
            this.followingLabel.Text = "0";
            this.followingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernameLabel.Location = new System.Drawing.Point(356, 132);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(53, 13);
            this.usernameLabel.TabIndex = 13;
            this.usernameLabel.Text = "username";
            // 
            // totalFollowCountLabel
            // 
            this.totalFollowCountLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.totalFollowCountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalFollowCountLabel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.totalFollowCountLabel.Location = new System.Drawing.Point(139, 463);
            this.totalFollowCountLabel.Name = "totalFollowCountLabel";
            this.totalFollowCountLabel.Size = new System.Drawing.Size(114, 40);
            this.totalFollowCountLabel.TabIndex = 14;
            this.totalFollowCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // totalUnfollowCountLabel
            // 
            this.totalUnfollowCountLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.totalUnfollowCountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalUnfollowCountLabel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.totalUnfollowCountLabel.Location = new System.Drawing.Point(576, 463);
            this.totalUnfollowCountLabel.Name = "totalUnfollowCountLabel";
            this.totalUnfollowCountLabel.Size = new System.Drawing.Size(114, 40);
            this.totalUnfollowCountLabel.TabIndex = 15;
            this.totalUnfollowCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // unfollowcount
            // 
            this.unfollowcount.AutoSize = true;
            this.unfollowcount.Location = new System.Drawing.Point(576, 510);
            this.unfollowcount.Name = "unfollowcount";
            this.unfollowcount.Size = new System.Drawing.Size(115, 13);
            this.unfollowcount.TabIndex = 16;
            this.unfollowcount.Text = "Overall Unfollow Count";
            // 
            // followcount
            // 
            this.followcount.AutoSize = true;
            this.followcount.Location = new System.Drawing.Point(143, 510);
            this.followcount.Name = "followcount";
            this.followcount.Size = new System.Drawing.Size(104, 13);
            this.followcount.TabIndex = 17;
            this.followcount.Text = "Overall Follow Count";
            // 
            // HomePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(827, 560);
            this.Controls.Add(this.followcount);
            this.Controls.Add(this.unfollowcount);
            this.Controls.Add(this.totalUnfollowCountLabel);
            this.Controls.Add(this.totalFollowCountLabel);
            this.Controls.Add(this.usernameLabel);
            this.Controls.Add(this.followingLabel);
            this.Controls.Add(this.followersLabel);
            this.Controls.Add(this.postsLabel);
            this.Controls.Add(this.followers);
            this.Controls.Add(this.following);
            this.Controls.Add(this.posts);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.startFollowingButton);
            this.Controls.Add(this.startUnfollowingButton);
            this.Controls.Add(this.followgramLogoBanner);
            this.Controls.Add(this.profilePicSource);
            this.Controls.Add(this.logoutButton);
            this.Name = "HomePage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HomePage";
            this.Load += new System.EventHandler(this.HomePage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startFollowingButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startUnfollowingButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.followgramLogoBanner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.profilePicSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoutButton)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox logoutButton;
        private System.Windows.Forms.PictureBox profilePicSource;
        private System.Windows.Forms.PictureBox followgramLogoBanner;
        private System.Windows.Forms.PictureBox startUnfollowingButton;
        private System.Windows.Forms.PictureBox startFollowingButton;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label posts;
        private System.Windows.Forms.Label following;
        private System.Windows.Forms.Label followers;
        private System.Windows.Forms.Label postsLabel;
        private System.Windows.Forms.Label followersLabel;
        private System.Windows.Forms.Label followingLabel;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.Label totalFollowCountLabel;
        private System.Windows.Forms.Label totalUnfollowCountLabel;
        private System.Windows.Forms.Label unfollowcount;
        private System.Windows.Forms.Label followcount;
    }
}