﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FollowUnfollower.Properties;

namespace FollowUnfollower
{
    public partial class FollowPage3 : Form
    {
        public FollowPage3()
        {
            InitializeComponent();
            MaxDailyAmount.Text = Settings.Default["MaxDailyAmount"].ToString();
            MaxDailyFollowsTrackBar.Value = (int)Settings.Default["MaxDailyAmount"];
        }

        private void SettingsGuideScreen_Load(object sender, EventArgs e)
        {
            //comment test
        }

        private void ContinueButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            Settings.Default["MaxDailyAmount"] = (int)MaxDailyFollowsTrackBar.Value;
            Settings.Default.Save();

            if (Globals.followPage4 == null)
                Globals.followPage4 = new FollowPage4();

            Globals.followPage4.Show();

        }

        private void HomeButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            Globals.homePage.Show();
        }

        private void MaxDailyFollowsTrackBar_Scroll(object sender, EventArgs e)
        {
            MaxDailyAmount.Text = MaxDailyFollowsTrackBar.Value.ToString();
        }

        private void LinkToFollowPage2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Globals.followPage2.Show();
        }

        private void LinkToFollowPage1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Globals.followPage1.Show();
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            Utils.ExitProgram();
        }
    }
}
