﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FollowUnfollower.Properties;

namespace FollowUnfollower
{
    public partial class UnfollowPage1 : Form
    {
        public UnfollowPage1()
        {
            InitializeComponent();
            TimeOfDayFromPicker1.Value = (DateTime)Settings.Default["TimeFrom1"];
            TimeOfDayToPicker1.Value = (DateTime)Settings.Default["TimeTo1"];
            TimeOfDayFromPicker2.Value = (DateTime)Settings.Default["TimeFrom2"];
            TimeOfDayToPicker2.Value = (DateTime)Settings.Default["TimeTo2"];
        }

        private void SettingsGuideScreen_Load(object sender, EventArgs e)
        {


        }

        private void ContinueButton_Click(object sender, EventArgs e)
        {
            Settings.Default["TimeFrom1"] = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, TimeOfDayFromPicker1.Value.Hour, TimeOfDayFromPicker1.Value.Minute, 0);
            Settings.Default["Timeto1"] = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, TimeOfDayToPicker1.Value.Hour, TimeOfDayToPicker1.Value.Minute, 0);
            Settings.Default["TimeFrom2"] = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, TimeOfDayFromPicker2.Value.Hour, TimeOfDayFromPicker2.Value.Minute, 0);
            Settings.Default["TimeTo2"] = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, TimeOfDayToPicker2.Value.Hour, TimeOfDayToPicker2.Value.Minute, 0);
            Settings.Default.Save();


            this.Hide();
            if (Globals.unfollowPage2 == null)
                Globals.unfollowPage2 = new UnfollowPage2();

            Globals.unfollowPage2.Show();

        }

        private void AddTimesButton_Click(object sender, EventArgs e)
        {
            AddTimesButton.Visible = false;
            TimeOfDayFromPicker2.Visible = true;
            TimeOfDayToPicker2.Visible = true;
        }

        private void HomeButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            Globals.homePage.Show();
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            Utils.ExitProgram();
        }
    }
}
