﻿namespace FollowUnfollower
{
    partial class AddMoreTargetsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TargetAccountsTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.AddTargetsButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TargetAccountsTextBox
            // 
            this.TargetAccountsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.TargetAccountsTextBox.Location = new System.Drawing.Point(32, 42);
            this.TargetAccountsTextBox.MinimumSize = new System.Drawing.Size(322, 130);
            this.TargetAccountsTextBox.Multiline = true;
            this.TargetAccountsTextBox.Name = "TargetAccountsTextBox";
            this.TargetAccountsTextBox.Size = new System.Drawing.Size(322, 130);
            this.TargetAccountsTextBox.TabIndex = 0;
            this.TargetAccountsTextBox.Text = "e.g @puppyinsta\r\n@stevejobs\r\n@fightme";
            this.TargetAccountsTextBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TargetAccountsTextBox_MouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label1.Location = new System.Drawing.Point(29, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(325, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Input the names of target accounts (one per line)";
            // 
            // AddTargetsButton
            // 
            this.AddTargetsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.AddTargetsButton.Location = new System.Drawing.Point(32, 182);
            this.AddTargetsButton.Name = "AddTargetsButton";
            this.AddTargetsButton.Size = new System.Drawing.Size(322, 33);
            this.AddTargetsButton.TabIndex = 2;
            this.AddTargetsButton.Text = "Add all accounts";
            this.AddTargetsButton.UseVisualStyleBackColor = true;
            this.AddTargetsButton.Click += new System.EventHandler(this.AddTargetsButton_Click);
            // 
            // AddMoreTargetsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 220);
            this.Controls.Add(this.AddTargetsButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TargetAccountsTextBox);
            this.Name = "AddMoreTargetsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddMoreTargetsForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TargetAccountsTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button AddTargetsButton;
    }
}