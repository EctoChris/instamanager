﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FollowUnfollower
{
    public partial class UnfollowingPage : Form
    {
        public bool isFollowing = true;
        public UnfollowingPage()
        {
            InitializeComponent();
            LiveSuccessfulFollowsListBox.DrawItem += new DrawItemEventHandler(listBox_DrawItem);
        }

        private void listBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            ListBox List = (ListBox)sender;
            if (e.Index > -1)
            {
                object item = List.Items[e.Index];
                e.DrawBackground();
                e.DrawFocusRectangle();
                Brush brush = new SolidBrush(e.ForeColor);
                SizeF size = e.Graphics.MeasureString(item.ToString(), e.Font);
                e.Graphics.DrawString(item.ToString(), e.Font, brush, e.Bounds.Left + (e.Bounds.Width / 2 - size.Width / 2), e.Bounds.Top + (e.Bounds.Height / 2 - size.Height / 2));
            }
        }
        private void SettingsGuideScreen_Load(object sender, EventArgs e)
        {
            //comment test
        }


        private void EditSettingsButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            Globals.unfollowPage1.Show();

        }

        private void PlayPauseButton_Click(object sender, EventArgs e)
        {
            isFollowing = !isFollowing;
            PlayPauseButton.BackgroundImage = null;

            string newImage;

            if (isFollowing)
                newImage = Const.resourceFolderPath + "InProgressButton.png";
            else
                newImage = Const.resourceFolderPath + "PausedButton.png";

            Image image = Image.FromFile(newImage);
            PlayPauseButton.BackgroundImage = image;
        }

        private void HomeButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            Globals.homePage.Show();
        }
    }
}
