﻿namespace FollowUnfollower
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.setFollowTime = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.followStep3 = new System.Windows.Forms.Label();
            this.TimeOfDayFollowPic = new System.Windows.Forms.PictureBox();
            this.TimeOfDayFollowButton = new System.Windows.Forms.Button();
            this.followStep2 = new System.Windows.Forms.Label();
            this.FollowLimitPic = new System.Windows.Forms.PictureBox();
            this.FollowLimitButton = new System.Windows.Forms.Button();
            this.followStep1 = new System.Windows.Forms.Label();
            this.SleepPicFollow = new System.Windows.Forms.PictureBox();
            this.SleepButtonFollow = new System.Windows.Forms.Button();
            this.submitTimeOfDayF = new System.Windows.Forms.Button();
            this.TimeSelectorUntil2F = new System.Windows.Forms.DateTimePicker();
            this.TimeSelectorFrom2F = new System.Windows.Forms.DateTimePicker();
            this.UntilLabelF = new System.Windows.Forms.Label();
            this.FromLabel2F = new System.Windows.Forms.Label();
            this.addTimeSelectorF = new System.Windows.Forms.Button();
            this.TimeSelectorUntil1F = new System.Windows.Forms.DateTimePicker();
            this.label28 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.TimeSelectorFrom1F = new System.Windows.Forms.DateTimePicker();
            this.submitFollows = new System.Windows.Forms.Button();
            this.maxFollowsLabel = new System.Windows.Forms.Label();
            this.followMaxTrackBar = new System.Windows.Forms.TrackBar();
            this.label27 = new System.Windows.Forms.Label();
            this.followTime2 = new System.Windows.Forms.Label();
            this.followTime1 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.followTrackBar2 = new System.Windows.Forms.TrackBar();
            this.followTrackBar1 = new System.Windows.Forms.TrackBar();
            this.label31 = new System.Windows.Forms.Label();
            this.submitFollowTime = new System.Windows.Forms.Button();
            this.FollowCheck3 = new System.Windows.Forms.CheckBox();
            this.FollowCheck2 = new System.Windows.Forms.CheckBox();
            this.FollowCheck1 = new System.Windows.Forms.CheckBox();
            this.StartFollowPic = new System.Windows.Forms.PictureBox();
            this.FollowStartButton = new System.Windows.Forms.Button();
            this.submitIntervalTime = new System.Windows.Forms.TabPage();
            this.Step3 = new System.Windows.Forms.Label();
            this.setTimeOfDayPic = new System.Windows.Forms.PictureBox();
            this.TimeOfDayButton = new System.Windows.Forms.Button();
            this.Step2 = new System.Windows.Forms.Label();
            this.setUnfollowLimitPic = new System.Windows.Forms.PictureBox();
            this.UnfollowLimitButton = new System.Windows.Forms.Button();
            this.Step1 = new System.Windows.Forms.Label();
            this.setSleepPic = new System.Windows.Forms.PictureBox();
            this.SleepButton = new System.Windows.Forms.Button();
            this.UnfollowStep3Check = new System.Windows.Forms.CheckBox();
            this.UnfollowStep2Check = new System.Windows.Forms.CheckBox();
            this.UnfollowStep1Check = new System.Windows.Forms.CheckBox();
            this.startButton = new System.Windows.Forms.PictureBox();
            this.SubmitTimeOfDay = new System.Windows.Forms.Button();
            this.TimeSelectorUntil2 = new System.Windows.Forms.DateTimePicker();
            this.TimeSelectorFrom2 = new System.Windows.Forms.DateTimePicker();
            this.UntilLabel2 = new System.Windows.Forms.Label();
            this.FromLabel2 = new System.Windows.Forms.Label();
            this.AddTimeSelector = new System.Windows.Forms.Button();
            this.TimeSelectorUntil1 = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TimeSelectorFrom1 = new System.Windows.Forms.DateTimePicker();
            this.submitUnfollows = new System.Windows.Forms.Button();
            this.maxUnfollows = new System.Windows.Forms.Label();
            this.unfollowMaxTrackBar = new System.Windows.Forms.TrackBar();
            this.label3 = new System.Windows.Forms.Label();
            this.unfollowTime2 = new System.Windows.Forms.Label();
            this.unfollowTime1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.unfollowTrackBar2 = new System.Windows.Forms.TrackBar();
            this.unfollowTrackBar1 = new System.Windows.Forms.TrackBar();
            this.SetSleepLabel = new System.Windows.Forms.Label();
            this.StartUnfollowButton = new System.Windows.Forms.Button();
            this.SetUnfollowTime = new System.Windows.Forms.Button();
            this.UnfollowPanel = new System.Windows.Forms.Panel();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.Secs = new System.Windows.Forms.TextBox();
            this.Mins = new System.Windows.Forms.TextBox();
            this.Hrs = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.RunningStatus = new System.Windows.Forms.Label();
            this.CurrentUnfollowAmount = new System.Windows.Forms.Label();
            this.UnfollowBackButton = new System.Windows.Forms.PictureBox();
            this.label22 = new System.Windows.Forms.Label();
            this.TimeUntil2 = new System.Windows.Forms.Label();
            this.TimeFrom2 = new System.Windows.Forms.Label();
            this.TimeUntil1 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.TimeFrom1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.MaxUnfollowDisplay = new System.Windows.Forms.Label();
            this.SleepInterval2 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.SleepInterval1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.SuccessfulUnfollowDisplay = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label6 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.UnfollowPauseButton = new System.Windows.Forms.PictureBox();
            this.UnfollowPlayButton = new System.Windows.Forms.PictureBox();
            this.UnfollowTimer = new System.Windows.Forms.Timer(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.setFollowTime.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TimeOfDayFollowPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FollowLimitPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SleepPicFollow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.followMaxTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.followTrackBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.followTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartFollowPic)).BeginInit();
            this.submitIntervalTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.setTimeOfDayPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.setUnfollowLimitPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.setSleepPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unfollowMaxTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unfollowTrackBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unfollowTrackBar1)).BeginInit();
            this.UnfollowPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UnfollowBackButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnfollowPauseButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnfollowPlayButton)).BeginInit();
            this.SuspendLayout();
            // 
            // setFollowTime
            // 
            this.setFollowTime.Controls.Add(this.tabPage1);
            this.setFollowTime.Controls.Add(this.submitIntervalTime);
            this.setFollowTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.setFollowTime.Location = new System.Drawing.Point(2, 1);
            this.setFollowTime.Name = "setFollowTime";
            this.setFollowTime.Padding = new System.Drawing.Point(30, 10);
            this.setFollowTime.SelectedIndex = 0;
            this.setFollowTime.Size = new System.Drawing.Size(791, 343);
            this.setFollowTime.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.followStep3);
            this.tabPage1.Controls.Add(this.TimeOfDayFollowPic);
            this.tabPage1.Controls.Add(this.TimeOfDayFollowButton);
            this.tabPage1.Controls.Add(this.followStep2);
            this.tabPage1.Controls.Add(this.FollowLimitPic);
            this.tabPage1.Controls.Add(this.FollowLimitButton);
            this.tabPage1.Controls.Add(this.followStep1);
            this.tabPage1.Controls.Add(this.SleepPicFollow);
            this.tabPage1.Controls.Add(this.SleepButtonFollow);
            this.tabPage1.Controls.Add(this.submitTimeOfDayF);
            this.tabPage1.Controls.Add(this.TimeSelectorUntil2F);
            this.tabPage1.Controls.Add(this.TimeSelectorFrom2F);
            this.tabPage1.Controls.Add(this.UntilLabelF);
            this.tabPage1.Controls.Add(this.FromLabel2F);
            this.tabPage1.Controls.Add(this.addTimeSelectorF);
            this.tabPage1.Controls.Add(this.TimeSelectorUntil1F);
            this.tabPage1.Controls.Add(this.label28);
            this.tabPage1.Controls.Add(this.label32);
            this.tabPage1.Controls.Add(this.TimeSelectorFrom1F);
            this.tabPage1.Controls.Add(this.submitFollows);
            this.tabPage1.Controls.Add(this.maxFollowsLabel);
            this.tabPage1.Controls.Add(this.followMaxTrackBar);
            this.tabPage1.Controls.Add(this.label27);
            this.tabPage1.Controls.Add(this.followTime2);
            this.tabPage1.Controls.Add(this.followTime1);
            this.tabPage1.Controls.Add(this.label29);
            this.tabPage1.Controls.Add(this.label30);
            this.tabPage1.Controls.Add(this.followTrackBar2);
            this.tabPage1.Controls.Add(this.followTrackBar1);
            this.tabPage1.Controls.Add(this.label31);
            this.tabPage1.Controls.Add(this.submitFollowTime);
            this.tabPage1.Controls.Add(this.FollowCheck3);
            this.tabPage1.Controls.Add(this.FollowCheck2);
            this.tabPage1.Controls.Add(this.FollowCheck1);
            this.tabPage1.Controls.Add(this.StartFollowPic);
            this.tabPage1.Controls.Add(this.FollowStartButton);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tabPage1.Location = new System.Drawing.Point(4, 49);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(783, 290);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Follower";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // followStep3
            // 
            this.followStep3.AutoSize = true;
            this.followStep3.BackColor = System.Drawing.Color.LightCyan;
            this.followStep3.Cursor = System.Windows.Forms.Cursors.No;
            this.followStep3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.followStep3.Location = new System.Drawing.Point(15, 233);
            this.followStep3.Name = "followStep3";
            this.followStep3.Size = new System.Drawing.Size(81, 26);
            this.followStep3.TabIndex = 30;
            this.followStep3.Text = "Step 3:";
            // 
            // TimeOfDayFollowPic
            // 
            this.TimeOfDayFollowPic.BackColor = System.Drawing.Color.PaleTurquoise;
            this.TimeOfDayFollowPic.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("TimeOfDayFollowPic.BackgroundImage")));
            this.TimeOfDayFollowPic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.TimeOfDayFollowPic.Location = new System.Drawing.Point(156, 178);
            this.TimeOfDayFollowPic.Name = "TimeOfDayFollowPic";
            this.TimeOfDayFollowPic.Size = new System.Drawing.Size(82, 81);
            this.TimeOfDayFollowPic.TabIndex = 29;
            this.TimeOfDayFollowPic.TabStop = false;
            this.TimeOfDayFollowPic.Click += new System.EventHandler(this.TimeOfDayFollowButton_Click);
            // 
            // TimeOfDayFollowButton
            // 
            this.TimeOfDayFollowButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("TimeOfDayFollowButton.BackgroundImage")));
            this.TimeOfDayFollowButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.TimeOfDayFollowButton.FlatAppearance.BorderSize = 2;
            this.TimeOfDayFollowButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TimeOfDayFollowButton.Location = new System.Drawing.Point(7, 141);
            this.TimeOfDayFollowButton.Name = "TimeOfDayFollowButton";
            this.TimeOfDayFollowButton.Size = new System.Drawing.Size(381, 134);
            this.TimeOfDayFollowButton.TabIndex = 28;
            this.TimeOfDayFollowButton.Text = "Set Time Of Day";
            this.TimeOfDayFollowButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.TimeOfDayFollowButton.UseVisualStyleBackColor = true;
            this.TimeOfDayFollowButton.Click += new System.EventHandler(this.TimeOfDayFollowButton_Click);
            // 
            // followStep2
            // 
            this.followStep2.AutoSize = true;
            this.followStep2.BackColor = System.Drawing.Color.LightCyan;
            this.followStep2.Cursor = System.Windows.Forms.Cursors.No;
            this.followStep2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.followStep2.Location = new System.Drawing.Point(406, 99);
            this.followStep2.Name = "followStep2";
            this.followStep2.Size = new System.Drawing.Size(81, 26);
            this.followStep2.TabIndex = 27;
            this.followStep2.Text = "Step 2:";
            // 
            // FollowLimitPic
            // 
            this.FollowLimitPic.BackColor = System.Drawing.Color.PaleTurquoise;
            this.FollowLimitPic.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("FollowLimitPic.BackgroundImage")));
            this.FollowLimitPic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FollowLimitPic.Location = new System.Drawing.Point(550, 41);
            this.FollowLimitPic.Name = "FollowLimitPic";
            this.FollowLimitPic.Size = new System.Drawing.Size(85, 80);
            this.FollowLimitPic.TabIndex = 26;
            this.FollowLimitPic.TabStop = false;
            this.FollowLimitPic.Click += new System.EventHandler(this.FollowLimitButton_Click);
            // 
            // FollowLimitButton
            // 
            this.FollowLimitButton.BackColor = System.Drawing.Color.Lavender;
            this.FollowLimitButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("FollowLimitButton.BackgroundImage")));
            this.FollowLimitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.FollowLimitButton.FlatAppearance.BorderColor = System.Drawing.Color.Blue;
            this.FollowLimitButton.FlatAppearance.BorderSize = 2;
            this.FollowLimitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FollowLimitButton.Location = new System.Drawing.Point(395, 4);
            this.FollowLimitButton.Name = "FollowLimitButton";
            this.FollowLimitButton.Size = new System.Drawing.Size(382, 134);
            this.FollowLimitButton.TabIndex = 25;
            this.FollowLimitButton.Text = "Set Unfollow Limit";
            this.FollowLimitButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.FollowLimitButton.UseVisualStyleBackColor = false;
            this.FollowLimitButton.Click += new System.EventHandler(this.FollowLimitButton_Click);
            // 
            // followStep1
            // 
            this.followStep1.AutoSize = true;
            this.followStep1.BackColor = System.Drawing.Color.LightCyan;
            this.followStep1.CausesValidation = false;
            this.followStep1.Cursor = System.Windows.Forms.Cursors.No;
            this.followStep1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.followStep1.Location = new System.Drawing.Point(17, 100);
            this.followStep1.Name = "followStep1";
            this.followStep1.Size = new System.Drawing.Size(81, 26);
            this.followStep1.TabIndex = 24;
            this.followStep1.Text = "Step 1:";
            // 
            // SleepPicFollow
            // 
            this.SleepPicFollow.BackColor = System.Drawing.Color.PaleTurquoise;
            this.SleepPicFollow.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("SleepPicFollow.BackgroundImage")));
            this.SleepPicFollow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SleepPicFollow.Location = new System.Drawing.Point(151, 40);
            this.SleepPicFollow.Name = "SleepPicFollow";
            this.SleepPicFollow.Size = new System.Drawing.Size(83, 80);
            this.SleepPicFollow.TabIndex = 23;
            this.SleepPicFollow.TabStop = false;
            this.SleepPicFollow.Click += new System.EventHandler(this.SleepButtonFollow_Click);
            // 
            // SleepButtonFollow
            // 
            this.SleepButtonFollow.BackColor = System.Drawing.Color.Lavender;
            this.SleepButtonFollow.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("SleepButtonFollow.BackgroundImage")));
            this.SleepButtonFollow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SleepButtonFollow.FlatAppearance.BorderColor = System.Drawing.Color.Blue;
            this.SleepButtonFollow.FlatAppearance.BorderSize = 2;
            this.SleepButtonFollow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SleepButtonFollow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.SleepButtonFollow.Location = new System.Drawing.Point(7, 3);
            this.SleepButtonFollow.Name = "SleepButtonFollow";
            this.SleepButtonFollow.Size = new System.Drawing.Size(381, 134);
            this.SleepButtonFollow.TabIndex = 22;
            this.SleepButtonFollow.Text = "Set Sleep Intervals";
            this.SleepButtonFollow.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.SleepButtonFollow.UseVisualStyleBackColor = false;
            this.SleepButtonFollow.Click += new System.EventHandler(this.SleepButtonFollow_Click);
            // 
            // submitTimeOfDayF
            // 
            this.submitTimeOfDayF.BackColor = System.Drawing.Color.White;
            this.submitTimeOfDayF.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.submitTimeOfDayF.FlatAppearance.BorderColor = System.Drawing.Color.DarkOrange;
            this.submitTimeOfDayF.FlatAppearance.BorderSize = 2;
            this.submitTimeOfDayF.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.submitTimeOfDayF.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.submitTimeOfDayF.ForeColor = System.Drawing.Color.Black;
            this.submitTimeOfDayF.Location = new System.Drawing.Point(301, 230);
            this.submitTimeOfDayF.Name = "submitTimeOfDayF";
            this.submitTimeOfDayF.Size = new System.Drawing.Size(76, 34);
            this.submitTimeOfDayF.TabIndex = 63;
            this.submitTimeOfDayF.Text = "Submit";
            this.submitTimeOfDayF.UseVisualStyleBackColor = false;
            this.submitTimeOfDayF.Click += new System.EventHandler(this.submitTimeOfDayF_Click);
            // 
            // TimeSelectorUntil2F
            // 
            this.TimeSelectorUntil2F.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimeSelectorUntil2F.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.TimeSelectorUntil2F.Location = new System.Drawing.Point(260, 199);
            this.TimeSelectorUntil2F.Name = "TimeSelectorUntil2F";
            this.TimeSelectorUntil2F.ShowUpDown = true;
            this.TimeSelectorUntil2F.Size = new System.Drawing.Size(128, 27);
            this.TimeSelectorUntil2F.TabIndex = 62;
            this.TimeSelectorUntil2F.Value = new System.DateTime(2017, 10, 25, 12, 0, 0, 0);
            // 
            // TimeSelectorFrom2F
            // 
            this.TimeSelectorFrom2F.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimeSelectorFrom2F.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.TimeSelectorFrom2F.Location = new System.Drawing.Point(260, 162);
            this.TimeSelectorFrom2F.Name = "TimeSelectorFrom2F";
            this.TimeSelectorFrom2F.ShowUpDown = true;
            this.TimeSelectorFrom2F.Size = new System.Drawing.Size(128, 27);
            this.TimeSelectorFrom2F.TabIndex = 61;
            this.TimeSelectorFrom2F.Value = new System.DateTime(2017, 10, 25, 12, 0, 0, 0);
            // 
            // UntilLabelF
            // 
            this.UntilLabelF.AutoSize = true;
            this.UntilLabelF.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UntilLabelF.ForeColor = System.Drawing.Color.Black;
            this.UntilLabelF.Location = new System.Drawing.Point(206, 202);
            this.UntilLabelF.Name = "UntilLabelF";
            this.UntilLabelF.Size = new System.Drawing.Size(48, 20);
            this.UntilLabelF.TabIndex = 60;
            this.UntilLabelF.Text = "Until:";
            // 
            // FromLabel2F
            // 
            this.FromLabel2F.AutoSize = true;
            this.FromLabel2F.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FromLabel2F.ForeColor = System.Drawing.Color.Black;
            this.FromLabel2F.Location = new System.Drawing.Point(200, 162);
            this.FromLabel2F.Name = "FromLabel2F";
            this.FromLabel2F.Size = new System.Drawing.Size(53, 20);
            this.FromLabel2F.TabIndex = 59;
            this.FromLabel2F.Text = "From:";
            // 
            // addTimeSelectorF
            // 
            this.addTimeSelectorF.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("addTimeSelectorF.BackgroundImage")));
            this.addTimeSelectorF.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.addTimeSelectorF.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addTimeSelectorF.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addTimeSelectorF.Location = new System.Drawing.Point(23, 232);
            this.addTimeSelectorF.Name = "addTimeSelectorF";
            this.addTimeSelectorF.Size = new System.Drawing.Size(183, 30);
            this.addTimeSelectorF.TabIndex = 58;
            this.addTimeSelectorF.Text = "+ Add Another TimeSlot";
            this.addTimeSelectorF.UseVisualStyleBackColor = true;
            this.addTimeSelectorF.Click += new System.EventHandler(this.addTimeSelectorF_Click);
            // 
            // TimeSelectorUntil1F
            // 
            this.TimeSelectorUntil1F.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimeSelectorUntil1F.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.TimeSelectorUntil1F.Location = new System.Drawing.Point(72, 199);
            this.TimeSelectorUntil1F.Name = "TimeSelectorUntil1F";
            this.TimeSelectorUntil1F.ShowUpDown = true;
            this.TimeSelectorUntil1F.Size = new System.Drawing.Size(128, 27);
            this.TimeSelectorUntil1F.TabIndex = 57;
            this.TimeSelectorUntil1F.Value = new System.DateTime(2017, 10, 25, 12, 0, 0, 0);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(18, 202);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(48, 20);
            this.label28.TabIndex = 56;
            this.label28.Text = "Until:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(13, 162);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(53, 20);
            this.label32.TabIndex = 55;
            this.label32.Text = "From:";
            // 
            // TimeSelectorFrom1F
            // 
            this.TimeSelectorFrom1F.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimeSelectorFrom1F.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.TimeSelectorFrom1F.Location = new System.Drawing.Point(72, 162);
            this.TimeSelectorFrom1F.Name = "TimeSelectorFrom1F";
            this.TimeSelectorFrom1F.ShowUpDown = true;
            this.TimeSelectorFrom1F.Size = new System.Drawing.Size(128, 27);
            this.TimeSelectorFrom1F.TabIndex = 54;
            this.TimeSelectorFrom1F.Value = new System.DateTime(2017, 10, 25, 12, 0, 0, 0);
            // 
            // submitFollows
            // 
            this.submitFollows.BackColor = System.Drawing.Color.White;
            this.submitFollows.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.submitFollows.FlatAppearance.BorderColor = System.Drawing.Color.DarkOrange;
            this.submitFollows.FlatAppearance.BorderSize = 2;
            this.submitFollows.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.submitFollows.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.submitFollows.ForeColor = System.Drawing.Color.Black;
            this.submitFollows.Location = new System.Drawing.Point(698, 55);
            this.submitFollows.Name = "submitFollows";
            this.submitFollows.Size = new System.Drawing.Size(76, 34);
            this.submitFollows.TabIndex = 53;
            this.submitFollows.Text = "Submit";
            this.submitFollows.UseVisualStyleBackColor = false;
            this.submitFollows.Click += new System.EventHandler(this.submitFollows_Click);
            // 
            // maxFollowsLabel
            // 
            this.maxFollowsLabel.AutoSize = true;
            this.maxFollowsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxFollowsLabel.Location = new System.Drawing.Point(637, 57);
            this.maxFollowsLabel.Name = "maxFollowsLabel";
            this.maxFollowsLabel.Size = new System.Drawing.Size(27, 29);
            this.maxFollowsLabel.TabIndex = 52;
            this.maxFollowsLabel.Text = "0";
            // 
            // followMaxTrackBar
            // 
            this.followMaxTrackBar.BackColor = System.Drawing.SystemColors.Control;
            this.followMaxTrackBar.Location = new System.Drawing.Point(413, 50);
            this.followMaxTrackBar.Maximum = 1000;
            this.followMaxTrackBar.Name = "followMaxTrackBar";
            this.followMaxTrackBar.Size = new System.Drawing.Size(218, 45);
            this.followMaxTrackBar.TabIndex = 51;
            this.followMaxTrackBar.Scroll += new System.EventHandler(this.followMaxTrackBar_Scroll);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(511, 4);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(161, 24);
            this.label27.TabIndex = 50;
            this.label27.Text = "Max Daily Follows";
            // 
            // followTime2
            // 
            this.followTime2.AutoSize = true;
            this.followTime2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.followTime2.Location = new System.Drawing.Point(240, 97);
            this.followTime2.Name = "followTime2";
            this.followTime2.Size = new System.Drawing.Size(27, 29);
            this.followTime2.TabIndex = 48;
            this.followTime2.Text = "0";
            // 
            // followTime1
            // 
            this.followTime1.AutoSize = true;
            this.followTime1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.followTime1.Location = new System.Drawing.Point(240, 34);
            this.followTime1.Name = "followTime1";
            this.followTime1.Size = new System.Drawing.Size(27, 29);
            this.followTime1.TabIndex = 47;
            this.followTime1.Text = "0";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(298, 7);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(86, 20);
            this.label29.TabIndex = 46;
            this.label29.Text = "(Seconds)";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(240, 72);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(25, 17);
            this.label30.TabIndex = 45;
            this.label30.Text = "To";
            // 
            // followTrackBar2
            // 
            this.followTrackBar2.Location = new System.Drawing.Point(16, 85);
            this.followTrackBar2.Maximum = 300;
            this.followTrackBar2.Name = "followTrackBar2";
            this.followTrackBar2.Size = new System.Drawing.Size(218, 45);
            this.followTrackBar2.TabIndex = 44;
            this.followTrackBar2.Scroll += new System.EventHandler(this.followTrackBar2_Scroll);
            // 
            // followTrackBar1
            // 
            this.followTrackBar1.BackColor = System.Drawing.SystemColors.Control;
            this.followTrackBar1.Location = new System.Drawing.Point(16, 34);
            this.followTrackBar1.Maximum = 300;
            this.followTrackBar1.Name = "followTrackBar1";
            this.followTrackBar1.Size = new System.Drawing.Size(218, 45);
            this.followTrackBar1.TabIndex = 43;
            this.followTrackBar1.Scroll += new System.EventHandler(this.followTrackBar1_Scroll);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(78, 7);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(207, 24);
            this.label31.TabIndex = 42;
            this.label31.Text = "Time Between Follows:";
            // 
            // submitFollowTime
            // 
            this.submitFollowTime.BackColor = System.Drawing.Color.White;
            this.submitFollowTime.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.submitFollowTime.FlatAppearance.BorderColor = System.Drawing.Color.DarkOrange;
            this.submitFollowTime.FlatAppearance.BorderSize = 2;
            this.submitFollowTime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.submitFollowTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.submitFollowTime.ForeColor = System.Drawing.Color.Black;
            this.submitFollowTime.Location = new System.Drawing.Point(293, 58);
            this.submitFollowTime.Name = "submitFollowTime";
            this.submitFollowTime.Size = new System.Drawing.Size(76, 34);
            this.submitFollowTime.TabIndex = 49;
            this.submitFollowTime.Text = "Submit";
            this.submitFollowTime.UseVisualStyleBackColor = false;
            this.submitFollowTime.Click += new System.EventHandler(this.submitFollowTime_Click);
            // 
            // FollowCheck3
            // 
            this.FollowCheck3.AutoCheck = false;
            this.FollowCheck3.AutoSize = true;
            this.FollowCheck3.BackColor = System.Drawing.Color.LightCyan;
            this.FollowCheck3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.FollowCheck3.Location = new System.Drawing.Point(409, 230);
            this.FollowCheck3.Name = "FollowCheck3";
            this.FollowCheck3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.FollowCheck3.Size = new System.Drawing.Size(81, 24);
            this.FollowCheck3.TabIndex = 41;
            this.FollowCheck3.Text = ":Step 3";
            this.FollowCheck3.UseVisualStyleBackColor = false;
            // 
            // FollowCheck2
            // 
            this.FollowCheck2.AutoCheck = false;
            this.FollowCheck2.AutoSize = true;
            this.FollowCheck2.BackColor = System.Drawing.Color.LightCyan;
            this.FollowCheck2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.FollowCheck2.Location = new System.Drawing.Point(409, 196);
            this.FollowCheck2.Name = "FollowCheck2";
            this.FollowCheck2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.FollowCheck2.Size = new System.Drawing.Size(81, 24);
            this.FollowCheck2.TabIndex = 40;
            this.FollowCheck2.Text = ":Step 2";
            this.FollowCheck2.UseVisualStyleBackColor = false;
            // 
            // FollowCheck1
            // 
            this.FollowCheck1.AutoCheck = false;
            this.FollowCheck1.AutoSize = true;
            this.FollowCheck1.BackColor = System.Drawing.Color.LightCyan;
            this.FollowCheck1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.FollowCheck1.Location = new System.Drawing.Point(409, 162);
            this.FollowCheck1.Name = "FollowCheck1";
            this.FollowCheck1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.FollowCheck1.Size = new System.Drawing.Size(81, 24);
            this.FollowCheck1.TabIndex = 39;
            this.FollowCheck1.Text = ":Step 1";
            this.FollowCheck1.UseVisualStyleBackColor = false;
            this.FollowCheck1.CheckStateChanged += new System.EventHandler(this.FollowCheck1_CheckStateChanged);
            // 
            // StartFollowPic
            // 
            this.StartFollowPic.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("StartFollowPic.BackgroundImage")));
            this.StartFollowPic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.StartFollowPic.Location = new System.Drawing.Point(533, 162);
            this.StartFollowPic.Name = "StartFollowPic";
            this.StartFollowPic.Size = new System.Drawing.Size(110, 100);
            this.StartFollowPic.TabIndex = 38;
            this.StartFollowPic.TabStop = false;
            // 
            // FollowStartButton
            // 
            this.FollowStartButton.BackColor = System.Drawing.Color.Transparent;
            this.FollowStartButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("FollowStartButton.BackgroundImage")));
            this.FollowStartButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.FollowStartButton.FlatAppearance.BorderColor = System.Drawing.Color.Blue;
            this.FollowStartButton.FlatAppearance.BorderSize = 2;
            this.FollowStartButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FollowStartButton.ForeColor = System.Drawing.Color.Red;
            this.FollowStartButton.Location = new System.Drawing.Point(395, 143);
            this.FollowStartButton.Name = "FollowStartButton";
            this.FollowStartButton.Size = new System.Drawing.Size(381, 134);
            this.FollowStartButton.TabIndex = 37;
            this.FollowStartButton.UseVisualStyleBackColor = false;
            // 
            // submitIntervalTime
            // 
            this.submitIntervalTime.Controls.Add(this.Step3);
            this.submitIntervalTime.Controls.Add(this.setTimeOfDayPic);
            this.submitIntervalTime.Controls.Add(this.TimeOfDayButton);
            this.submitIntervalTime.Controls.Add(this.Step2);
            this.submitIntervalTime.Controls.Add(this.setUnfollowLimitPic);
            this.submitIntervalTime.Controls.Add(this.UnfollowLimitButton);
            this.submitIntervalTime.Controls.Add(this.Step1);
            this.submitIntervalTime.Controls.Add(this.setSleepPic);
            this.submitIntervalTime.Controls.Add(this.SleepButton);
            this.submitIntervalTime.Controls.Add(this.UnfollowStep3Check);
            this.submitIntervalTime.Controls.Add(this.UnfollowStep2Check);
            this.submitIntervalTime.Controls.Add(this.UnfollowStep1Check);
            this.submitIntervalTime.Controls.Add(this.startButton);
            this.submitIntervalTime.Controls.Add(this.SubmitTimeOfDay);
            this.submitIntervalTime.Controls.Add(this.TimeSelectorUntil2);
            this.submitIntervalTime.Controls.Add(this.TimeSelectorFrom2);
            this.submitIntervalTime.Controls.Add(this.UntilLabel2);
            this.submitIntervalTime.Controls.Add(this.FromLabel2);
            this.submitIntervalTime.Controls.Add(this.AddTimeSelector);
            this.submitIntervalTime.Controls.Add(this.TimeSelectorUntil1);
            this.submitIntervalTime.Controls.Add(this.label5);
            this.submitIntervalTime.Controls.Add(this.label4);
            this.submitIntervalTime.Controls.Add(this.TimeSelectorFrom1);
            this.submitIntervalTime.Controls.Add(this.submitUnfollows);
            this.submitIntervalTime.Controls.Add(this.maxUnfollows);
            this.submitIntervalTime.Controls.Add(this.unfollowMaxTrackBar);
            this.submitIntervalTime.Controls.Add(this.label3);
            this.submitIntervalTime.Controls.Add(this.unfollowTime2);
            this.submitIntervalTime.Controls.Add(this.unfollowTime1);
            this.submitIntervalTime.Controls.Add(this.label2);
            this.submitIntervalTime.Controls.Add(this.label1);
            this.submitIntervalTime.Controls.Add(this.unfollowTrackBar2);
            this.submitIntervalTime.Controls.Add(this.unfollowTrackBar1);
            this.submitIntervalTime.Controls.Add(this.SetSleepLabel);
            this.submitIntervalTime.Controls.Add(this.StartUnfollowButton);
            this.submitIntervalTime.Controls.Add(this.SetUnfollowTime);
            this.submitIntervalTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.submitIntervalTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.submitIntervalTime.Location = new System.Drawing.Point(4, 49);
            this.submitIntervalTime.Name = "submitIntervalTime";
            this.submitIntervalTime.Padding = new System.Windows.Forms.Padding(3);
            this.submitIntervalTime.Size = new System.Drawing.Size(783, 285);
            this.submitIntervalTime.TabIndex = 1;
            this.submitIntervalTime.Text = "Unfollower";
            this.submitIntervalTime.UseVisualStyleBackColor = true;
            // 
            // Step3
            // 
            this.Step3.AutoSize = true;
            this.Step3.BackColor = System.Drawing.Color.LightCyan;
            this.Step3.Cursor = System.Windows.Forms.Cursors.No;
            this.Step3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Step3.Location = new System.Drawing.Point(15, 241);
            this.Step3.Name = "Step3";
            this.Step3.Size = new System.Drawing.Size(81, 26);
            this.Step3.TabIndex = 23;
            this.Step3.Text = "Step 3:";
            // 
            // setTimeOfDayPic
            // 
            this.setTimeOfDayPic.BackColor = System.Drawing.Color.PaleTurquoise;
            this.setTimeOfDayPic.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("setTimeOfDayPic.BackgroundImage")));
            this.setTimeOfDayPic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.setTimeOfDayPic.Location = new System.Drawing.Point(156, 186);
            this.setTimeOfDayPic.Name = "setTimeOfDayPic";
            this.setTimeOfDayPic.Size = new System.Drawing.Size(82, 81);
            this.setTimeOfDayPic.TabIndex = 6;
            this.setTimeOfDayPic.TabStop = false;
            this.setTimeOfDayPic.Click += new System.EventHandler(this.TimeOfDayButton_Click);
            // 
            // TimeOfDayButton
            // 
            this.TimeOfDayButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("TimeOfDayButton.BackgroundImage")));
            this.TimeOfDayButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.TimeOfDayButton.FlatAppearance.BorderSize = 2;
            this.TimeOfDayButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TimeOfDayButton.Location = new System.Drawing.Point(7, 145);
            this.TimeOfDayButton.Name = "TimeOfDayButton";
            this.TimeOfDayButton.Size = new System.Drawing.Size(381, 134);
            this.TimeOfDayButton.TabIndex = 3;
            this.TimeOfDayButton.Text = "Set Time Of Day";
            this.TimeOfDayButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.TimeOfDayButton.UseVisualStyleBackColor = true;
            this.TimeOfDayButton.Click += new System.EventHandler(this.TimeOfDayButton_Click);
            // 
            // Step2
            // 
            this.Step2.AutoSize = true;
            this.Step2.BackColor = System.Drawing.Color.LightCyan;
            this.Step2.Cursor = System.Windows.Forms.Cursors.No;
            this.Step2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Step2.Location = new System.Drawing.Point(401, 103);
            this.Step2.Name = "Step2";
            this.Step2.Size = new System.Drawing.Size(81, 26);
            this.Step2.TabIndex = 22;
            this.Step2.Text = "Step 2:";
            // 
            // setUnfollowLimitPic
            // 
            this.setUnfollowLimitPic.BackColor = System.Drawing.Color.PaleTurquoise;
            this.setUnfollowLimitPic.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("setUnfollowLimitPic.BackgroundImage")));
            this.setUnfollowLimitPic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.setUnfollowLimitPic.Location = new System.Drawing.Point(545, 45);
            this.setUnfollowLimitPic.Name = "setUnfollowLimitPic";
            this.setUnfollowLimitPic.Size = new System.Drawing.Size(85, 80);
            this.setUnfollowLimitPic.TabIndex = 5;
            this.setUnfollowLimitPic.TabStop = false;
            this.setUnfollowLimitPic.Click += new System.EventHandler(this.UnfollowLimitButton_Click);
            // 
            // UnfollowLimitButton
            // 
            this.UnfollowLimitButton.BackColor = System.Drawing.Color.Lavender;
            this.UnfollowLimitButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("UnfollowLimitButton.BackgroundImage")));
            this.UnfollowLimitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.UnfollowLimitButton.FlatAppearance.BorderColor = System.Drawing.Color.Blue;
            this.UnfollowLimitButton.FlatAppearance.BorderSize = 2;
            this.UnfollowLimitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UnfollowLimitButton.Location = new System.Drawing.Point(392, 6);
            this.UnfollowLimitButton.Name = "UnfollowLimitButton";
            this.UnfollowLimitButton.Size = new System.Drawing.Size(381, 134);
            this.UnfollowLimitButton.TabIndex = 1;
            this.UnfollowLimitButton.Text = "Set Unfollow Limit";
            this.UnfollowLimitButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.UnfollowLimitButton.UseVisualStyleBackColor = false;
            this.UnfollowLimitButton.Click += new System.EventHandler(this.UnfollowLimitButton_Click);
            // 
            // Step1
            // 
            this.Step1.AutoSize = true;
            this.Step1.BackColor = System.Drawing.Color.LightCyan;
            this.Step1.CausesValidation = false;
            this.Step1.Cursor = System.Windows.Forms.Cursors.No;
            this.Step1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Step1.Location = new System.Drawing.Point(12, 103);
            this.Step1.Name = "Step1";
            this.Step1.Size = new System.Drawing.Size(81, 26);
            this.Step1.TabIndex = 21;
            this.Step1.Text = "Step 1:";
            // 
            // setSleepPic
            // 
            this.setSleepPic.BackColor = System.Drawing.Color.PaleTurquoise;
            this.setSleepPic.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("setSleepPic.BackgroundImage")));
            this.setSleepPic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.setSleepPic.Location = new System.Drawing.Point(153, 45);
            this.setSleepPic.Name = "setSleepPic";
            this.setSleepPic.Size = new System.Drawing.Size(83, 80);
            this.setSleepPic.TabIndex = 4;
            this.setSleepPic.TabStop = false;
            this.setSleepPic.Click += new System.EventHandler(this.SleepButton_Click);
            // 
            // SleepButton
            // 
            this.SleepButton.BackColor = System.Drawing.Color.Lavender;
            this.SleepButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("SleepButton.BackgroundImage")));
            this.SleepButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SleepButton.FlatAppearance.BorderColor = System.Drawing.Color.Blue;
            this.SleepButton.FlatAppearance.BorderSize = 2;
            this.SleepButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SleepButton.Location = new System.Drawing.Point(5, 6);
            this.SleepButton.Name = "SleepButton";
            this.SleepButton.Size = new System.Drawing.Size(381, 134);
            this.SleepButton.TabIndex = 0;
            this.SleepButton.Text = "Set Sleep Intervals";
            this.SleepButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.SleepButton.UseVisualStyleBackColor = false;
            this.SleepButton.Click += new System.EventHandler(this.SleepButton_Click);
            // 
            // UnfollowStep3Check
            // 
            this.UnfollowStep3Check.AutoCheck = false;
            this.UnfollowStep3Check.AutoSize = true;
            this.UnfollowStep3Check.BackColor = System.Drawing.Color.LightCyan;
            this.UnfollowStep3Check.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.UnfollowStep3Check.Location = new System.Drawing.Point(406, 233);
            this.UnfollowStep3Check.Name = "UnfollowStep3Check";
            this.UnfollowStep3Check.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.UnfollowStep3Check.Size = new System.Drawing.Size(81, 24);
            this.UnfollowStep3Check.TabIndex = 36;
            this.UnfollowStep3Check.Text = ":Step 3";
            this.UnfollowStep3Check.UseVisualStyleBackColor = false;
            this.UnfollowStep3Check.CheckStateChanged += new System.EventHandler(this.UnfollowStep1Check_CheckStateChanged);
            // 
            // UnfollowStep2Check
            // 
            this.UnfollowStep2Check.AutoCheck = false;
            this.UnfollowStep2Check.AutoSize = true;
            this.UnfollowStep2Check.BackColor = System.Drawing.Color.LightCyan;
            this.UnfollowStep2Check.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.UnfollowStep2Check.Location = new System.Drawing.Point(406, 199);
            this.UnfollowStep2Check.Name = "UnfollowStep2Check";
            this.UnfollowStep2Check.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.UnfollowStep2Check.Size = new System.Drawing.Size(81, 24);
            this.UnfollowStep2Check.TabIndex = 35;
            this.UnfollowStep2Check.Text = ":Step 2";
            this.UnfollowStep2Check.UseVisualStyleBackColor = false;
            this.UnfollowStep2Check.CheckStateChanged += new System.EventHandler(this.UnfollowStep1Check_CheckStateChanged);
            // 
            // UnfollowStep1Check
            // 
            this.UnfollowStep1Check.AutoCheck = false;
            this.UnfollowStep1Check.AutoSize = true;
            this.UnfollowStep1Check.BackColor = System.Drawing.Color.LightCyan;
            this.UnfollowStep1Check.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.UnfollowStep1Check.Location = new System.Drawing.Point(406, 165);
            this.UnfollowStep1Check.Name = "UnfollowStep1Check";
            this.UnfollowStep1Check.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.UnfollowStep1Check.Size = new System.Drawing.Size(81, 24);
            this.UnfollowStep1Check.TabIndex = 34;
            this.UnfollowStep1Check.Text = ":Step 1";
            this.UnfollowStep1Check.UseVisualStyleBackColor = false;
            this.UnfollowStep1Check.CheckStateChanged += new System.EventHandler(this.UnfollowStep1Check_CheckStateChanged);
            // 
            // startButton
            // 
            this.startButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("startButton.BackgroundImage")));
            this.startButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.startButton.Location = new System.Drawing.Point(530, 165);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(110, 100);
            this.startButton.TabIndex = 33;
            this.startButton.TabStop = false;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // SubmitTimeOfDay
            // 
            this.SubmitTimeOfDay.BackColor = System.Drawing.Color.White;
            this.SubmitTimeOfDay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.SubmitTimeOfDay.FlatAppearance.BorderColor = System.Drawing.Color.DarkOrange;
            this.SubmitTimeOfDay.FlatAppearance.BorderSize = 2;
            this.SubmitTimeOfDay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SubmitTimeOfDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubmitTimeOfDay.ForeColor = System.Drawing.Color.Black;
            this.SubmitTimeOfDay.Location = new System.Drawing.Point(295, 233);
            this.SubmitTimeOfDay.Name = "SubmitTimeOfDay";
            this.SubmitTimeOfDay.Size = new System.Drawing.Size(76, 34);
            this.SubmitTimeOfDay.TabIndex = 32;
            this.SubmitTimeOfDay.Text = "Submit";
            this.SubmitTimeOfDay.UseVisualStyleBackColor = false;
            this.SubmitTimeOfDay.Click += new System.EventHandler(this.SubmitTimeOfDay_Click);
            // 
            // TimeSelectorUntil2
            // 
            this.TimeSelectorUntil2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimeSelectorUntil2.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.TimeSelectorUntil2.Location = new System.Drawing.Point(254, 202);
            this.TimeSelectorUntil2.Name = "TimeSelectorUntil2";
            this.TimeSelectorUntil2.ShowUpDown = true;
            this.TimeSelectorUntil2.Size = new System.Drawing.Size(128, 27);
            this.TimeSelectorUntil2.TabIndex = 31;
            this.TimeSelectorUntil2.Value = new System.DateTime(2017, 10, 25, 12, 0, 0, 0);
            // 
            // TimeSelectorFrom2
            // 
            this.TimeSelectorFrom2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimeSelectorFrom2.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.TimeSelectorFrom2.Location = new System.Drawing.Point(254, 165);
            this.TimeSelectorFrom2.Name = "TimeSelectorFrom2";
            this.TimeSelectorFrom2.ShowUpDown = true;
            this.TimeSelectorFrom2.Size = new System.Drawing.Size(128, 27);
            this.TimeSelectorFrom2.TabIndex = 30;
            this.TimeSelectorFrom2.Value = new System.DateTime(2017, 10, 25, 12, 0, 0, 0);
            // 
            // UntilLabel2
            // 
            this.UntilLabel2.AutoSize = true;
            this.UntilLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UntilLabel2.ForeColor = System.Drawing.Color.Black;
            this.UntilLabel2.Location = new System.Drawing.Point(200, 205);
            this.UntilLabel2.Name = "UntilLabel2";
            this.UntilLabel2.Size = new System.Drawing.Size(48, 20);
            this.UntilLabel2.TabIndex = 29;
            this.UntilLabel2.Text = "Until:";
            // 
            // FromLabel2
            // 
            this.FromLabel2.AutoSize = true;
            this.FromLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FromLabel2.ForeColor = System.Drawing.Color.Black;
            this.FromLabel2.Location = new System.Drawing.Point(194, 165);
            this.FromLabel2.Name = "FromLabel2";
            this.FromLabel2.Size = new System.Drawing.Size(53, 20);
            this.FromLabel2.TabIndex = 28;
            this.FromLabel2.Text = "From:";
            // 
            // AddTimeSelector
            // 
            this.AddTimeSelector.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("AddTimeSelector.BackgroundImage")));
            this.AddTimeSelector.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AddTimeSelector.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddTimeSelector.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddTimeSelector.Location = new System.Drawing.Point(17, 235);
            this.AddTimeSelector.Name = "AddTimeSelector";
            this.AddTimeSelector.Size = new System.Drawing.Size(183, 30);
            this.AddTimeSelector.TabIndex = 27;
            this.AddTimeSelector.Text = "+ Add Another TimeSlot";
            this.AddTimeSelector.UseVisualStyleBackColor = true;
            this.AddTimeSelector.Click += new System.EventHandler(this.AddTimeSelector_Click);
            // 
            // TimeSelectorUntil1
            // 
            this.TimeSelectorUntil1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimeSelectorUntil1.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.TimeSelectorUntil1.Location = new System.Drawing.Point(66, 202);
            this.TimeSelectorUntil1.Name = "TimeSelectorUntil1";
            this.TimeSelectorUntil1.ShowUpDown = true;
            this.TimeSelectorUntil1.Size = new System.Drawing.Size(128, 27);
            this.TimeSelectorUntil1.TabIndex = 26;
            this.TimeSelectorUntil1.Value = new System.DateTime(2017, 10, 25, 12, 0, 0, 0);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(12, 205);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 20);
            this.label5.TabIndex = 25;
            this.label5.Text = "Until:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(7, 165);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 20);
            this.label4.TabIndex = 24;
            this.label4.Text = "From:";
            // 
            // TimeSelectorFrom1
            // 
            this.TimeSelectorFrom1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimeSelectorFrom1.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.TimeSelectorFrom1.Location = new System.Drawing.Point(66, 165);
            this.TimeSelectorFrom1.Name = "TimeSelectorFrom1";
            this.TimeSelectorFrom1.ShowUpDown = true;
            this.TimeSelectorFrom1.Size = new System.Drawing.Size(128, 27);
            this.TimeSelectorFrom1.TabIndex = 18;
            this.TimeSelectorFrom1.Value = new System.DateTime(2017, 10, 25, 12, 0, 0, 0);
            // 
            // submitUnfollows
            // 
            this.submitUnfollows.BackColor = System.Drawing.Color.White;
            this.submitUnfollows.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.submitUnfollows.FlatAppearance.BorderColor = System.Drawing.Color.DarkOrange;
            this.submitUnfollows.FlatAppearance.BorderSize = 2;
            this.submitUnfollows.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.submitUnfollows.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.submitUnfollows.ForeColor = System.Drawing.Color.Black;
            this.submitUnfollows.Location = new System.Drawing.Point(697, 57);
            this.submitUnfollows.Name = "submitUnfollows";
            this.submitUnfollows.Size = new System.Drawing.Size(76, 34);
            this.submitUnfollows.TabIndex = 20;
            this.submitUnfollows.Text = "Submit";
            this.submitUnfollows.UseVisualStyleBackColor = false;
            this.submitUnfollows.Click += new System.EventHandler(this.submitUnfollows_Click);
            // 
            // maxUnfollows
            // 
            this.maxUnfollows.AutoSize = true;
            this.maxUnfollows.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxUnfollows.Location = new System.Drawing.Point(636, 59);
            this.maxUnfollows.Name = "maxUnfollows";
            this.maxUnfollows.Size = new System.Drawing.Size(27, 29);
            this.maxUnfollows.TabIndex = 19;
            this.maxUnfollows.Text = "0";
            // 
            // unfollowMaxTrackBar
            // 
            this.unfollowMaxTrackBar.BackColor = System.Drawing.SystemColors.Control;
            this.unfollowMaxTrackBar.Location = new System.Drawing.Point(412, 52);
            this.unfollowMaxTrackBar.Name = "unfollowMaxTrackBar";
            this.unfollowMaxTrackBar.Size = new System.Drawing.Size(218, 45);
            this.unfollowMaxTrackBar.TabIndex = 18;
            this.unfollowMaxTrackBar.Scroll += new System.EventHandler(this.unfollowMaxTrackBar_Scroll);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(510, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(177, 24);
            this.label3.TabIndex = 17;
            this.label3.Text = "Max Daily Unfollows";
            // 
            // unfollowTime2
            // 
            this.unfollowTime2.AutoSize = true;
            this.unfollowTime2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.unfollowTime2.Location = new System.Drawing.Point(242, 96);
            this.unfollowTime2.Name = "unfollowTime2";
            this.unfollowTime2.Size = new System.Drawing.Size(27, 29);
            this.unfollowTime2.TabIndex = 15;
            this.unfollowTime2.Text = "0";
            // 
            // unfollowTime1
            // 
            this.unfollowTime1.AutoSize = true;
            this.unfollowTime1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.unfollowTime1.Location = new System.Drawing.Point(242, 33);
            this.unfollowTime1.Name = "unfollowTime1";
            this.unfollowTime1.Size = new System.Drawing.Size(27, 29);
            this.unfollowTime1.TabIndex = 14;
            this.unfollowTime1.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(300, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 20);
            this.label2.TabIndex = 13;
            this.label2.Text = "(Seconds)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(242, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 17);
            this.label1.TabIndex = 12;
            this.label1.Text = "To";
            // 
            // unfollowTrackBar2
            // 
            this.unfollowTrackBar2.Location = new System.Drawing.Point(18, 84);
            this.unfollowTrackBar2.Name = "unfollowTrackBar2";
            this.unfollowTrackBar2.Size = new System.Drawing.Size(218, 45);
            this.unfollowTrackBar2.TabIndex = 10;
            this.unfollowTrackBar2.Scroll += new System.EventHandler(this.unfollowTrackBar2_Scroll);
            // 
            // unfollowTrackBar1
            // 
            this.unfollowTrackBar1.BackColor = System.Drawing.SystemColors.Control;
            this.unfollowTrackBar1.Location = new System.Drawing.Point(18, 33);
            this.unfollowTrackBar1.Name = "unfollowTrackBar1";
            this.unfollowTrackBar1.Size = new System.Drawing.Size(218, 45);
            this.unfollowTrackBar1.TabIndex = 8;
            this.unfollowTrackBar1.Scroll += new System.EventHandler(this.unfollowTrackBar1_Scroll);
            // 
            // SetSleepLabel
            // 
            this.SetSleepLabel.AutoSize = true;
            this.SetSleepLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SetSleepLabel.Location = new System.Drawing.Point(80, 6);
            this.SetSleepLabel.Name = "SetSleepLabel";
            this.SetSleepLabel.Size = new System.Drawing.Size(223, 24);
            this.SetSleepLabel.TabIndex = 7;
            this.SetSleepLabel.Text = "Time Between Unfollows:";
            // 
            // StartUnfollowButton
            // 
            this.StartUnfollowButton.BackColor = System.Drawing.Color.Transparent;
            this.StartUnfollowButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("StartUnfollowButton.BackgroundImage")));
            this.StartUnfollowButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.StartUnfollowButton.FlatAppearance.BorderColor = System.Drawing.Color.Blue;
            this.StartUnfollowButton.FlatAppearance.BorderSize = 2;
            this.StartUnfollowButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StartUnfollowButton.ForeColor = System.Drawing.Color.Red;
            this.StartUnfollowButton.Location = new System.Drawing.Point(392, 146);
            this.StartUnfollowButton.Name = "StartUnfollowButton";
            this.StartUnfollowButton.Size = new System.Drawing.Size(381, 134);
            this.StartUnfollowButton.TabIndex = 2;
            this.StartUnfollowButton.UseVisualStyleBackColor = false;
            // 
            // SetUnfollowTime
            // 
            this.SetUnfollowTime.BackColor = System.Drawing.Color.White;
            this.SetUnfollowTime.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.SetUnfollowTime.FlatAppearance.BorderColor = System.Drawing.Color.DarkOrange;
            this.SetUnfollowTime.FlatAppearance.BorderSize = 2;
            this.SetUnfollowTime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SetUnfollowTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SetUnfollowTime.ForeColor = System.Drawing.Color.Black;
            this.SetUnfollowTime.Location = new System.Drawing.Point(295, 57);
            this.SetUnfollowTime.Name = "SetUnfollowTime";
            this.SetUnfollowTime.Size = new System.Drawing.Size(76, 34);
            this.SetUnfollowTime.TabIndex = 16;
            this.SetUnfollowTime.Text = "Submit";
            this.SetUnfollowTime.UseVisualStyleBackColor = false;
            this.SetUnfollowTime.Click += new System.EventHandler(this.SetUnfollowTime_Click);
            // 
            // UnfollowPanel
            // 
            this.UnfollowPanel.BackColor = System.Drawing.Color.Lavender;
            this.UnfollowPanel.Controls.Add(this.StatusLabel);
            this.UnfollowPanel.Controls.Add(this.Secs);
            this.UnfollowPanel.Controls.Add(this.Mins);
            this.UnfollowPanel.Controls.Add(this.Hrs);
            this.UnfollowPanel.Controls.Add(this.label21);
            this.UnfollowPanel.Controls.Add(this.RunningStatus);
            this.UnfollowPanel.Controls.Add(this.CurrentUnfollowAmount);
            this.UnfollowPanel.Controls.Add(this.UnfollowBackButton);
            this.UnfollowPanel.Controls.Add(this.label22);
            this.UnfollowPanel.Controls.Add(this.TimeUntil2);
            this.UnfollowPanel.Controls.Add(this.TimeFrom2);
            this.UnfollowPanel.Controls.Add(this.TimeUntil1);
            this.UnfollowPanel.Controls.Add(this.label17);
            this.UnfollowPanel.Controls.Add(this.TimeFrom1);
            this.UnfollowPanel.Controls.Add(this.pictureBox2);
            this.UnfollowPanel.Controls.Add(this.label19);
            this.UnfollowPanel.Controls.Add(this.label13);
            this.UnfollowPanel.Controls.Add(this.MaxUnfollowDisplay);
            this.UnfollowPanel.Controls.Add(this.SleepInterval2);
            this.UnfollowPanel.Controls.Add(this.label16);
            this.UnfollowPanel.Controls.Add(this.label15);
            this.UnfollowPanel.Controls.Add(this.label14);
            this.UnfollowPanel.Controls.Add(this.label12);
            this.UnfollowPanel.Controls.Add(this.SleepInterval1);
            this.UnfollowPanel.Controls.Add(this.label10);
            this.UnfollowPanel.Controls.Add(this.label9);
            this.UnfollowPanel.Controls.Add(this.pictureBox1);
            this.UnfollowPanel.Controls.Add(this.label8);
            this.UnfollowPanel.Controls.Add(this.SuccessfulUnfollowDisplay);
            this.UnfollowPanel.Controls.Add(this.progressBar1);
            this.UnfollowPanel.Controls.Add(this.label6);
            this.UnfollowPanel.Controls.Add(this.label11);
            this.UnfollowPanel.Controls.Add(this.label7);
            this.UnfollowPanel.Controls.Add(this.label18);
            this.UnfollowPanel.Controls.Add(this.label23);
            this.UnfollowPanel.Controls.Add(this.label24);
            this.UnfollowPanel.Controls.Add(this.UnfollowPauseButton);
            this.UnfollowPanel.Controls.Add(this.UnfollowPlayButton);
            this.UnfollowPanel.Location = new System.Drawing.Point(2, 1);
            this.UnfollowPanel.Name = "UnfollowPanel";
            this.UnfollowPanel.Size = new System.Drawing.Size(787, 346);
            this.UnfollowPanel.TabIndex = 1;
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.StatusLabel.ForeColor = System.Drawing.Color.Red;
            this.StatusLabel.Location = new System.Drawing.Point(516, 14);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(89, 18);
            this.StatusLabel.TabIndex = 43;
            this.StatusLabel.Text = "Status Label";
            this.StatusLabel.Visible = false;
            // 
            // Secs
            // 
            this.Secs.BackColor = System.Drawing.Color.Lavender;
            this.Secs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Secs.Location = new System.Drawing.Point(258, 120);
            this.Secs.Name = "Secs";
            this.Secs.Size = new System.Drawing.Size(44, 21);
            this.Secs.TabIndex = 38;
            this.Secs.Text = "0";
            this.Secs.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Mins
            // 
            this.Mins.BackColor = System.Drawing.Color.Lavender;
            this.Mins.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mins.Location = new System.Drawing.Point(202, 120);
            this.Mins.Name = "Mins";
            this.Mins.Size = new System.Drawing.Size(44, 21);
            this.Mins.TabIndex = 37;
            this.Mins.Text = "0";
            this.Mins.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Hrs
            // 
            this.Hrs.BackColor = System.Drawing.Color.Lavender;
            this.Hrs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Hrs.Location = new System.Drawing.Point(148, 120);
            this.Hrs.Name = "Hrs";
            this.Hrs.Size = new System.Drawing.Size(44, 21);
            this.Hrs.TabIndex = 36;
            this.Hrs.Text = "0";
            this.Hrs.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label21.Location = new System.Drawing.Point(3, 109);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(139, 37);
            this.label21.TabIndex = 35;
            this.label21.Text = "Time Running:";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RunningStatus
            // 
            this.RunningStatus.AutoSize = true;
            this.RunningStatus.BackColor = System.Drawing.Color.Transparent;
            this.RunningStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RunningStatus.ForeColor = System.Drawing.Color.Lime;
            this.RunningStatus.Image = ((System.Drawing.Image)(resources.GetObject("RunningStatus.Image")));
            this.RunningStatus.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.RunningStatus.Location = new System.Drawing.Point(143, 71);
            this.RunningStatus.Name = "RunningStatus";
            this.RunningStatus.Size = new System.Drawing.Size(99, 26);
            this.RunningStatus.TabIndex = 34;
            this.RunningStatus.Text = "Running.";
            // 
            // CurrentUnfollowAmount
            // 
            this.CurrentUnfollowAmount.AutoSize = true;
            this.CurrentUnfollowAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentUnfollowAmount.ForeColor = System.Drawing.Color.Blue;
            this.CurrentUnfollowAmount.Location = new System.Drawing.Point(714, 298);
            this.CurrentUnfollowAmount.Name = "CurrentUnfollowAmount";
            this.CurrentUnfollowAmount.Size = new System.Drawing.Size(18, 20);
            this.CurrentUnfollowAmount.TabIndex = 30;
            this.CurrentUnfollowAmount.Text = "0";
            // 
            // UnfollowBackButton
            // 
            this.UnfollowBackButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("UnfollowBackButton.BackgroundImage")));
            this.UnfollowBackButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.UnfollowBackButton.Location = new System.Drawing.Point(7, 0);
            this.UnfollowBackButton.Name = "UnfollowBackButton";
            this.UnfollowBackButton.Size = new System.Drawing.Size(44, 38);
            this.UnfollowBackButton.TabIndex = 25;
            this.UnfollowBackButton.TabStop = false;
            this.UnfollowBackButton.Click += new System.EventHandler(this.UnfollowBackButton_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(203, 297);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(19, 25);
            this.label22.TabIndex = 24;
            this.label22.Text = "-";
            this.label22.Visible = false;
            // 
            // TimeUntil2
            // 
            this.TimeUntil2.AutoSize = true;
            this.TimeUntil2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimeUntil2.ForeColor = System.Drawing.Color.Blue;
            this.TimeUntil2.Location = new System.Drawing.Point(245, 301);
            this.TimeUntil2.Name = "TimeUntil2";
            this.TimeUntil2.Size = new System.Drawing.Size(18, 20);
            this.TimeUntil2.TabIndex = 23;
            this.TimeUntil2.Text = "0";
            this.TimeUntil2.Visible = false;
            // 
            // TimeFrom2
            // 
            this.TimeFrom2.AutoSize = true;
            this.TimeFrom2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimeFrom2.ForeColor = System.Drawing.Color.Blue;
            this.TimeFrom2.Location = new System.Drawing.Point(124, 301);
            this.TimeFrom2.Name = "TimeFrom2";
            this.TimeFrom2.Size = new System.Drawing.Size(18, 20);
            this.TimeFrom2.TabIndex = 22;
            this.TimeFrom2.Text = "0";
            this.TimeFrom2.Visible = false;
            // 
            // TimeUntil1
            // 
            this.TimeUntil1.AutoSize = true;
            this.TimeUntil1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimeUntil1.ForeColor = System.Drawing.Color.Blue;
            this.TimeUntil1.Location = new System.Drawing.Point(247, 280);
            this.TimeUntil1.Name = "TimeUntil1";
            this.TimeUntil1.Size = new System.Drawing.Size(18, 20);
            this.TimeUntil1.TabIndex = 21;
            this.TimeUntil1.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(203, 276);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(19, 25);
            this.label17.TabIndex = 20;
            this.label17.Text = "-";
            // 
            // TimeFrom1
            // 
            this.TimeFrom1.AutoSize = true;
            this.TimeFrom1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimeFrom1.ForeColor = System.Drawing.Color.Blue;
            this.TimeFrom1.Location = new System.Drawing.Point(125, 280);
            this.TimeFrom1.Name = "TimeFrom1";
            this.TimeFrom1.Size = new System.Drawing.Size(18, 20);
            this.TimeFrom1.TabIndex = 19;
            this.TimeFrom1.Text = "0";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(10, 161);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(41, 38);
            this.pictureBox2.TabIndex = 18;
            this.pictureBox2.TabStop = false;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label19.Location = new System.Drawing.Point(321, 200);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(76, 22);
            this.label19.TabIndex = 17;
            this.label19.Text = "(Seconds)";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(257, 197);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(19, 25);
            this.label13.TabIndex = 16;
            this.label13.Text = "-";
            // 
            // MaxUnfollowDisplay
            // 
            this.MaxUnfollowDisplay.AutoSize = true;
            this.MaxUnfollowDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaxUnfollowDisplay.ForeColor = System.Drawing.Color.Blue;
            this.MaxUnfollowDisplay.Location = new System.Drawing.Point(166, 229);
            this.MaxUnfollowDisplay.Name = "MaxUnfollowDisplay";
            this.MaxUnfollowDisplay.Size = new System.Drawing.Size(18, 20);
            this.MaxUnfollowDisplay.TabIndex = 15;
            this.MaxUnfollowDisplay.Text = "0";
            // 
            // SleepInterval2
            // 
            this.SleepInterval2.AutoSize = true;
            this.SleepInterval2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SleepInterval2.ForeColor = System.Drawing.Color.Blue;
            this.SleepInterval2.Location = new System.Drawing.Point(282, 201);
            this.SleepInterval2.Name = "SleepInterval2";
            this.SleepInterval2.Size = new System.Drawing.Size(33, 20);
            this.SleepInterval2.TabIndex = 14;
            this.SleepInterval2.Text = "SI2";
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label16.Location = new System.Drawing.Point(17, 300);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(99, 21);
            this.label16.TabIndex = 13;
            this.label16.Text = "Time Slot 2:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label16.Visible = false;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label15.Location = new System.Drawing.Point(12, 281);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(107, 19);
            this.label15.TabIndex = 12;
            this.label15.Text = "Time Slot 1:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.label14.Location = new System.Drawing.Point(19, 250);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(102, 31);
            this.label14.TabIndex = 11;
            this.label14.Text = "Run Times:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.label12.Location = new System.Drawing.Point(19, 224);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(130, 30);
            this.label12.TabIndex = 9;
            this.label12.Text = "Goal Unfollows:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SleepInterval1
            // 
            this.SleepInterval1.AutoSize = true;
            this.SleepInterval1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SleepInterval1.ForeColor = System.Drawing.Color.Blue;
            this.SleepInterval1.Location = new System.Drawing.Point(218, 202);
            this.SleepInterval1.Name = "SleepInterval1";
            this.SleepInterval1.Size = new System.Drawing.Size(33, 20);
            this.SleepInterval1.TabIndex = 8;
            this.SleepInterval1.Text = "SI1";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.label10.Location = new System.Drawing.Point(3, 202);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(225, 22);
            this.label10.TabIndex = 7;
            this.label10.Text = "Time between Unfollows:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(39, 161);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(124, 38);
            this.label9.TabIndex = 6;
            this.label9.Text = "Settings";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(0, 43);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(791, 10);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.label8.Location = new System.Drawing.Point(495, 50);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(225, 26);
            this.label8.TabIndex = 4;
            this.label8.Text = "Successful Unfollows";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SuccessfulUnfollowDisplay
            // 
            this.SuccessfulUnfollowDisplay.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.SuccessfulUnfollowDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.SuccessfulUnfollowDisplay.FullRowSelect = true;
            this.SuccessfulUnfollowDisplay.GridLines = true;
            this.SuccessfulUnfollowDisplay.Location = new System.Drawing.Point(436, 79);
            this.SuccessfulUnfollowDisplay.Name = "SuccessfulUnfollowDisplay";
            this.SuccessfulUnfollowDisplay.Size = new System.Drawing.Size(338, 209);
            this.SuccessfulUnfollowDisplay.TabIndex = 3;
            this.SuccessfulUnfollowDisplay.UseCompatibleStateImageBehavior = false;
            this.SuccessfulUnfollowDisplay.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Profile Names";
            this.columnHeader1.Width = 338;
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.Color.White;
            this.progressBar1.ForeColor = System.Drawing.Color.Lime;
            this.progressBar1.Location = new System.Drawing.Point(564, 296);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(144, 26);
            this.progressBar1.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.25F);
            this.label6.Location = new System.Drawing.Point(320, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(171, 35);
            this.label6.TabIndex = 0;
            this.label6.Text = "Unfollower";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.label11.ForeColor = System.Drawing.Color.Blue;
            this.label11.Location = new System.Drawing.Point(51, -5);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(122, 56);
            this.label11.TabIndex = 26;
            this.label11.Text = "Stop and Change Settings";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label11.Click += new System.EventHandler(this.UnfollowBackButton_Click);
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(432, 294);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(126, 28);
            this.label7.TabIndex = 2;
            this.label7.Text = "Goal Progress:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.Lavender;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label18.ForeColor = System.Drawing.Color.Blue;
            this.label18.Location = new System.Drawing.Point(2, 67);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(95, 30);
            this.label18.TabIndex = 31;
            this.label18.Text = "Status:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(190, 118);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(14, 20);
            this.label23.TabIndex = 39;
            this.label23.Text = ":";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(246, 120);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(14, 20);
            this.label24.TabIndex = 40;
            this.label24.Text = ":";
            // 
            // UnfollowPauseButton
            // 
            this.UnfollowPauseButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("UnfollowPauseButton.BackgroundImage")));
            this.UnfollowPauseButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.UnfollowPauseButton.Location = new System.Drawing.Point(738, 3);
            this.UnfollowPauseButton.Name = "UnfollowPauseButton";
            this.UnfollowPauseButton.Size = new System.Drawing.Size(42, 37);
            this.UnfollowPauseButton.TabIndex = 41;
            this.UnfollowPauseButton.TabStop = false;
            this.UnfollowPauseButton.Click += new System.EventHandler(this.UnfollowPauseButton_Click);
            // 
            // UnfollowPlayButton
            // 
            this.UnfollowPlayButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("UnfollowPlayButton.BackgroundImage")));
            this.UnfollowPlayButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.UnfollowPlayButton.Location = new System.Drawing.Point(738, 3);
            this.UnfollowPlayButton.Name = "UnfollowPlayButton";
            this.UnfollowPlayButton.Size = new System.Drawing.Size(42, 37);
            this.UnfollowPlayButton.TabIndex = 42;
            this.UnfollowPlayButton.TabStop = false;
            this.UnfollowPlayButton.Visible = false;
            this.UnfollowPlayButton.Click += new System.EventHandler(this.UnfollowPlayButton_Click);
            // 
            // UnfollowTimer
            // 
            this.UnfollowTimer.Interval = 1000;
            this.UnfollowTimer.Tick += new System.EventHandler(this.UnfollowTimer_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 332);
            this.Controls.Add(this.setFollowTime);
            this.Controls.Add(this.UnfollowPanel);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.setFollowTime.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TimeOfDayFollowPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FollowLimitPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SleepPicFollow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.followMaxTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.followTrackBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.followTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartFollowPic)).EndInit();
            this.submitIntervalTime.ResumeLayout(false);
            this.submitIntervalTime.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.setTimeOfDayPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.setUnfollowLimitPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.setSleepPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unfollowMaxTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unfollowTrackBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unfollowTrackBar1)).EndInit();
            this.UnfollowPanel.ResumeLayout(false);
            this.UnfollowPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UnfollowBackButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnfollowPauseButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnfollowPlayButton)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl setFollowTime;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage submitIntervalTime;
        private System.Windows.Forms.Button TimeOfDayButton;
        private System.Windows.Forms.Button StartUnfollowButton;
        private System.Windows.Forms.Button UnfollowLimitButton;
        private System.Windows.Forms.Button SleepButton;
        private System.Windows.Forms.PictureBox setSleepPic;
        private System.Windows.Forms.PictureBox setUnfollowLimitPic;
        private System.Windows.Forms.PictureBox setTimeOfDayPic;
        private System.Windows.Forms.Label unfollowTime2;
        private System.Windows.Forms.Label unfollowTime1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar unfollowTrackBar2;
        private System.Windows.Forms.TrackBar unfollowTrackBar1;
        private System.Windows.Forms.Label SetSleepLabel;
        private System.Windows.Forms.Button SetUnfollowTime;
        private System.Windows.Forms.Button submitUnfollows;
        private System.Windows.Forms.Label maxUnfollows;
        private System.Windows.Forms.TrackBar unfollowMaxTrackBar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Step3;
        private System.Windows.Forms.Label Step2;
        private System.Windows.Forms.Label Step1;
        private System.Windows.Forms.DateTimePicker TimeSelectorFrom1;
        private System.Windows.Forms.DateTimePicker TimeSelectorUntil1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button SubmitTimeOfDay;
        private System.Windows.Forms.DateTimePicker TimeSelectorUntil2;
        private System.Windows.Forms.DateTimePicker TimeSelectorFrom2;
        private System.Windows.Forms.Label UntilLabel2;
        private System.Windows.Forms.Label FromLabel2;
        private System.Windows.Forms.Button AddTimeSelector;
        private System.Windows.Forms.PictureBox startButton;
        private System.Windows.Forms.CheckBox UnfollowStep3Check;
        private System.Windows.Forms.CheckBox UnfollowStep2Check;
        private System.Windows.Forms.CheckBox UnfollowStep1Check;
        private System.Windows.Forms.Panel UnfollowPanel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label SleepInterval1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label MaxUnfollowDisplay;
        private System.Windows.Forms.Label SleepInterval2;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label TimeUntil2;
        private System.Windows.Forms.Label TimeFrom2;
        private System.Windows.Forms.Label TimeUntil1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label TimeFrom1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox UnfollowBackButton;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label RunningStatus;
        private System.Windows.Forms.Label CurrentUnfollowAmount;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox Secs;
        private System.Windows.Forms.TextBox Mins;
        private System.Windows.Forms.TextBox Hrs;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Timer UnfollowTimer;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox UnfollowPauseButton;
        private System.Windows.Forms.PictureBox UnfollowPlayButton;
        public System.Windows.Forms.ColumnHeader columnHeader1;
        public System.Windows.Forms.ListView SuccessfulUnfollowDisplay;
        private System.Windows.Forms.Label followTime2;
        private System.Windows.Forms.Label followTime1;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TrackBar followTrackBar2;
        private System.Windows.Forms.TrackBar followTrackBar1;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button submitFollowTime;
        private System.Windows.Forms.Label followStep1;
        private System.Windows.Forms.PictureBox SleepPicFollow;
        private System.Windows.Forms.Button SleepButtonFollow;
        private System.Windows.Forms.CheckBox FollowCheck3;
        private System.Windows.Forms.CheckBox FollowCheck2;
        private System.Windows.Forms.CheckBox FollowCheck1;
        private System.Windows.Forms.PictureBox StartFollowPic;
        private System.Windows.Forms.Button FollowStartButton;
        private System.Windows.Forms.Label followStep3;
        private System.Windows.Forms.PictureBox TimeOfDayFollowPic;
        private System.Windows.Forms.Button TimeOfDayFollowButton;
        private System.Windows.Forms.Label followStep2;
        private System.Windows.Forms.PictureBox FollowLimitPic;
        private System.Windows.Forms.Button FollowLimitButton;
        private System.Windows.Forms.Button submitFollows;
        private System.Windows.Forms.Label maxFollowsLabel;
        private System.Windows.Forms.TrackBar followMaxTrackBar;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button submitTimeOfDayF;
        private System.Windows.Forms.DateTimePicker TimeSelectorUntil2F;
        private System.Windows.Forms.DateTimePicker TimeSelectorFrom2F;
        private System.Windows.Forms.Label UntilLabelF;
        private System.Windows.Forms.Label FromLabel2F;
        private System.Windows.Forms.Button addTimeSelectorF;
        private System.Windows.Forms.DateTimePicker TimeSelectorUntil1F;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.DateTimePicker TimeSelectorFrom1F;
        private System.Windows.Forms.Label StatusLabel;
    }
}