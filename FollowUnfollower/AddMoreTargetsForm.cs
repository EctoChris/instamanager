﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.Specialized;
using FollowUnfollower.Properties;
using System.Text.RegularExpressions;

namespace FollowUnfollower
{
    public partial class AddMoreTargetsForm : Form
    {
       // public Form parentForm;
        public bool firstTimeFocused = true;
        public AddMoreTargetsForm()
        {
            InitializeComponent();
        }

        private void AddTargetsButton_Click(object sender, EventArgs e)
        {
            //Get current target accounts list
            StringCollection savedList = (StringCollection)Settings.Default["CurrentTargetAccounts"];
            var list = savedList.Cast<string>().ToList();

            //Get user inputted new accounts
            List<string> targetAccounts = new List<string>();
            for (int i = 0; i < TargetAccountsTextBox.Lines.Length; i++)
            {
                if(TargetAccountsTextBox.Lines.Length > 0 )
                {
                    bool containsLetter = Regex.IsMatch(TargetAccountsTextBox.Lines[i], "[a-zA-Z]");
                    if(containsLetter)
                          targetAccounts.Add(TargetAccountsTextBox.Lines[i]);
                }
            }

            //Combine lists
            foreach(string s in targetAccounts)
            {
                if (!list.Contains(s))
                    list.Add(s);
            }

            //Remove lines that don't start with '@'
            for(int i = 0; i < list.Count; i++)
            {
                if (list[i][0] != '@')
                    list.Remove(list[i]);
            }

            //Update Settings.Default
            StringCollection collection = new StringCollection();
            collection.AddRange(list.ToArray());

            Settings.Default["CurrentTargetAccounts"] = collection;
            Settings.Default.Save();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }


        private void TargetAccountsTextBox_MouseClick(object sender, MouseEventArgs e)
        {
            if (firstTimeFocused)
            {
                TargetAccountsTextBox.Clear();
                firstTimeFocused = false;
            }
        }
    }
}
