﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FollowUnfollower.Properties;

namespace FollowUnfollower
{
    public partial class UnfollowPage3 : Form
    {
        public UnfollowPage3()
        {
            InitializeComponent();
            MaxDailyAmount.Text = Settings.Default["MaxDailyAmount"].ToString();
            MaxDailyFollowsTrackBar.Value = (int)Settings.Default["MaxDailyAmount"];
        }

        private void SettingsGuideScreen_Load(object sender, EventArgs e)
        {
            //comment test
        }

        private void ContinueButton_Click(object sender, EventArgs e)
        {
            Settings.Default["MaxDailyAmount"] = (int)MaxDailyFollowsTrackBar.Value;
            Settings.Default.Save();

            this.Hide();
            if (Globals.unfollowPage4 == null)
                Globals.unfollowPage4 = new UnfollowPage4();

            Globals.unfollowPage4.Show();

        }

        private void HomeButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            Globals.homePage.Show();
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            Utils.ExitProgram();
        }

        private void MaxDailyFollowsTrackBar_Scroll(object sender, EventArgs e)
        {
            MaxDailyAmount.Text = MaxDailyFollowsTrackBar.Value.ToString();
        }
    }
}
