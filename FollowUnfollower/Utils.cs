﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using OpenQA.Selenium.Chrome;

namespace FollowUnfollower
{
    public class Utils
    {
        public static void FindAndSendKeys(string xpath, string sentKeys)
        {
            try
            {
                IWebElement test = Globals.pDriver.FindElement(By.XPath(xpath));
                test.SendKeys(sentKeys);
                Thread.Sleep(1500);
            } catch (Exception e)
            {
                MessageBox.Show("Didnt Work xPath: ", xpath);
                FindAndSendKeys(xpath, sentKeys);
            }
        }

        public static IList<IWebElement> GetListOfButtons(string xpath)
        {
            try
            {
                IList<IWebElement> unfollowButtonsVisible = new List<IWebElement>();
                unfollowButtonsVisible = Globals.pDriver.FindElements(By.XPath(xpath));
                return unfollowButtonsVisible;
            } catch(Exception e)
            {
                MessageBox.Show(xpath + e.Message);
                GetListOfButtons(xpath);
            }
            return null;
        }

        public static void FindAndSendKeysAndEnter(string xpath, string sentKeys)
        {
            try
            {
                IWebElement element = Globals.pDriver.FindElement(By.XPath(xpath));
                element.SendKeys(sentKeys);
                Thread.Sleep(2000);

                element.SendKeys(OpenQA.Selenium.Keys.Enter);
                Thread.Sleep(500);
                element.SendKeys(OpenQA.Selenium.Keys.Enter);
                element.SendKeys(OpenQA.Selenium.Keys.Enter);

                //element.Submit();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                FindAndSendKeysAndEnter(xpath, sentKeys);
            }
        }

        public static void FindByIdAndSendKeys(string id, string sentKeys)
        {
            try
            {
                Globals.pDriver.FindElement(By.Id(id)).SendKeys(sentKeys);
                Thread.Sleep(1500);
            }
            catch (Exception ex)
            {
                MessageBox.Show("fuck it didnt work: findbyidandsendkeys " + ex.Message);
                FindByIdAndSendKeys(id, sentKeys);
            }
        }
        public static void FindAndClick(string XPath)
        {
            try
            {
                Globals.pDriver.FindElement(By.XPath(XPath)).Click();
                Thread.Sleep(1000);
            } catch (ElementNotVisibleException e)
            {
                Console.WriteLine(XPath + "element not found, continuing search");
                FindAndClick(XPath);
            }
        }

        public static string FindId(string XPath)
        {
            try
            {
               IWebElement element =  Globals.pDriver.FindElement(By.XPath(XPath));
                string idName = element.GetAttribute("for");
                return idName;
            } catch (Exception ex)
            {
                MessageBox.Show("Didnt Work");
                return null;
            }
        }
        public static void GoToUrl(string url)
        {
            Globals.pDriver.Url = url;    //Navigating to Instagram
            Thread.Sleep(1000);
            Globals.pDriver.Navigate();
        }

        public static IWebElement FindElement(string XPath)
        {
            while (true)
            {
                try
                {
                    IWebElement element = Globals.pDriver.FindElement(By.XPath(XPath));
                    return element;
                }
                catch (Exception e) { }
            }
        }

        public static IWebElement FindElement(IWebElement baseElement, string XPath)
        {
            if(baseElement == null)
            {
                return null;
            }
            int searchCounter = 0;
            while (searchCounter < 10)
            {
                try
                {
                    IWebElement element = baseElement.FindElement(By.XPath(XPath));
                    return element;
                }
                catch (Exception e) {
                    Console.WriteLine("Problem with " + XPath);
                    searchCounter++;
                }
            }
            return null;
        }

        public static void RandomSleepInterval(int num1, int num2)
        {
            Random r = new Random();
            int num1Ms = num1 * 1000;
            int num2Ms = num2 * 1000;
            int randomNumber = r.Next(num1Ms, num2Ms);
            Thread.Sleep(randomNumber);
        }


        public static bool CheckConditions(DateTime RunTime1, DateTime RunTime2, DateTime RunTime3, DateTime RunTime4, int unfollowCounter, int maxUnfollows, bool unfollowerRunning)
        {
            bool result = false;

            if (DateTime.Now >= RunTime1 && DateTime.Now <= RunTime2 || (DateTime.Now >= RunTime3 && DateTime.Now <= RunTime4 && Globals.secondTimeSlotExists == true))
            {
                if (unfollowCounter < maxUnfollows && unfollowerRunning == false) //&& UnfollowerRunning == false
                {
                    result = true;
                }
            } else
            {
                Globals.ProgramStatus = Globals.Status.Idle;
            }
            return result;
        }

        public static void StopTimer(PictureBox pauseButton, PictureBox playButton, Label status, System.Windows.Forms.Timer timer)
        {
            timer.Stop();
            pauseButton.Hide();
            playButton.Show();
            status.Text = "Stopped";
            status.BackColor = Color.Black;
            status.ForeColor = Color.Red;
        }

        public static void StartTimer(PictureBox pauseButton, PictureBox playbutton, Label status, System.Windows.Forms.Timer timer)
        {
            timer.Start();
            pauseButton.Show();
            playbutton.Hide();
            status.Text = "Running.";
            status.BackColor = Color.Black;
            status.ForeColor = Color.Red;
        }

        public static void ExitProgram()
        {
            Globals.pDriver.Close();
            Globals.pDriver.Quit();
            Application.Exit();
        }

    }
}

