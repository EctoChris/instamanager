﻿namespace FollowUnfollower
{
    partial class UnfollowingPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UnfollowingPage));
            this.FollowSettingsLabel = new System.Windows.Forms.Label();
            this.Status = new System.Windows.Forms.Label();
            this.LiveSuccessfulFollowsListBox = new System.Windows.Forms.ListBox();
            this.FollowsToday = new System.Windows.Forms.Label();
            this.TimeRunning = new System.Windows.Forms.Label();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.FollowsTodayLabel = new System.Windows.Forms.Label();
            this.TimeRunningLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.EditSettingsButton = new System.Windows.Forms.PictureBox();
            this.PlayPauseButton = new System.Windows.Forms.PictureBox();
            this.HomeButton = new System.Windows.Forms.PictureBox();
            this.logoutButton = new System.Windows.Forms.PictureBox();
            this.ViewCurrentSettingsButton = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.EditSettingsButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayPauseButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HomeButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoutButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewCurrentSettingsButton)).BeginInit();
            this.SuspendLayout();
            // 
            // FollowSettingsLabel
            // 
            this.FollowSettingsLabel.AutoSize = true;
            this.FollowSettingsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.25F);
            this.FollowSettingsLabel.Location = new System.Drawing.Point(284, 61);
            this.FollowSettingsLabel.Name = "FollowSettingsLabel";
            this.FollowSettingsLabel.Size = new System.Drawing.Size(282, 30);
            this.FollowSettingsLabel.TabIndex = 4;
            this.FollowSettingsLabel.Text = "FollowGram Unfollower";
            // 
            // Status
            // 
            this.Status.AutoSize = true;
            this.Status.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.Status.Location = new System.Drawing.Point(336, 95);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(65, 24);
            this.Status.TabIndex = 5;
            this.Status.Text = "Status:";
            // 
            // LiveSuccessfulFollowsListBox
            // 
            this.LiveSuccessfulFollowsListBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LiveSuccessfulFollowsListBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.LiveSuccessfulFollowsListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.LiveSuccessfulFollowsListBox.FormattingEnabled = true;
            this.LiveSuccessfulFollowsListBox.ItemHeight = 18;
            this.LiveSuccessfulFollowsListBox.Items.AddRange(new object[] {
            "user1",
            "user2",
            "user3"});
            this.LiveSuccessfulFollowsListBox.Location = new System.Drawing.Point(219, 398);
            this.LiveSuccessfulFollowsListBox.Name = "LiveSuccessfulFollowsListBox";
            this.LiveSuccessfulFollowsListBox.Size = new System.Drawing.Size(389, 130);
            this.LiveSuccessfulFollowsListBox.TabIndex = 24;
            // 
            // FollowsToday
            // 
            this.FollowsToday.AutoSize = true;
            this.FollowsToday.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.FollowsToday.Location = new System.Drawing.Point(247, 119);
            this.FollowsToday.Name = "FollowsToday";
            this.FollowsToday.Size = new System.Drawing.Size(154, 24);
            this.FollowsToday.TabIndex = 28;
            this.FollowsToday.Text = "Unfollows Today:";
            // 
            // TimeRunning
            // 
            this.TimeRunning.AutoSize = true;
            this.TimeRunning.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.TimeRunning.Location = new System.Drawing.Point(266, 144);
            this.TimeRunning.Name = "TimeRunning";
            this.TimeRunning.Size = new System.Drawing.Size(135, 24);
            this.TimeRunning.TabIndex = 29;
            this.TimeRunning.Text = "Time Running:";
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.StatusLabel.Location = new System.Drawing.Point(438, 99);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(74, 18);
            this.StatusLabel.TabIndex = 30;
            this.StatusLabel.Text = "Running...";
            // 
            // FollowsTodayLabel
            // 
            this.FollowsTodayLabel.AutoSize = true;
            this.FollowsTodayLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.FollowsTodayLabel.Location = new System.Drawing.Point(437, 123);
            this.FollowsTodayLabel.Name = "FollowsTodayLabel";
            this.FollowsTodayLabel.Size = new System.Drawing.Size(32, 18);
            this.FollowsTodayLabel.TabIndex = 31;
            this.FollowsTodayLabel.Text = "120";
            // 
            // TimeRunningLabel
            // 
            this.TimeRunningLabel.AutoSize = true;
            this.TimeRunningLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.TimeRunningLabel.Location = new System.Drawing.Point(440, 147);
            this.TimeRunningLabel.Name = "TimeRunningLabel";
            this.TimeRunningLabel.Size = new System.Drawing.Size(64, 18);
            this.TimeRunningLabel.TabIndex = 32;
            this.TimeRunningLabel.Text = "00:00:00";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label1.Location = new System.Drawing.Point(300, 371);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(226, 24);
            this.label1.TabIndex = 34;
            this.label1.Text = "Live Successful Unfollows";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // EditSettingsButton
            // 
            this.EditSettingsButton.BackgroundImage = global::FollowUnfollower.Properties.Resources.EditSettingsButton;
            this.EditSettingsButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.EditSettingsButton.Location = new System.Drawing.Point(650, 507);
            this.EditSettingsButton.Name = "EditSettingsButton";
            this.EditSettingsButton.Size = new System.Drawing.Size(152, 41);
            this.EditSettingsButton.TabIndex = 35;
            this.EditSettingsButton.TabStop = false;
            this.EditSettingsButton.Click += new System.EventHandler(this.EditSettingsButton_Click);
            // 
            // PlayPauseButton
            // 
            this.PlayPauseButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PlayPauseButton.BackgroundImage")));
            this.PlayPauseButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PlayPauseButton.Location = new System.Drawing.Point(317, 179);
            this.PlayPauseButton.Name = "PlayPauseButton";
            this.PlayPauseButton.Size = new System.Drawing.Size(193, 180);
            this.PlayPauseButton.TabIndex = 33;
            this.PlayPauseButton.TabStop = false;
            this.PlayPauseButton.Click += new System.EventHandler(this.PlayPauseButton_Click);
            // 
            // HomeButton
            // 
            this.HomeButton.BackgroundImage = global::FollowUnfollower.Properties.Resources.FollowGramLogo;
            this.HomeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.HomeButton.Location = new System.Drawing.Point(12, -3);
            this.HomeButton.Name = "HomeButton";
            this.HomeButton.Size = new System.Drawing.Size(103, 97);
            this.HomeButton.TabIndex = 3;
            this.HomeButton.TabStop = false;
            this.HomeButton.Click += new System.EventHandler(this.HomeButton_Click);
            // 
            // logoutButton
            // 
            this.logoutButton.BackgroundImage = global::FollowUnfollower.Properties.Resources.logoutButton;
            this.logoutButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.logoutButton.Location = new System.Drawing.Point(676, 22);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(126, 49);
            this.logoutButton.TabIndex = 2;
            this.logoutButton.TabStop = false;
            // 
            // ViewCurrentSettingsButton
            // 
            this.ViewCurrentSettingsButton.BackgroundImage = global::FollowUnfollower.Properties.Resources.ViewCurrentSettingsButton;
            this.ViewCurrentSettingsButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ViewCurrentSettingsButton.Location = new System.Drawing.Point(14, 507);
            this.ViewCurrentSettingsButton.Name = "ViewCurrentSettingsButton";
            this.ViewCurrentSettingsButton.Size = new System.Drawing.Size(181, 44);
            this.ViewCurrentSettingsButton.TabIndex = 36;
            this.ViewCurrentSettingsButton.TabStop = false;
            // 
            // UnfollowingPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 560);
            this.Controls.Add(this.ViewCurrentSettingsButton);
            this.Controls.Add(this.EditSettingsButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PlayPauseButton);
            this.Controls.Add(this.TimeRunningLabel);
            this.Controls.Add(this.FollowsTodayLabel);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.TimeRunning);
            this.Controls.Add(this.FollowsToday);
            this.Controls.Add(this.LiveSuccessfulFollowsListBox);
            this.Controls.Add(this.Status);
            this.Controls.Add(this.FollowSettingsLabel);
            this.Controls.Add(this.HomeButton);
            this.Controls.Add(this.logoutButton);
            this.Name = "UnfollowingPage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Follow Page 1";
            this.Load += new System.EventHandler(this.SettingsGuideScreen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.EditSettingsButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayPauseButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HomeButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoutButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewCurrentSettingsButton)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox logoutButton;
        private System.Windows.Forms.PictureBox HomeButton;
        private System.Windows.Forms.Label FollowSettingsLabel;
        private System.Windows.Forms.Label Status;
        private System.Windows.Forms.ListBox LiveSuccessfulFollowsListBox;
        private System.Windows.Forms.Label FollowsToday;
        private System.Windows.Forms.Label TimeRunning;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.Label FollowsTodayLabel;
        private System.Windows.Forms.Label TimeRunningLabel;
        private System.Windows.Forms.PictureBox PlayPauseButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox EditSettingsButton;
        private System.Windows.Forms.PictureBox ViewCurrentSettingsButton;
    }
}